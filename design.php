<?php
//Header+OG setup
$siteName = "Geek Media";
$title = "Grafiske løsninger";
$description = "Form og funktion gennem spændende grænseflader og menneskelige oplevelser.";
$keywords = "";
$image = "images/header/hero9.jpg";
?>
<!DOCTYPE html>
<html>

<head>
    <?php include("includes/meta.php")?>
    <?php require("includes/styles.php")?>
</head>
<body>
<?php include("includes/preloader.php");?>
<?php include("includes/nav.php");?>
<div class="main-container">
    <header class="page-header">
        <div class="background-image-holder">
            <img class="background-image" alt="Background Image" src="/images/header/hero9.jpg">
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <span class="text-white alt-font">Grafisk opsætning</span>
                    <h1 class="text-white">Design</h1>
                    <p class="text-white lead">Form og funktion <br> der samler dit brand.</p>
                </div>
            </div>
        </div>
    </header>
    <section class="milestones services-selector bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                    <h1>Grafisk opsætning</h1>
                    <p class="lead">Form og funktion gennem spændende grænseflader og menneskelige oplevelser.
                        Udtryksfulde publikationer der fanger læserne, og en visuelt identitet der er en sammenhængende
                    fortælling af firmaet. Dette er nogle af de områder hvor jeg kan hjælpe dig som freelance
                        samarbejdspartner!</p>
                </div>
            </div>
            <div class="row">
                <div class="container">
                    <ul class="services-tabs clearfix">
                        <li class="col-md-3 col-sm-6 text-center col-md-offset-1-5 space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-pencil"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Design & UX</h5>
                                <span>Form og funktion</span>
                                <span class="sub">LÆS MERE</span>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-printer"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Print design</h5>
                                <span>Fra skærm til bord</span>
                                <span class="sub">LÆS MERE</span>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-target"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Visuel identitet</h5>
                                <span>Fra platform til platform</span>
                                <span class="sub">LÆS MERE</span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="container">
                    <ul class="services-content">
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <h2>Bedre oplevelse gennem design</h2>
                                    <p class="lead">
                                        Visuel engagement og en imponerende brugeroplevelse i det moderne internet er
                                        afgørende for vellykkede projekter, mindeværdige brands og glade brugere. Med et
                                        fokus på menneskelige erfaringer, mener jeg, at kvaliteten af et
                                        design har en afgørende indflydelse på succesen af et hvert projekt.
                                    </p>
                                    <p class="lead">
                                        Jeg anerkender, at hvert projekt er unikt, og at der kræves sans for detaljer
                                        for at nå frem til det ultimative og mest givende resultat.
                                    </p>
                                </div>
                                <div class="col-sm-8 col-sm-offset-2">
                                    <div class="col-sm-6 col-md-4 services" id="design_1">
                                        <h3>Design strategi</h3>
                                        <p>Målbevidste anbefalinger baseret på konstruktiv design konsultation, brand analyse og gennemgang af målsætninger.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="design_2" >
                                        <h3>Informations planlægning</h3>
                                        <p>Publikum og mål analyse til at udvikle forløb til flot design, opmuntrende enkelhed og klarhed i struktur.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="design_3" >
                                        <h3>Interface design</h3>
                                        <p>Detaljeret konceptualisering af alle centrale elementer i et digitalt projekt til fremme af målbevidste og intelligente grænseflader.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="design_4">
                                        <h3>User experience</h3>
                                        <p>Løsningsforslag til at fremme engagement gennem enkle, intuitive og dejlige oplevelser på tværs af alle enheder.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="design_5" >
                                        <h3>Prototype</h3>
                                        <p>Udforskning af potentielle koncepter for på tværs af enheder oplevelser gennem wireframes og digitale prototyper</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="design_6" >
                                        <h3>Interaktion</h3>
                                        <p>Udnyttelse af det digitales interaktive natur med unikke muligheder i engagement gennem bruger drevet grænseflader.</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <h2>Tryk grafik og printdesign</h2>
                                    <p class="lead">En god måde at øge din virksomheds troværdighed og skabe
                                        opmærksomhed, er ved hjælp af trykte medier. Med en allerede solid digital
                                        markedsføring i din virksomhed, er det logiske valg at markedsføre sig gennem
                                        trykte medier, og det kan jeg hjælpe med her ved Geek Media. Hvis du søger en
                                        samarbejdspartner der er professionel, idérig, erfaren og har kompetencerne,
                                        er det mig du skal vælge til dit næste projekt.</p>
                                    <p class="lead">
                                        Jeg skaber et design der er i overensstemmelse med din virksomheds identitet
                                        og idealer, hvilket er det absolut vigtigste når der skal laves trykdesign der
                                        skaber resultater. Jeg leverer et produkt der er interessant for kunden, samt
                                        får din virksomhed til at fremstå som kompetent og kvalitetsbevidst overfor
                                        kunden. Jeg arbejder målrettet på at skabe den løsning der giver mest interesse
                                        fra den ønskede målgruppe.</p>
                                    <p class="lead">
                                        Jeg er behjælpelige i alle faser af arbejdsprocessen, fra idé til godkendelse
                                        af layout til tryk.
                                    </p>
                                </div>
                                <div class="col-sm-8 col-sm-offset-2">
                                    <div class="col-sm-6 col-md-4 services" id="print_1">
                                        <h3>Design strategi</h3>
                                        <p>Målbevidste anbefalinger baseret på konstruktiv design konsultation, brand analyse og gennemgang af målsætninger.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="print_2">
                                        <h3>Løsninger</h3>
                                        <p>
                                            Leverer løsning i bredt spektrum som passer til jeres mål og målgruppe, fra plakater til bøger.
                                        </p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="print_3">
                                        <h3>User experience</h3>
                                        <p>Løsningsforslag til at fremme engagement gennem enkle, intuitive og dejlige oplevelser på tværs af alle løsninger.</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <h2>Visuel Identitet er ikke blot ét flot design</h2>
                                    <p class="lead">En virksomheds visuelle identitet er meget vigtig, da den præsenter
                                        virksomheden til omverdenen og derfor skal afspejle virksomhedens kvalitet og
                                        branche. Den visuelle identitet skal bruges som et værktøj til at skabe
                                        fundamentet for virksomhedens positionering, branding og markedsføring.</p>
                                    <p class="lead">
                                        For at skabe en god visuel identitet, kræver det indsigt i virksomheden, dens
                                        produkter, værdier og vision. Der skal skabes et overordnet tema, alle de
                                        grafiske elementer afhænger af, hvilket både kræver erfaring og faglig
                                        kompetence.</p>
                                    <p class="lead">
                                        En visuel identitet skal være integreret dybt med virksomhedens allerede lagte
                                        strategi, og være med til at underbygge denne. Den skal afspejle niveauet og
                                        kvaliteten af produkter og ydelser fra virksomheden. Den skal være med til at
                                        støtte op om alle medier og tiltag hos virksomheden, da den røde tråd skal
                                        fremhæves, for at gennemskueliggøre virksomheden for kunden.
                                    </p>
                                    <p class="lead">
                                        Jeg skaber din komplette identitet, med grafik og temaer der kan genbruges på
                                        alle dine medier, som f.eks. brevpapir, vi-sitkort osv.
                                    </p>
                                </div>
                                <div class="col-sm-8 col-sm-offset-2">
                                    <div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.2s" id="brand_1" style="visibility: visible; animation-delay: 0.2s; animation-name: zoomIn;">
                                        <h3>Research &amp; analyse</h3>
                                        <p>Dybdegående brand opdagelse der dækker markedsundersøgelser, demografi, målgrupper og konkurrentanalyse.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.4s" id="brand_2" style="visibility: visible; animation-delay: 0.4s; animation-name: zoomIn;">
                                        <h3>Holistiske strategier</h3>
                                        <p>
                                            Skitsere retningen for hele projektet, herunder eksterne kommunikationer som social og digital markedsføring.
                                        </p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.6s" id="brand_3" style="visibility: visible; animation-delay: 0.6s; animation-name: zoomIn;">
                                        <h3>Fremtids sikring</h3>
                                        <p>Rådgivning om eksisterende teknologier og anbefalinger, der skaber muligheder for at udvikle sig med det digitale rum.</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <h2>Følg dine besøgende fra start til slut</h2>
                                    <p class="lead">Hvis din side har kommercielle mål, altså skaffer henvendelser til din
                                        forretning, eller direkte salg, så er det vigtigt at opsætte mål i Google Analytics,
                                        så du kan spore hvor dine konverteringer kommer fra.</p>
                                    <p class="lead">
                                        Google Analytics er et af mange analyseværktøjer der kan bruges til at forstå brugernes
                                        adfærd på din hjemmeside. Du kan f.eks. finde ud af hvilke trafikkilder der bidrager til
                                        den største omsætning, hvilke søgeord der leverer mest trafik eller noget helt andet.
                                        Adfærd på nettet er gennemsigtigt og der er næsten ingen grænser for hvad du kan få svar
                                        på med den rette viden og opsætning.</p>
                                    <p class="lead">
                                        Med det rette datagrundlag i Google Analytics kan du få den fornøden viden til at foretage
                                        konverteringsoptimering af din hjemmeside, sikre at du bruger dit annoncebudget optimalt ved
                                        ikke at spilde dem på værdiløse trafikkilder og generelt set få en langt bedre forståelse for
                                        dine brugers købsadfærd i kombination med en effektiv søgeordsanalyse der giver indsigt om deres søgeadfærd.
                                    </p>
                                    <p class="lead">
                                        Den optimale opsætning af Google Analytics afhænger af din forretningsstrategi, mål og ønsker.
                                        På baggrund af den tekniske opsætning kan der opsættes løbende rapporter og såkaldte dashboards
                                        for at følge udviklingen og spotte eventuelle trends som du kan målrette med din markedsføring.
                                    </p>
                                    <p class="lead">Jeg hjælpe dig med at alt indenfor opsætning, rapportering og webanalyse med Google Analytics.</p>
                                </div>
                                <div class="col-sm-8  col-sm-offset-2">
                                    <div class="col-sm-6 col-md-4 services" id="analyt_1">
                                        <h3>Præsentation</h3>
                                        <p>Se alle dataene i nemme og overskuelige diagrammer efter eget valg.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="analyt_2">
                                        <h3>Segmentering</h3>
                                        <p>Segmenter og filtrer alle dine data så du kun fokusere på din ønskede målgruppe.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="analyt_3">
                                        <h3>Følg dem</h3>
                                        <p>Følg dine besøgende hele vejen igennem sitet fra ankomst til afgang.</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <?php include("includes/cta.php");?>
</div>
<?php require("includes/footer.php");?>
<?php require("includes/scripts.php");?>
</body>
</html>
