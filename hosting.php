<?php
//Header+OG setup
$siteName = "Geek Media";
$title = "Moderne hosting løsninger";
$description = "Giv din hjemmeside de absolut bedste vilkår, med en høj stabilitet og super hurtige svartider med hosting hos Geek Media.";
$keywords = "";
$image = "images/header/hero5.jpg";
?>
<!DOCTYPE html>
<html>

<head>
    <?php include("includes/meta.php")?>
    <?php require("includes/styles.php")?>
</head>
<body>
<?php include("includes/preloader.php");?>
<?php include("includes/nav.php");?>
<div class="main-container">
    <header class="page-header">
        <div class="background-image-holder">
            <img class="background-image" alt="Background Image" src="/images/header/hero5.jpg">
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <span class="text-white alt-font">Online muligheder</span>
                    <h1 class="text-white">Hosting</h1>
                    <p class="text-white lead">Moderne hosting løsninger<br> til den moderne virksomhed.</p>
                </div>
            </div>
        </div>
    </header>
    <section class="milestones services-selector bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                    <h1>Hosting services</h1>
                    <p class="lead">
                        Rigtig mange overser desværre vigtigheden af, at ens hjemmeside eller webshop er sikret med en høj oppetid, sikkerhed mod hacking,
                        effektive backuprutiner, hurtige svartider osv. der alle er elementer, som har stor indvirkning på den oplevelse som besøgende eller
                        kunder erfarer. Der er investeret rigtig store summer i det datacenter og netværk hvor jeg hoster fra, for netop at optimere
                        slutbrugeroplevelsen når de besøger et webhotel, som er hostet hos mig.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="container">
                    <ul class="services-tabs clearfix">
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-layers"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Webhosting</h5>
                                <span>Moderne webhotel</span>
                                <span class="sub">LÆS MERE</span>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-global"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Domæner</h5>
                                <span>Tag dit brand online</span>
                                <span class="sub">LÆS MERE</span>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-envelope"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Hosted Exchange</h5>
                                <span>Tilgå mail overalt</span>
                                <span class="sub">LÆS MERE</span>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-adjustments"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Web monitorering</h5>
                                <span>Optimer din oppetid</span>
                                <span class="sub">LÆS MERE</span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="container">
                    <ul class="services-content">
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <h2>Det rigtige valg</h2>
                                    <p class="lead">Langt de fleste virksomheder, organisationer eller endda
                                        private har i dag en online profil i form af en hjemmeside, blog,
                                        webshop eller lignende. Derfor forventer langt de fleste kunder
                                        også, at kunne finde dig eller din virksomhed på internettet, at
                                        kunne fremsøge dine produkter, danne sig et indtryk af din
                                        virksomhed eller have et sted at kontakte dig, når de er klar til at
                                        initiere et salg.</p>
                                    <p class="lead">
                                        Webhotellerne jeg tilbyder er ikke udelukkende bygget op efter,
                                        at kunne tilbyde dig den absolut laveste pris. Rigtig mange overser
                                        desværre vigtigheden af, at ens hjemmeside eller webshop
                                        er sikret med en høj oppetid, sikkerhed mod hacking, effektive
                                        backuprutiner, hurtige svartider osv. der alle er elementer, som
                                        har stor indvirkning på den oplevelse som besøgende eller
                                        kunder erfarer.</p>
                                    <p class="lead">
                                        Der er investeret rigtig store summer i det datacenter og netværk
                                        hvor jeg hoster fra, for netop at optimere slutbruger-oplevelsen
                                        når de besøger et webhotel, som er hosted hos mig.
                                    </p>
                                    <p class="lead">
                                        Derfor vil du heller ikke opleve, at serverne fyldes op til
                                        bristepunktet, således at trafikken fra andre brugere vil få
                                        indflydelse på ydeevnen fra dit webhotel, som man så ofte ser det
                                        hos lavprisselskaberne.
                                    </p>
                                    <a href="/priser" class="btn btn-primary">SE PRISER</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <h2>Vælg det rigtige domæne</h2>
                                    <p class="lead">For at komme i gang med at lave en hjemmeside, så skal I finde
                                        et domænenavn. Det lyder meget enkelt, men for mange kan det være et af de
                                        sværeste og mest tidskrævende skridt i processen.</p>
                                    <p class="lead">At vælge det helt rigtig domæne til jeres website/shop er
                                        ingen spøg og det er noget der skal tænkes grundigt over. Vælger I forkert,
                                        så kan I risikere at ærgre jer gul og grøn på et senere tidspunkt.</p>
                                    <p class="lead">Der skal bl.a. tages stilling til følgende ting:</p>
                                    <p class="lead">
                                        &nbsp;&nbsp;- Skal domænet være .dk eller .com (eller noget helt tredje)?<br>
                                        &nbsp;&nbsp;- Skal man have et søgeord i domænet eller skal det være generisk?<br>
                                        &nbsp;&nbsp;- Skal domænet være kort eller må det godt være lidt længere?<br>
                                        &nbsp;&nbsp;- Skal i have flere forskellige domæner?
                                    </p>
                                    <p class="lead">Jeg guider jer hele processen fra de første tanker og beslutninger
                                    omkring domænevalget og hele vejen til bestilling og opsætning af domænet på jeres
                                    hostede webløsning.</p>
                                </div>
                            </div>
                        </li>
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <h2>Adgang uanset hvor og hvilken enhed du er på</h2>
                                    <p class="lead">Hosted Exchange er et uundværligt værtøj for virksomheden,
                                        der ønsker at effektivisere sin kommunikation og udstråle
                                        professionalisme overfor sine kunder.</p>
                                    <p class="lead">
                                        Det giver dig og dine kollegaer mulighed for at synkronisere og
                                        dele jeres e-mails, kontakter og kalendere med hinanden og på
                                        tværs af jeres forskellige enheder såsom mobiltelefonen, tablet,
                                        arbejdscomputeren og hjemme PC-en. Hvis blot du har adgang til
                                        internettet, vil du med webmail, desuden kunne tilgå dine e-mails
                                        fra enhver lokation</p>
                                    <p class="lead">
                                        Med Hosted Exchange kan du oprette en personlig mailboks
                                        og mailadresse til alle jeres medarbejdere. Mailadresserne vil
                                        desuden inkludere jeres domænenavn, så jeres kunder aldrig
                                        vil være i tvivl om hvem en e-mail kommer fra. Der er ingen
                                        begrænsninger og du kan selv vælge og løbende tilpasse hvor
                                        mange konti du skal bruge og hvor meget plads de hver især skal
                                        have.
                                    </p>
                                    <p class="lead">
                                        Systemet sørger for at opfange det meste spam og de fleste vira
                                        med et integreret spamfilter. Derudover inkluderer løsningen
                                        automatisk backup, som bliver gemt i det moderne og topsikre
                                        datacenter, der også har ansvaret for hostingen af din løsning.
                                        Herved vil du aldrig miste dine data eller e-mails, blot fordi din
                                        telefon eller computer forulykker.
                                    </p>
                                    <a href="/priser" class="btn btn-primary">SE PRISER</a>
                                </div>
                            </div>
                        </li>
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <h2>Hurtigere respons på nedbrud</h2>
                                    <p class="lead">Den almindelige internetbruger bliver mere og mere utålmodig
                                        og er ikke villig til at vente ret længe, før vedkommende opgiver sit
                                        ærinde, især hvis der findes alternative sites. Det er derfor vigtigt
                                        hele tiden at have fokus på dit sites performance og tilgængelighed
                                        for at sikre brugernes tilfredshed. Målet er ikke kun at sikre 100 %
                                        oppetid, men i lige så høj grad at reducere svartiderne og handle
                                        hurtigt ved performanceproblemer.
                                    </p>
                                    <p class="lead">
                                        GeekCheck er en internetbaseret service der giver overblik
                                        over jeres web performance 24/7/365. Systemet kræver ingen
                                        installation af hverken soft- eller hardware. På den måde sparer
                                        du kostbare interne ressourcer til at udvikle og vedligeholde
                                        systemer.</p>
                                    <p class="lead">
                                        Ved at foretage en forespørgsel på sitet udefra, simulerer
                                        GeekCheck en rigtig brugers bevægelser i webmiljøet. Dette er
                                        en af de store forskelle fra øvrige målesystemer, der som oftest
                                        måler på indersiden af firewall’en og for det meste alene på
                                        teknisk oppetid. Når målingen kun foretages internt, fortæller den
                                        således ikke noget om hvordan ISP og firewall komponenterne
                                        performer. Ligesom de heller ikke måler på hele indhol-det på en
                                        webside, og derved ikke giver et retvisende billede af svartiderne.
                                    </p>
                                    <p class="lead">
                                        Når GeekCheck finder performanceproblemer, registreres de i
                                        en overskuelig fejllog, og der sendes straks en e-mailalarm om
                                        problemet til de brugere, der er opsat for det pågældende check.
                                        Det betyder, at vi kan få rettet fejlen hurtigt.
                                    </p>
                                    <a href="/priser" class="btn btn-primary">SE PRISER</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <?php include("includes/cta.php");?>
    <section class="feature-selector bg-white" >
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                    <h1>Hvorfor vælge hosting services igennem mig?</h1>
                </div>
            </div>
            <div class="row">
                <ul class="selector-tabs clearfix">
                    <li class="clearfix text-primary col-md-3 col-sm-6 active">
                        <i class="icon icon-linegraph"></i>
                        <span>Høj performance</span>
                    </li>

                    <li class="clearfix text-primary col-md-3 col-sm-6">
                        <i class="icon icon-lock"></i>
                        <span>Udvidet sikkerhed</span>
                    </li>

                    <li class="clearfix text-primary col-md-3 col-sm-6">
                        <i class="icon icon-tools-2"></i>
                        <span>Uovertruffen support</span>
                    </li>

                    <li class="clearfix text-primary col-md-3 col-sm-6">
                        <i class="icon icon-clock"></i>
                        <span>99.9% oppetid</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container">
            <ul class="selector-content">
                <li class="clearfix active">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <h2>State-of-the-art datacenter</h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-5">
                            <img src="/images/hosting/hosting-pc.png" alt="Top moderne datacenter" />
                        </div>

                        <div class="col-sm-7">
                            <p class="lead">
                                Datacenteret er tilkoblet 2 x 10 Gbit fiber forbindelser - den ene til TDC og den anden til Telia.
                                Eftersom TDC og Telia benytter hver deres fiberring, er hele ruten mellem dig som kunde og datacenteret desuden redundant - hvilket betyder,
                                at uanset hvor i kæden der opstår problemer, så vil din hostede løsning fortsat fungere uden problemer.
                            </p>
                        </div>
                    </div>
                </li>
                <li class="clearfix">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <h2>Slap af - 24 timers vagtordning sikre dine data hele året rundt</h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 text-center">
                            <img src="/images/hosting/hacking.png" alt="Beskyttet imod hacking" class="space-bottom-small" />
                            <p class="lead space-bottom-small">
                                Netværksinfrastrukturen er optimeret til at kunne modstå alle former for hackerangreb (heriblandt DoS- og DDoS angreb)
                                på en sådan måde, at du som kunde aldrig vil komme til at mærke til det
                            </p>
                        </div>

                        <div class="col-sm-4 text-center">
                            <img src="/images/hosting/firewall.png" alt="Firwall & Antivira" class="space-bottom-small" />
                            <p class="lead space-bottom-small">
                                Alle arbejdsmaskiner og servere er udstyret med firewalls og antivirus software, der har til hensigt at blokere alle former for vira mv.
                            </p>
                        </div>
                        <div class="col-sm-4 text-center">
                            <img src="/images/hosting/alarmering.png" alt="Fejl alarmering" class="space-bottom-small" />
                            <p class="lead space-bottom-small">
                                Alle parametre i datacenteret bliver overvåget, og ved de mindst udsving bliver vagtpersonalet alarmeret og påbegynder fejlfinding på systemet.
                            </p>
                        </div>
                    </div>
                </li>
                <li class="clearfix">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <h2>Uovertruffen support - Jeg er der når du har brug for hjælp</h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-5">
                            <img src="/images/hosting/hosting-pc.png" alt="Top moderne datacenter" />
                        </div>

                        <div class="col-sm-7">
                            <p class="lead">
                                Support til alle mine kunder er ekstremt vigtigt for mig, og derfor kan du få hjælp via min Live Chat alle hverdage fra 8-17. Er dit problem opstået udenfor dette
                                tidsrum kan du finde mig og stil supportspørgsmål på mine sociale medier hvor du vil kunne få fat på mig frem til kl. 21 alle hverdage.
                            </p>
                            <a href="https://www.facebook.com/GeekMediaDK" class="btn btn-primary" target="_blank">FACEBOOK</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="https://twitter.com/GeekmediaDK" class="btn btn-primary" target="_blank">TWITTER</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="https://dk.linkedin.com/in/cmeldgaard" class="btn btn-primary" target="_blank">LINKEDIN</a>
                            <br>
                            <br>
                            <p class="lead">Har du en Geek Partner aftale vil du derudover have adgang til at kontakt mig på min supportmail, og have adgang til ticket system (Under udvikling).</p>
                        </div>
                    </div>
                </li>
                <li class="clearfix">
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <h2>SLA på 99,9% hver måned</h2>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-7">
                            <p class="lead">
                                Alle webservere drives på højtydende Windows og Linux/unix servere. Der tages backup af alle data, og systemerne er overvåget 24 timer i døgnet, hvilket sikrer at webhotellet har en oppetid på over 99,9%
                            </p>
                        </div>
                        <div class="col-sm-5">
                            <img src="/images/hosting/uptime.png" alt="Top moderne datacenter" />
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </section>
    <?php include("includes/newsletterForm.php"); ?>
</div>
<?php require("includes/footer.php");?>
<?php require("includes/scripts.php");?>
</body>
</html>