<?php
//Header+OG setup
$siteName = "Geek Media";
$title = "Den rigtige markedsføring";
$description = "Bliv guidet igennem alle facetterne af markedsføring hos Geek Media.";
$keywords = "";
$image = "images/header/hero12.jpg";
?>
<!DOCTYPE html>
<html>

<head>
    <?php include("includes/meta.php")?>
    <?php require("includes/styles.php")?>
</head>
<body>
<?php include("includes/preloader.php");?>
<?php include("includes/nav.php");?>
<div class="main-container">
    <header class="page-header">
        <div class="background-image-holder">
            <img class="background-image" alt="Background Image" src="/images/header/hero12.jpg">
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <span class="text-white alt-font">Ram kunderne</span>
                    <h1 class="text-white">Markedsføring</h1>
                    <p class="text-white lead">Bliv guidet igennem alle facetterne<br> af markedsføring hos Geek Media.</p>
                </div>
            </div>
        </div>
    </header>
    <section class="milestones services-selector bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                    <h1>Den rigtige markedsføring</h1>
                    <p class="lead">Uden markedsføring kommer jeres virksomhed ingen vegne! Bliv guidet igennem alle
                        facetterne af markedsføring hos mig, fra brugen af sociale medier til gennemgang af alle
                        jeres præsentationsmaterialer.</p>
                </div>
            </div>
            <div class="row">
                <div class="container">
                    <ul class="services-tabs clearfix">
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-facebook"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Sociale Medier</h5>
                                <span>Connect med kunderne</span>
                                <span class="sub">LÆS MERE</span>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-camera"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Billeder/Fotografering</h5>
                                <span>Det visuelle brand</span>
                                <span class="sub">LÆS MERE</span>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-envelope"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Nyhedsbrev</h5>
                                <span>Del dine historier</span>
                                <span class="sub">LÆS MERE</span>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-newspaper"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Præsentations materiale</h5>
                                <span>Afspejl hvem i er</span>
                                <span class="sub">LÆS MERE</span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="container">
                    <ul class="services-content">
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <h2>Bliv set på de sociale medier</h2>
                                    <p class="lead">
                                        På de sociale medier kan I skabe og udbygge relationer mellem jer og jeres
                                        kunder. På de sociale medier har I alle muligheder for effektfulde tiltag og for
                                        at nå en eksponentielt voksende målgruppe.
                                    </p>
                                    <p class="lead">
                                        Sociale medier gør det muligt for jeres kunder nemt at knytte sig til jeres
                                        virksomhed online, modtage nyheder og tilbud, samt vise deres venner, at jeres
                                        brand og produkter er troværdige.
                                    </p>
                                    <p class="lead">
                                        Næsten 70 % af alle danske virksomheder bruger aktivt sociale medier i
                                        forskellige udstrækning. Dette gør de for at holde på deres kunder, opbygge
                                        loyalitet, samt øge indtjeningen ved kampagner.
                                    </p>
                                    <p class="lead">
                                        Derfor skal der jeres sociale medier være lige så up-to-date som resten af
                                        jeres grafiske profil. Jeg kan tilbyde at optimere på jeres sociale profiler
                                        og forbinde jeres sociale medier på tværs af hinanden.
                                    </p>
                                    <p class="lead">
                                        I Danmark bruger vi typisk LinkedIn, Twitter, Facebook, Instagram, Pinterest
                                        og Google+. Har jeres virksomhed ikke nogen af disse sociale profiler kan jeg
                                        hjælpe med den indledende opsætning, samt rådgivning omkring hvilke af disse
                                        medier der vil være nyttige for netop jeres virksomhed.
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <h2>Et billede siger mere end 1000 ord</h2>
                                    <p class="lead">Lige så vigtigt at jeres visuelle identitet og det grafiske design
                                        af jeres præsentationsmateriale ser professionelt ud, lige så vigtigt er det
                                        at billederne i viser på fx jeres website udstråler det samme.</p>
                                    <p class="lead">
                                        Så skal i have fundet nye billeder til jeres website eller visuelle identitet,
                                        så <a href="/kontakt">tag fat på mig</a> og få input til nye muligheder omkring
                                        jeres billeder.</p>
                                    <p class="lead">
                                        Jeg kan downloade professionelle billeder fra en række databaser på nettet eller
                                        vi kan få taget jeres helt egne unikke billeder, som skaber en større personlighed
                                        og derved også sikre at ingen andre af jeres konkurrenter har disse
                                        billeder på deres website fx.
                                        <br>
                                        <br>
                                        <img src="/images/extLogo/pixabay.png" width="180" alt="pixabay logo" style="margin-right: 15px" />
                                        <img src="/images/extLogo/raumrot.png"  width="180" alt="Raumrot logo"/>
                                        <img src="/images/extLogo/deathtostock.png"  height="75" alt="Death to the stock photo logo"/>
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <h2>Del dine historier direkte med kunderne</h2>
                                    <p class="lead">Nyhedsbreve er en effektivt og relativ billig måde at styrke
                                        markedsføringen og forholdet til jeres kunder. Uanset hvilket produkt eller
                                        service I leverer, er et nyhedsbrev den direkte vej til nuværende eller
                                        kommende kunder, der allerede har vist interesse for jeres virksomhed, og
                                        derfor værdifulde (potentielle) kunder.</p>
                                    <p class="lead">
                                        Jeg tilbyder forskellige former for løsninger. I kan vælge at få hjælp til
                                        opsætningen af systemet. Her vil jeg opsætte en konto hos Mailchimp, design
                                        et template som passer til jeres virksomhed samt en hurtig gennemgang af
                                        hvordan i sender nyhedsbrevet igennem systemet. I kan også vælge at overlade
                                        opsætning og udsendelse til mig. I skal da blot sende jeres tekst og billeder,
                                        hvorved jeg vil opsætte nyhedsbrevet og sende det til godkendelse hos jer,
                                        inden det sendes ud. Nemmere bliver det ikke!
                                    </p>
                                    <p class="lead">
                                        Er jeres liste med emailadresser gammel, og ikke blevet opdateret i sin levetid
                                        kan det være nødvendigt med en rensning af emailadresser, for at undgå at mails
                                        fra jer ryger i spam-mappen hos jeres kunder. Her kan jeg tilbyde en one-time
                                        cleaning af listen samt et månedligt abonnement hvor jeres maillister bliver
                                        analyseret dagligt for nye og ødelagte adresser.
                                    </p>
                                    <p class="lead">
                                        Ligegyldigt hvilken løsning I vælger, vil I have adgang til Mailchimp, hvor
                                        i selv kan følge med i, hvor succesfuldt jeres nyhedsbrev er, og hvor mange der
                                        åbner og klikker på hvert af de enkelte links i nyhedsbrevet. Det er et rigtig
                                        godt redskab til at forbedre jeres fremtidige nyhedsbreve.
                                    </p>
                                    <p class="lead">
                                        Jeg bruger veletableret nyhedsbrevssystemer til at tillade fuld
                                        kontrol over enhver kampagne, sikre effektivitet, fleksibilitet og
                                        brugervenlighed. Mine foretrukne systemer omfatter Mailchimp og DataValidation
                                        for deres brugervenlighed og fleksibilitet.
                                        <br>
                                        <br>
                                        <img src="/images/extLogo/datavalidation.png" width="300" alt="Datavalidation.com logo" style="margin-right: 15px" />
                                        <img src="/images/extLogo/mailchimp.png"  width="300" alt="Mailchimp logo"/>
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <h2>Find den røde tråd</h2>
                                    <p class="lead">
                                        Brochurer, bøger og magasiner skal lige som alt andet grafisk arbejde afspejle
                                        afsenderens identitet. Samtidig skal det være indbydende og ved hjælp fra
                                        billeder, farver, typografi og udformning, sende de rigtige signaler og
                                        budskaber til modtageren.
                                    </p>
                                    <p class="lead">
                                        Derfor skal jeg præsentationsmaterialet også løbende opdateres og tilpasses
                                        efterhånden som jeres virksomhed udvikler sig.
                                    </p>
                                    <p class="lead">
                                        Lad mig hjælpe jer, uanset om det for redesign af jeres præsentationsmateriale
                                        eller blot for at få et nyt syn på jeres eksisterende materiale og hvilke
                                        muligheder I har med det.
                                    </p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <?php include("includes/cta.php");?>
</div>
<?php require("includes/footer.php");?>
<?php require("includes/scripts.php");?>
</body>
</html>