<?php
//Header+OG setup
$siteName = "Geek Media";
$title = "Ups! Du er vist på afveje";
$description = "Lej en freelance samarbejdspartner til at håndtere jeres virksomheds design og web opgaver i Esbjerg og Varde.";
$keywords = "markedsføring, reklame, marketing, grafisk design, grafisk, facebook, sociale medier, projektledelse, Varde, Esbjerg, messer, kundearrangement, arrangement, hjemmeside, web, seo, søgeordsoptimering, hosting, print, print design";
$image = "images/FB-Cover-01.png";
$type = "website";
?>
<!DOCTYPE html>
<html>

<head>
    <?php include("includes/meta.php")?>
    <?php require("includes/styles.php")?>
</head>
<body>

<?php include("includes/preloader.php");?>

<?php include("includes/nav.php");?>

<div class="main-container">
  <section class="no-pad error-page bg-primary bg-secondary-1 fullscreen-element first-child" style="height: 1113px;">
      <div class="container align-vertical" style="padding-top: 335.25px;">
        <div class="row">
          <div class="col-sm-12 text-center">
            <i class="icon icon-compass"></i>
            <h1 class="jumbo">404</h1>
            <h1><strong>Åh nej, ser ud til at jeg har ført dig på vildspor!</strong><br>Lad os komme tilbage på sporet...</h1>
            <a href="/" class="btn btn-primary btn-white">Tag mig hjem</a>
            <a href="mailto:hallo@geekmedia.dk?subject=404 Fejl&body=Fejl data:%0D%0A URL: <?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>%0D%0AReferer: <?php echo $_SERVER['HTTP_REFERER'];?> " class="btn btn-primary btn-white btn-text-only">Anmeld dette</a>
          </div>
        </div>
      </div>
    </section>
</div>
<?php require("includes/scripts.php");?>
</body>
</html>
