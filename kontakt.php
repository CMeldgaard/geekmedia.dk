<?php
//Header+OG setup
$siteName = "Geek Media";
$title = "Kontakt mig nu!";
$description = "Kontakt Geek Media. Ring eller skriv til mig, og lad os aftale et møde omkring muligheden for at bruge mig som freelance samarbejdspartner";
$keywords = "";
$image = "images/header/hero8.jpg";
?>
<!DOCTYPE html>
<html>

<head>
    <?php include("includes/meta.php")?>
    <?php require("includes/styles.php")?>
</head>
<body>

<?php include("includes/preloader.php");?>

<?php include("includes/nav.php");?>

<div class="main-container">
    <header class="page-header">
        <div class="background-image-holder">
            <img class="background-image" alt="Background Image" src="/images/header/hero8.jpg">
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <span class="text-white alt-font">Komme i kontakt</span>
                    <h1 class="text-white">Kontakt mig</h1>
                    <p class="text-white lead">Arbejder ud fra Esbjerg<br> med kunder i hele landet</p>
                </div>
            </div>
        </div>
    </header>
    <section class="pure-text-contact">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 text-center">
                    <span class="sub alt-font">Jeg modtager nye kunder</span>
                    <h1><strong>Jeg er beliggende i Esbjerg, og jeg arbejder med kunder fra hele landet.</strong></h1>
                    <i class="icon icon-map icon-jumbo"></i>
                    <i class="icon icon-bike icon-jumbo"></i>
                    <i class="icon icon-streetsign icon-jumbo"></i>
                </div>
            </div>

        </div>
    </section>
    <section class="contact-thirds">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h1>Kontakt mig<br>
                        Vil elske at høre fra dig
                    </h1>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <h5>Start af projekt</h5>
                    <p>
                        Tak fordi du er interesseret i at arbejde sammen med Geek Media. At komme igang er meget nemt
                        - blot udfylde de tomme felter i formularen, send det til mig og jeg vil vende tilbage indenfor
                        én arbejdsdag.

                    <p>
                        Hvor spændende!
                    </p>
                </div>

                <div class="col-sm-4 text-center">
                    <h5>Detaljer</h5>
                    <p class="lead ">
                        +45 3160 6013<br>
                        hallo@geekmedia.dk<br>
                        <br>
                        Rolfsgade 154 st. tv.<br>
                        6700 Esbjerg
                    </p>
                </div>

                <div class="col-sm-4">
                    <h5>Send en besked</h5>
                    <div class="form-wrapper clearfix">
                        <form class="form-contact email-form">
                            <div class="inputs-wrapper">
                                <input class="form-name validate-required" type="text" placeholder="Dit navn" name="name">
                                <input class="form-name" type="text" placeholder="Firmanavn" name="companyname">
                                <input class="form-email validate-required validate-email " type="text" placeholder="Din e-mail adresse" name="email" >
                                <input class="form-email" type="text" placeholder="Dit telefon nummer" name="phone" >
                                <textarea class="form-message validate-required" name="message" placeholder="Din besked"></textarea>
                            </div>
                            <input type="submit" class="send-form" value="Send Besked">
                            <div class="form-success">
                                <span class="text-white">Besked sendt - Mange tak, jeg vil vende tilbage hurtigst muligt</span>
                            </div>
                            <div class="form-error">
                                <span class="text-white ">Udfyld venligst alle felterne</span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php include("includes/newsletterForm.php");?>
</div>
<?php require("includes/footer.php");?>
<?php require("includes/scripts.php");?>
</body>
</html>