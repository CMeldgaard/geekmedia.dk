<?php
//Header+OG setup
$siteName = "Geek Media";
$title = "Din nye webpartner";
$description = "Gør det nemt at vedligeholde en dynamisk hjemmeside uanset dit it-niveau og færdigheder.";
$keywords = "";
$image = "images/header/hero7.jpg";
?>
<!DOCTYPE html>
<html>

<head>
    <?php include("includes/meta.php")?>
    <?php require("includes/styles.php")?>
</head>
<body>
<?php include("includes/preloader.php");?>
<?php include("includes/nav.php");?>
<div class="main-container">
    <header class="page-header">
        <div class="background-image-holder">
            <img class="background-image" alt="Background Image" src="/images/header/hero7.jpg">
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <span class="text-white alt-font">Gå online</span>
                    <h1 class="text-white">Web</h1>
                    <p class="text-white lead">Tag din markedsføring online<br> og mød dine kunder online.</p>
                </div>
            </div>
        </div>
    </header>
    <section class="milestones services-selector bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                    <h1>Web løsninger</h1>
                    <p class="lead">Du har få sekunder til at præsentere din virksomhed på internettet
                        og derfor skal din hjemmeside være moderne, gennemtænkt og
                        brugervenlig – ellers er brugeren væk igen sekundet efter.</p>
                    <p class="lead">
                        Din hjemmeside er din personlighed på internettet og derfor skal
                        den være professionelt bearbejdet og samtidig skal navigationen
                        være så intuitiv, at dine kunder hurtigt kan finde den ønskede
                        information.</p>
                </div>
            </div>
            <div class="row">
                <div class="container">
                    <ul class="services-tabs clearfix">
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-browser"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Website m. CMS</h5>
                                <span>Online markedsføring</span>
                                <span class="sub">LÆS MERE</span>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-basket"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>eCommerce</h5>
                                <span>Fra mursten til nettet</span>
                                <span class="sub">LÆS MERE</span>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-mobile"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Mobil</h5>
                                <span>Fra platform til platform</span>
                                <span class="sub">LÆS MERE</span>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-linegraph"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Google Analytics</h5>
                                <span>Optimer din oppetid</span>
                                <span class="sub">LÆS MERE</span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="container">
                    <ul class="services-content">
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <h2>Gør din markedsføring online</h2>
                                    <p class="lead">
                                        Den gode hjemmeside er hele tiden under forandring. Brugerne bliver lokket til
                                        med friskt og dynamisk indhold, søgeordene i teksterne bliver løbende
                                        opdateret og funktioner som blog, debatforum, billedgalleri, kalender og
                                        meget mere er dybt integreret med hjemmesidens struktur og indhold
                                    </p>
                                    <p class="lead">
                                        Desværre er der rigtig mange hjemmesider, som ikke følger den ovenstående
                                        forskrift, da det ofte er besværligt og meget ressourcekrævende at skulle
                                        holde en hjemmeside opdateret - specielt for uerfarne it-brugere. Det er
                                        den problematik, som jeg forsøger at komme til livs med ved at opsætte dit
                                        website i CMS.
                                    </p>
                                    <p class="lead">
                                        Uanset om du er uddannet grafiker, webudvikler eller på anden vis sidder med
                                        ansvaret for en virksomheds hjemmeside, vil CMS give dig værktøjerne til at
                                        etablere og vedligeholde et professionelt og dynamisk website
                                    </p>
                                    <p class="lead">
                                        CMS betyder bedre hjemmesider, fordi det bliver hurtigt og nemt for alle
                                        medarbejdere, at udvikle, justere og opdatere den eller teste nye idéer,
                                        for derefter at måle effekten i et statistikprogram.
                                    </p>
                                </div>
                                <div class="col-sm-8  col-sm-offset-2">
                                    <div class="col-sm-6 col-md-4 services"id="web_1">
                                        <h3>Website Strategi</h3>
                                        <p>Analyse af mål til at give grundige anbefalinger omkring det tekniske, designmæssige og UX overvejelser.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="web_2">
                                        <h3>UI DESIGN</h3>
                                        <p>Udvikling af høj kvalitetes koncepter der leverer mål påtværs af alle platforme og får websitet til at se godt ud</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="web_3">
                                        <h3>UX DESIGN</h3>
                                        <p>Fokus på de vigtigste målgrupper for at levere målrettede UX strategier, brugervenlighed, engagement og glæde.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="web_4">
                                        <h3>RESPONSIVE DESIGN</h3>
                                        <p>Problemfri fleksible rammer for at give optimale oplevelser på tværs af alle enheder, fra smartphones til desktops.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="web_5">
                                        <h3>CM SYSTEM</h3>
                                        <p>Robust og brugervenlig systemer til at styre websitet indhold og effektiv strukturering, som er skalerbar til netop dit projekts behov.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="web_6">
                                        <h3>HTML5</h3>
                                        <p>Udforskning af interaktion og moderne teknologier for at få mest ud af digitale platforme og tilskynde engagement.</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <h2>Flyt din butik hjem til kunderne</h2>
                                    <p class="lead">Danskerne har i stor stil taget e-handelskonceptet til sig, hvilket
                                        betød at den årlige omsætning i Danmark udgjorde kr. 80 mia. i
                                        2014. Det er 17% højere end året forinden, som var den højeste
                                        nogenside, og den positive tendens peger alt andet lige på at
                                        fortsætte.</p>
                                    <p class="lead">
                                        Desto vigtigere bliver det derfor også for både eksisterende detail
                                        butikker og nystartede virksomheder at være til stede online, når
                                        de købelystne danske forbrugere kommer forbi.</p>
                                    <p class="lead">
                                        Når din eCommerce løsning skal designes tages der udgangspunkt i, at det skal være nemt og intuitivt for kunderne at anvende
                                        shoppen og finde de ønskede produkter - og alle skal kunne være
                                        med, uanset deres brugerniveau.
                                    </p>
                                    <p class="lead">
                                        Et primært fokus derudover er, at minimere de økonomiske
                                        omkostninger og tiden der forbindes med, at opsætte og
                                        vedligeholde din shop uden at begrænse antallet af tilgængelige
                                        funktioner, muligheder og brugervenlighed.
                                    </p>
                                </div>
                                <div class="col-sm-8  col-sm-offset-2">
                                    <div class="col-sm-6 col-md-4 services" id="ecom_1">
                                        <h3>Ecommerce Strategi</h3>
                                        <p>Udvikle en grundig forståelse af online detail mål og give en sund strategisk retning.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="ecom_2">
                                        <h3>PUREPLAY & OMNICHANNEL</h3>
                                        <p>Løsninger til både online forhandlere og integrerede platforme for mursten og mørtel butikker.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="ecom_3">
                                        <h3>INDUSTRI LEDENDE PLATFORME</h3>
                                        <p>Hosted og komplette skræddersyede løsninger bygget på førende platforme til at tilfredsstille alle eCommerce krav.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="ecom_4">
                                        <h3>FULD FEATURED</h3>
                                        <p>Fra systemintegration, medlemskab, kampagner, betalinger og mere. Jeg har dig dækket.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="ecom_5">
                                        <h3>DESIGN & UX</h3>
                                        <p>Design & brugeroplevelses strategier for bedre at kommunikere dit brand ud og engagere sig med et publikum.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="ecom_6">
                                        <h3>DEVICE INDEPENDENCE</h3>
                                        <p>Fuldt optimeret eCommerce setup på tværs af alle enheder, herunder responsive og dedikerede mobile websites.</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <h2>Rør den. Klem den. Zoom den. Scroll den.</h2>
                                    <p class="lead">Fremtiden er dynamisk og mobil. Derfor skal du have et responsivt
                                        design, der tilpasser sig, så din hjemmeside fremstår tiltalende og
                                        overskuelig, uanset om dine kunder benytter en iPad, smartphone
                                        eller laptop.</p>
                                    <p class="lead">
                                        Et website, der kan tilpasse sig brugerens skærmstørrelse gør
                                        alting meget nemmere. Du kan anlægge én strategi for dit ind-hold
                                        og behøver kun opdatere det ét sted. Og du kan tænke content
                                        first, da det er dit indhold — særligt det skrevne ord — der skal
                                        fungere på alle platforme, uanset den visuelle indpakning</p>
                                    <p class="lead">
                                        Med denne strategi slipper du for at vedligeholde et særskilt
                                        mobilsite og kan fokusere på at bygge ét velstruktureret og
                                        brugervenligt website. Og dette site skal designes mobile
                                        first. Flere og flere af dine brugere vil nemlig se det først eller
                                        udelukkende på en smartphone eller en tablet. For deres skyld
                                        skal sitet koges ind til sin absolutte essens, og på denne baggrund kan vi designe udvidede oplevelser for brugere med mere
                                        skærmplads og hurtigere netforbindelser.
                                    </p>
                                    <p class="lead">
                                        Jeg kan tilbyde dig et responsivt website, der fungerer på tværs af
                                        alle platforme! Såfremt du vil beholde dit nuværende website kan jeg også tilbyde
                                        dig at optimere dette, således at du ikke mister mobile kunder.
                                    </p>
                                </div>
                                <div class="col-sm-8  col-sm-offset-2">
                                    <div class="col-sm-6 col-md-4 services" id="mob_1">
                                        <h3>Mobil Strategi</h3>
                                        <p>Dybdegående undersøgelser og analyser til at formulere tydelige mobile strategier, skitsere muligheder og opfylde målsætninger.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="mob_3">
                                        <h3>Mobile Websites</h3>
                                        <p>Omdefinere weboplevelse til touch-baserede enheder. Responsive design og mobile hjemmesider for det moderne web.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="mob_4">
                                        <h3>Interface Design</h3>
                                        <p>Nøje overvejet design til at fremme moderne æstetik og levere unikke præsentationer, der er egnet til formålet.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="mob_5">
                                        <h3>Design & UX</h3>
                                        <p>Engagere og belønne publikum ved at levere bedre digitale oplevelser på minimalt plads.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="mob_6">
                                        <h3>Videre end mobil</h3>
                                        <p>Udnyttelse teknologi til at udforske digitale muligheder i nye medier, ud over smartphones og tablets.</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="clearfix">
                            <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <h2>Følg dine besøgende fra start til slut</h2>
                                    <p class="lead">Hvis din side har kommercielle mål, altså skaffer henvendelser til din
                                        forretning, eller direkte salg, så er det vigtigt at opsætte mål i Google Analytics,
                                        så du kan spore hvor dine konverteringer kommer fra.</p>
                                    <p class="lead">
                                        Google Analytics er et af mange analyseværktøjer der kan bruges til at forstå brugernes
                                        adfærd på din hjemmeside. Du kan f.eks. finde ud af hvilke trafikkilder der bidrager til
                                        den største omsætning, hvilke søgeord der leverer mest trafik eller noget helt andet.
                                        Adfærd på nettet er gennemsigtigt og der er næsten ingen grænser for hvad du kan få svar
                                        på med den rette viden og opsætning.</p>
                                    <p class="lead">
                                        Med det rette datagrundlag i Google Analytics kan du få den fornøden viden til at foretage
                                        konverteringsoptimering af din hjemmeside, sikre at du bruger dit annoncebudget optimalt ved
                                        ikke at spilde dem på værdiløse trafikkilder og generelt set få en langt bedre forståelse for
                                        dine brugers købsadfærd i kombination med en effektiv søgeordsanalyse der giver indsigt om deres søgeadfærd.
                                    </p>
                                    <p class="lead">
                                        Den optimale opsætning af Google Analytics afhænger af din forretningsstrategi, mål og ønsker.
                                        På baggrund af den tekniske opsætning kan der opsættes løbende rapporter og såkaldte dashboards
                                        for at følge udviklingen og spotte eventuelle trends som du kan målrette med din markedsføring.
                                    </p>
                                    <p class="lead">Jeg hjælpe dig med at alt indenfor opsætning, rapportering og webanalyse med Google Analytics.</p>
                                </div>
                                <div class="col-sm-8  col-sm-offset-2">
                                    <div class="col-sm-6 col-md-4 services" id="analyt_1">
                                        <h3Præsentation</h3>
                                        <p>Se alle dataene i nemme og overskuelige diagrammer efter eget valg.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="analyt_2">
                                        <h3>Segmentering</h3>
                                        <p>Segmenter og filtrer alle dine data så du kun fokusere på din ønskede målgruppe.</p>
                                    </div>
                                    <div class="col-sm-6 col-md-4 services" id="analyt_3">
                                        <h3>Følg dem</h3>
                                        <p>Følg dine besøgende hele vejen igennem sitet fra ankomst til afgang.</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <?php include("includes/cta.php");?>
</div>
<?php require("includes/footer.php");?>
<?php require("includes/scripts.php");?>
</body>
</html>