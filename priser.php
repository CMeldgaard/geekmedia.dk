<?php
//Header+OG setup
$siteName = "Geek Media";
$title = "Konkurrencedygtige priser";
$description = "Køb timer, tegn abonnement efter behov og supplere med et antal klip fra et klippekort. De bedste betingelser for at få en fleksibel samarbejdspartner.";
$keywords = "";
$image = "images/header/hero11.jpg";
?>
<!DOCTYPE html>
<html>

<head>
    <?php include("includes/meta.php")?>
    <?php require("includes/styles.php")?>
</head>
<body>
<?php include("includes/preloader.php");?>
<?php include("includes/nav.php");?>
<div class="main-container">
    <header class="page-header">
        <div class="background-image-holder">
            <img class="background-image" alt="Background Image" src="/images/header/hero11.jpg">
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <span class="text-white alt-font">Simple priser</span>
                    <h1 class="text-white">Priser</h1>
                    <p class="text-white lead">Find priserne på<br>
                        mine timer og services.</p>
                </div>
            </div>
        </div>
    </header>
    <section class="milestones services-selector bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                    <h1>Mine priser</h1>
                    <p class="lead">Alle mine priser er gennemskuelige, og uden skjulte gebyrer. Dette giver en
                        fleksibel løsning for jer som virksomhed hvor i selv kan vælge om i vil købe enkelte timer
                    eller forudkøbe timer og derigennem opnå en rabat på timerne. Timerne kan placeres på faste dage
                    i måneden efter jeres eget ønske.</p>
                    <p class="lead">
                        Alle opgivede priser er ekskl. moms og evt. transporttillæg. Hvis du er interesseret i det
                    kedelige, kan du læse mine handelsbetingelser <a href="/handelsbetingelser">her</a> </p>
                </div>
            </div>
            <div class="row">
                <div class="container">
                    <ul class="clearfix">
                        <li class="col-md-3 col-sm-6 col-md-offset-1-5 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-hourglass"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Fast timepris</h5>
                                <span>650,- i timen. Betal kun for dit reelle forbrug!</span>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-scissors"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Klippekort</h5>
                                <span>Forudkøb dine timer og få op til 24% rabat </span>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-gift"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Iværksætter</h5>
                                <span>Ny startet iværksætter? Få special tilpassede pakker til netop dig!</span>
                            </div>
                        </li>

                        <li class="col-md-3 col-sm-6 col-md-offset-1-5 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-layers"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Webhosting</h5>
                                <span>Host igennem mig fra 100,- om måneden, inkl. web monitorering (Værdi 100,-)</span>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-envelope"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Hosted exchange</h5>
                                <span>Få en exchange mail fra 75,- pr. bruger om måneden</span>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium clearfix">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-adjustments"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Web monitorering</h5>
                                <span>Få basis monitorering fra kun 100,- om måneden</span>
                            </div>
                        </li>
                        <br style="clear: both">
                        <li class="col-md-3 col-sm-6 col-md-offset-1-5 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-lifesaver"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Geek Partner</h5>
                                <span>Få fast aftale fra kun 599,- om måneden!</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <?php include("includes/cta.php");?>
</div>
<?php require("includes/footer.php");?>
<?php require("includes/scripts.php");?>
</body>
</html>