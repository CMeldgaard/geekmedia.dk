<!DOCTYPE html>
<html>

<head>
    <title>Geek Media | Bag om Geek Media</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php require("includes/styles.php")?>
</head>
<body>

<?php include("includes/preloader.php");?>

<?php include("includes/nav.php");?>

<div class="main-container">
    <header class="page-header">
        <div class="background-image-holder">
            <img class="background-image" alt="Background Image" src="/images/header/hero1.jpg">
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <span class="text-white alt-font">Mit arbejde</span>
                    <h1 class="text-white">Portfolio</h1>
                    <p class="text-white lead">Et lille udpluk af mine nyeste<br> og bedste cases, fra mig til dig.</p>
                </div>
            </div>
        </div>
    </header>
    <section class="no-pad-bottom projects-gallery first-child">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center">
                    <h1>Min portfolio</h1>
                    <p class="lead">
                       Se nogle af mine udvalgte projekter og har du nogen spørgsmål er du velkommen til at <a href="/kontakt">kontakte</a> mig
                    </p>
                </div>
            </div>
        </div>

        <div class="projects-wrapper clearfix">
            <div class="container">
                <div class="projects-container column-projects">

                    <div class="col-md-4 col-sm-6 project image-holder">
                        <div class="background-image-holder">
                            <img class="background-image" alt="Background Image" src="/images/portfolio/cove-content-marketing_list.jpg">
                        </div>
                        <div class="hover-state">
                            <div class="align-vertical" style="padding-top: 101px;">
                                <h3 class="text-white"><strong>Cover</strong> Midtjysk Turisme</h3>
                                <p class="text-white">
                                    Design af cover til hæfte omhandlende<br> Content Marketing for Midtjysk Turisme
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 project image-holder">
                        <div class="background-image-holder">
                            <img class="background-image" alt="Background Image" src="/images/portfolio/visit-skanderborg_list.jpg">
                        </div>
                        <div class="hover-state">
                            <div class="align-vertical" style="padding-top: 101px;">
                                <h3 class="text-white"><strong>Nyhedsbrev</strong> Visit Skanderborg</h3>
                                <p class="text-white">Design af nyhedsbrevstemplate samt <br>opsætning i Mailchimp for VisitSkanderborg</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 project image-holder">
                        <div class="background-image-holder">
                            <img class="background-image" alt="Background Image" src="/images/portfolio/citizone_list.jpg">
                        </div>
                        <div class="hover-state">
                            <div class="align-vertical" style="padding-top: 101px;">
                                <h3 class="text-white"><strong>Identitet</strong> CityZone</h3>
                                <p class="text-white">
                                    Udvikling af fuld visuel identitet for CitiZone
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 project image-holder">
                        <div class="background-image-holder">
                            <img class="background-image" alt="Background Image" src="/images/portfolio/cleverdeal_list.jpg">
                        </div>
                        <div class="hover-state">
                            <div class="align-vertical" style="padding-top: 101px;">
                                <h3 class="text-white"><strong>Logo</strong> Cleverdeal.dk</h3>
                                <p class="text-white">
                                    Redesign af CleverDeal.dk's logo
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 project image-holder">
                        <div class="background-image-holder">
                            <img class="background-image" alt="Background Image" src="/images/portfolio/diers-klinik_list.jpg">
                        </div>
                        <div class="hover-state">
                            <div class="align-vertical" style="padding-top: 101px;">
                                <h3 class="text-white"><strong>Website</strong> Diers Klinik</h3>
                                <p class="text-white">
                                    Redesign & hosting af Diers Klinik's<br> nye website.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 project image-holder">
                        <div class="background-image-holder">
                            <img class="background-image" alt="Background Image" src="/images/portfolio/easv_list.jpg">
                        </div>
                        <div class="hover-state">
                            <div class="align-vertical" style="padding-top: 101px;">
                                <h3 class="text-white"><strong>Poster</strong> EASV</h3>
                                <p class="text-white">
                                    Udarbejdelse af promotion poster for<br> EASV Esbjerg, i forbindelse med skoleopgave.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 project image-holder">
                        <div class="background-image-holder">
                            <img class="background-image" alt="Background Image" src="/images/portfolio/rethink_list.jpg">
                        </div>
                        <div class="hover-state">
                            <div class="align-vertical" style="padding-top: 101px;">
                                <h3 class="text-white"><strong>Print</strong> Midtjysk Turisme</h3>
                                <p class="text-white">
                                    Udarbejdelse af hæfte for Midtjysk<br> Turisme's RETHINK Kulturturisme
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 project image-holder">
                        <div class="background-image-holder">
                            <img class="background-image" alt="Background Image" src="/images/portfolio/naturperlen_list.jpg">
                        </div>
                        <div class="hover-state">
                            <div class="align-vertical" style="padding-top: 101px;">
                                <h3 class="text-white"><strong>Logo</strong> Naturperlen</h3>
                                <p class="text-white">
                                    Udarbejdelse af logo for Naturperlen,<br> i forbindelse med skoleopgave
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 project image-holder">
                        <div class="background-image-holder">
                            <img class="background-image" alt="Background Image" src="/images/portfolio/Danish-Entrepreneuship-Award-2014_list.jpg">
                        </div>
                        <div class="hover-state">
                            <div class="align-vertical" style="padding-top: 101px;">
                                <h3 class="text-white"><strong>Poster</strong> DEA 2014</h3>
                                <p class="text-white">
                                    Konkurrenceforslag til Danish<br> Entrepreneuship Award 2014
                                </p>
                            </div>
                        </div>
                    </div>
                    <br style="clear: both;">
                </div>
            </div>
        </div>
    </section>
    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 text-center">
                    <h1>Hvad siger mine kunder</h1>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="testimonials-slider text-center">
                        <ul class="slides">
                            <li>
                                <p class="lead">Vores nyhedsbrev trængte i den grad til et facelift og igennem en god samarbejdspartner hørte vi om Geek Media.
                                    Vi har nu fået et nyt, smart look på vores nyhedsbrev vha. MailChimp, som er et meget brugervenligt og ”lige til at gå til” system.
                                    Carsten har hjulpet os professionelt og kyndigt hele vejen igennem og er super hurtig på mailen, når man stiller ham spørgsmål.
                                    Vi kan på det varmeste anbefale Geek Media.
                                </p>
                                <span class="author">Mia Bay - VisitSkanderborg</span>
                            </li>
                            <li>
                                <p class="lead">I forbindelse med en seminarrække i efterår/vinter 2013/14 havde vi brug for en skriftlig
                                    opsamling af den tilegnede viden. Vi udarbejdede en skitse over indhold og form og kontaktede Carsten efter anbefalinger. Alpha-Omega for
                                    produktet var en hurtig leveringstid og fleksibilitet omkring korrektur og udviklingen af produktet.
                                    Carsten var effektiv og behagelig i vores mailkorrespondance og forstod hurtigt hvilken stil vi ønskede. Carsten er dygtig til at aflæse
                                    kundens designønsker og kommer gerne med egne forslag der kan matche. Vi vil anbefale Carsten som en effektiv samarbejdspartner indenfor
                                    grafiske opgaver. </p>
                                <span class="author">Dorte Dejbjerg Arens - Midtjysk Turisme</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php include("includes/cta.php");?>
</div>
<?php require("includes/footer.php");?>
<?php require("includes/scripts.php");?>
</body>
</html>