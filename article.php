<?php include("includes/datacon.php");?>
<?php
$blogLink = str_replace(".php", "", $_GET["perma"]);

if ($blogLink == "" ){
    header('Location: /blog');

}else{
$stmt = mysqli_prepare($conn, "SELECT blog.*, LEFT(blog.blogContent, 350) as shortBlog, blogcategory.*, blogwriters.* FROM blog
INNER JOIN blogcategory
ON blog.blogCategoryID = blogcategory.blogCategoryID
INNER JOIN blogwriters
on blog.createdBy = blogwriters.createdBy
where blogLink=? order by createdDate desc");

/* bind parameters for markers */
mysqli_stmt_bind_param($stmt, "s", $blogLink);

/* execute query */
$exStmt = mysqli_stmt_execute($stmt);


/* instead of bind_result: */
$result = $stmt->get_result();

$count = 0;
/* now you can fetch the results into an array - NICE */
while ($row = $result->fetch_assoc()) {

//Header+OG setup
    $siteName = "Geek Media";
    $title = $row["blogTitle"];
    $description = TrimShort(strip_tags($row["shortBlog"]));
    $keywords = "";
    $image = "images/article/". $row["blogImage"];
    ?>
    <!DOCTYPE html>
    <html>

    <head>
        <?php include("includes/meta.php")?>
        <?php require("includes/styles.php")?>
    </head>
    <body>

    <?php include("includes/preloader.php"); ?>

    <?php include("includes/nav.php"); ?>

    <div class="main-container">
        <header class="page-header">
            <div class="background-image-holder">
                <img class="background-image" alt="Background Image" src="/<?php echo $image?>">
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <span class="text-white alt-font">
                          Posted d.
                          <?php
                          $posted = strtotime($row["createdDate"]);
                          echo date("d F Y", $posted);
                          ?>
                          I <a
                                  href="/blog/kategori/<?php echo $row["catUrl"] ?>"><?php echo $row["catName"] ?></a>
                        </span>
                        <h1 class="text-white"><?php echo $row["blogTitle"] ?></h1>
                        <p class="text-white lead"><?php echo $row["subTitle"] ?></p>
                    </div>
                </div>
            </div>
        </header>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div>
                            <!-- Blog Post-->
                            <article class="col-md-12 blog-masonry-item">
                                <div class="item-inner">
                                    <div class="post-title">
                                        <?php echo $row["blogContent"]?>

                                        <div id="disqus_thread"></div>
<script>
var disqus_config = function () {
this.page.url = <?php echo $row["blogLink"] ?>; // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = <?php echo $row["blogID"]?>; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};

(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');

s.src = '//geekmediablog.disqus.com/embed.js';

s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
                                    </div>
                                </div>
                            </article>
                        </div>

                    </div>
                    <?php include("includes/blog-sidebar.php"); ?>
                </div>
            </div>
        </section>
        <?php include("includes/newsletterForm.php"); ?>
    </div>
    <?php require("includes/footer.php"); ?>
    <?php require("includes/scripts.php"); ?>
    </body>
    </html>
    <?php
    $count = $count+1;
}
if ($count==0){
   include '404.php';
}
}
    ?>
