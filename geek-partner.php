<?php
//Header+OG setup
$siteName = "Geek Media";
$title = "Geek Partner";
$description = "Din professionelle samarbejdspartner - jeg hjælper med store som små problemer.";
$keywords = "";
$image = "images/header/hero10.jpg";
?>
<!DOCTYPE html>
<html>

<head>
    <?php include("includes/meta.php")?>
    <?php require("includes/styles.php")?>
</head>
<body>
<?php include("includes/preloader.php");?>
<?php include("includes/nav.php");?>
<div class="main-container">
    <header class="page-header">
        <div class="background-image-holder">
            <img class="background-image" alt="Background Image" src="/images/header/hero10.jpg">
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <span class="text-white alt-font">Opgrader din support</span>
                    <h1 class="text-white">Geek Partner</h1>
                    <p class="text-white lead">Din professionelle samarbejdspartner! <br>Jeg hjælper med store som små problemer.</p>
                </div>
            </div>
        </div>
    </header>
    <section class="milestones services-selector bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                    <h1>Din nye partner</h1>
                    <p class="lead">Med en Geek Partner aftale har du hjælp til de problemer du skulle støde på.
                        Jeg hjælper dig med vanskeligheder eller ting der ikke lige spiller.</p>
                </div>
            </div>
            <div class="row lead">
                <div class="container">
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.2s" id="part_1" style="visibility: visible; animation-delay: 0.2s; animation-name: zoomIn;">
                            <h3>Månedlig support</h3>
                            <p>Med Geek Partner har du månedligt 2 timer support, fejlretning og rådgivning.</p>
                        </div>
                        <div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.4s" id="part_2" style="visibility: visible; animation-delay: 0.4s; animation-name: zoomIn;">
                            <h3>Prioritering</h3>
                            <p>Prioritering af alle dine supportsager sikre hurtigt respons og rettelse af opståede fejl.</p>
                        </div>
                        <div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.6s" id="part_3" style="visibility: visible; animation-delay: 0.6s; animation-name: zoomIn;">
                            <h3>Rig kontakt</h3>
                            <p>Masser af supportmuligheder sikre at du hurtigt og knidningsfrit kan komme i kontakt med mig.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                    <p class="lead">Det er ofte ikke muligt for virksomheder at have en professionel webmaster
                        tilknyttet på fuld tid. Derfor tilbyder jeg denne Geek Partner aftale, hvor jeg sørger for at
                        holde styr på jeres hjemmeside. Med Geek Partner har du månedligt 2 timer support, fejlretning
                        og rådgivning, hvad enten det er til opsætning af nyheder, opsætning af produkter, fejlretning
                        på hjemmesiden, rådgivning til om i f.eks. skal benytte de sociale medier,
                        søgemaskineoptimering og meget mere.</p>
                </div>
                <div class="container text-center">
                    <ul class="clearfix">
                        <li class="col-md-3 col-sm-6 col-md-offset-1-5 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-phone"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Telefonsupport</h5>
                                <span>Adgang til support via telefon</span>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-envelope"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Mail support</h5>
                                <span>Brug af dedikeret support email</span>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-chat"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Live chat</h5>
                                <span>Support via Livechat alle dage</span>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 col-md-offset-1-5  text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-wallet"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>20% rbat</h5>
                                <span>Rabat på alle support timer</span>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 text-center space-bottom-medium">
                            <div class="feature feature-icon-large">
                                <i class="icon icon-trophy"></i>
                                <div class="pin-body"></div>
                                <div class="pin-head"></div>
                                <h5>Support prioritering</h5>
                                <span>1. prioritering i supportsager</span>
                            </div>
                        </li>
                    </ul>
                    <a href="/priser" class="btn btn-primary">SE PRISER</a>
                </div>
            </div>
        </div>
    </section>
    <?php include("includes/cta.php");?>
</div>
<?php require("includes/footer.php");?>
<?php require("includes/scripts.php");?>
</body>
</html>
