<?php

function countCategory($catID){
	global $conn;
	$sqlString="SELECT COUNT(*) AS posts FROM blog  WHERE releaseTime<=DATE_ADD(NOW(), INTERVAL 1 HOUR) AND blogCategoryID=$catID";
	$result = mysqli_query($conn, $sqlString);
	$row = mysqli_fetch_assoc($result);
	echo $row["posts"];
}

function countPosts(){
	global $conn;
	$sqlString="SELECT COUNT(*) AS posts FROM blog WHERE releaseTime<=DATE_ADD(NOW(), INTERVAL 1 HOUR)";
	$result = mysqli_query($conn, $sqlString);
	$row = mysqli_fetch_assoc($result);
	echo $row["posts"];
}

function latestPosts($numRows){
	global $conn;
	$sqlString="SELECT * FROM blog  WHERE releaseTime<=DATE_ADD(NOW(), INTERVAL 1 HOUR) ORDER BY blogID desc LIMIT $numRows";
	$result = mysqli_query($conn, $sqlString);
	if (mysqli_num_rows($result) > 0) {
		while($row = mysqli_fetch_assoc($result)) {
		echo "<li><a href='/blog/".$row["blogLink"]."'>".$row["blogTitle"]."</a></li>";
		}
	}
}
