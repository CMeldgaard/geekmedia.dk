<script src="/js/vendor/jquery.min.js"></script>
<script src="/js/vendor/jquery.plugin.min.js"></script>
<script src="/js/vendor/bootstrap.min.js"></script>
<script src="/js/vendor/jquery.flexslider-min.js"></script>
<script src="/js/vendor/smooth-scroll.min.js"></script>
<script src="/js/vendor/skrollr.min.js"></script>
<script src="/js/vendor/scrollReveal.min.js"></script>
<script src="/js/vendor/isotope.min.js"></script>
<script src="/js/scripts.js"></script>
<!--Crowdio Chat script-->
<script type="text/javascript">(function(){var e,t,n;t=false;e=document.createElement("script");e.type="text/javascript";e.src="https://app.crowdio.com/visitor/loadjs/253";e.onload=e.onreadystatechange=function(){if(!t&&(!this.readyState||this.readyState=="complete")){t=true}};n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(e,n)})()</script>
<!--Google Analytics script-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45624510-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Hotjar Tracking Code for http://www.geekmedia.dk -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:164136,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
