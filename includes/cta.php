<section class="strip bg-secondary-1">
    <div class="container">
        <div class="row clearfix">
            <div class="col-sm-6 col-xs-12 pull-left">
                <h3 class="text-white">
                    <strong>Start i dag.</strong>&nbsp;Og lad os brygge på gode idéer!</h3>
            </div>

            <div class="col-sm-4 col-xs-12 pull-right text-right">
                <a href="/kontakt" class="btn btn-primary btn-white">START I DAG!</a>
            </div>
        </div>
    </div>
</section>