<title>Geek Media | <?php echo $title?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta name="description" content="<?php echo $description?>">
<meta name="keywords" content="<?php echo $keywords?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta property="og:title" content="<?php echo $title?>" />
<meta property="og:description" content="<?php echo $description?>" />
<meta property="og:site_name" content="<?php echo $siteName?>"/>
<meta property="og:url" content="http://<?php echo$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>" />
<meta property="og:image" content="http://<?php echo $_SERVER['HTTP_HOST']?>/<?php echo $image?>" />