<div class="col-sm-4">
    <div class="blog-sidebar">
        <div class="sidebar-widget">
            <h5>Kategorier</h5>
            <ul>
                <li>
                    <a href="/blog">Alle indlæg</a><span class="badge"><?php echo countPosts()?></span>
                </li>
                <li>
                    <a href="/blog/kategori/chit-chat">Chit chat</a><span class="badge"><?php echo countCategory(1)?></span>
                </li>
                <li>
                    <a href="/blog/kategori/nyheder">Nyheder</a><span class="badge"><?php echo countCategory(2)?></span>
                </li>
                <li>
                    <a href="/blog/kategori/tech">Tech</a><span class="badge"><?php echo countCategory(3)?></span>
                </li>
                <li>
                    <a href="/blog/kategori/tutorials">Tutorials</a><span class="badge"><?php echo countCategory(4)?></span>
                </li>
                <li>
                    <a href="/blog/kategori/seneste-arbejde">Seneste arbejde</a><span class="badge"><?php echo countCategory(5)?></span>
                </li>
            </ul>
        </div>



        <div class="sidebar-widget">
            <h5>Seneste indlæg</h5>
            <ul>
                <?php echo latestPosts(5)?>
            </ul>
        </div>
    </div>
</div>
