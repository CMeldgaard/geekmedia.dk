<div class="nav-container">
    <nav class="contained-bar top-bar">
        <div class="container">
            <div class="contained-wrapper">
                <div class="row nav-menu">
                    <div class="col-md-4 col-sm-4 columns">
                        <a href="/"> <img class="logo logo-dark logo-wide" alt="Logo" src="/images/logo-dark.png""></a>
                    </div>
                    <div class="col-md-8 col-sm-8 columns">
                        <ul class="menu">
                            <li><a href="/">Forside</a>
                            </li>
                            <li><a href="/bag-om">Bag Geek Media</a>
                            </li>
                            <li class="has-dropdown"><a href="#">Muligheder</a>
                                <ul class="subnav">
                                    <li><a href="/hosting">Hosting</a></li>
                                    <li><a href="/web">Web</a></li>
                                    <li><a href="/design">Design</a></li>
                                    <li><a href="/markedsfoering">Markedsføring</a></li>
                                    <li><a href="/geek-partner">Geek Partner</a></li>
                                    <li><a href="/priser">Priser</a></li>
                                </ul>
                            </li>
                            <li><a href="/folio">Folio</a>
                            </li>
                            <li><a href="/blog">Blog</a>
                            </li>
                            <li><a href="/kontakt">Kontakt</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="mobile-toggle">
                    <i class="icon icon_menu"></i>
                </div>
            </div>
        </div>
        <div class="bottom-border"></div>
    </nav>
</div>