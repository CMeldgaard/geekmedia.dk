<link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" href="/images/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/images/favicon/android-icon-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/images/favicon/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/images/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/images/favicon/manifest.json">
<link rel="shortcut icon" href="/images/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#2b5797">

<link href="/css/vendor/flexslider.min.css" rel="stylesheet" type="text/css" media="all">
<link href="/css/vendor/line-icons.min.css" rel="stylesheet" type="text/css" media="all">
<link href="/css/vendor/elegant-icons.min.css" rel="stylesheet" type="text/css" media="all">
<link href="/css/vendor/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
<link href="/css/style.css?v=1.0.7" rel="stylesheet" type="text/css" media="all">
<!--[if gte IE 9]>
<link rel="stylesheet" type="text/css" href="/css/ie9.css" />
<![endif]-->
<script src="/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
