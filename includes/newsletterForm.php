<section class="strip bg-secondary-1">
    <div class="container">
        <div class="row clearfix">
            <form class="mail-list-signup" action="//geekmedia.us10.list-manage.com/subscribe/post?u=7fa854c313e3baad3707b89c7&amp;id=945468c802" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank" novalidate="">
                <div class="col-sm-9 col-xs-12 pull-left clear-fix">
                    <h3 class="text-white pull-left"><strong>Tilmeld til nyhedsbrev.&nbsp;</strong></h3>
                    <input type="email" class="signup-email-field validate-required validate-email" value="" name="EMAIL" class="form-control" id="mce-EMAIL" placeholder="Indtast email" required="">
                </div>

                <div class="col-sm-3 col-xs-12 pull-right text-right">
                    <input type="submit" class="btn btn-primary btn-filled vjs-1444125482797-7" value="Tilmeld" id="mc-embedded-subscribe">
                </div>
            </form>
        </div>

    </div>
</section>