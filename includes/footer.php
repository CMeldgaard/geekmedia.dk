<div class="footer-container">
    <footer class="bg-primary short-2">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <span class="text-white">© 2015 Geek Media | 36343451</span>
                    <span class="text-white"><a href="mailto:hallo@geekmedia.dk">hallo@geekmedia.dk</a></span>
                    <span class="text-white">+45 3160 6013</span>
                    <span class="text-white">Rolfsgade 154 st. tv. 6700 Esbjerg</span>
                </div>
            </div><!--end for row-->
        </div><!--end of container-->

        <div class="contact-action">
            <div class="align-vertical" style="padding-top: 80px;">
                <i class="icon text-white icon_mail"></i>
                <a href="/kontakt" class="text-white">
                    <span class="text-white">Få fat på mig <i class="icon arrow_right"></i></span>
                </a>
            </div>
        </div>
    </footer>
</div>