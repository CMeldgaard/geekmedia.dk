<?php
//Header+OG setup
$siteName = "Geek Media";
$title = "Konkurrencedygtige priser";
$description = "Køb timer, tegn abonnement efter behov og supplere med et antal klip fra et klippekort. De bedste betingelser for at få en fleksibel samarbejdspartner.";
$keywords = "";
$image = "images/header/hero11.jpg";
?>
<!DOCTYPE html>
<html>

<head>
    <?php include("includes/meta.php")?>
    <?php require("includes/styles.php")?>
</head>
<body>
<?php include("includes/preloader.php");?>
<?php include("includes/nav.php");?>
<div class="main-container">
    <header class="page-header">
        <div class="background-image-holder">
            <img class="background-image" alt="Background Image" src="/images/header/hero11.jpg">
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <span class="text-white alt-font">Det med småt</span>
                    <h1 class="text-white">Handelsbetingelser</h1>
                    <p class="text-white lead">Læs hvilke betingelser<br>
                    der er gældende ved handler.</p>
                </div>
            </div>
        </div>
    </header>
    <section class="milestones services-selector bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                    <h1>Handelsbetingelser</h1>
                    <h2>1. Anvendelse</h2>
                    <p>Nedenstående salgs- og leveringsbetingelser finder anvendelse ved salg fra Geek Media (herefter ”Sælger”) til kunden (herefter ”Køber”). Dette gælder også, selvom Køber under købsforhandlingerne eller i Købers udbudsmateriale eller i Købers bekræftelse af købet over for Sælger har anført andre vilkår for købet, idet alle sådanne vilkår anses for frafaldet ved Sælgers endelige tiltrædelse af købet, medmindre Sælger heri udtrykkeligt skriftligt gengiver de enkelte betingelser, der pålægges Sælger, og som er andre eller anderledes formulerede end de i disse salgs- og leveringsbetingelser anførte. </p>

                    <h2>2. Fortrolighed</h2>
                    <p>Parterne har tavshedspligt med hensyn til oplysninger, som efter deres indhold må anses for fortrolige.</p>
                    <p>Oplysninger udleveret i forbindelse med oprettelse af abonnement, er fortrolige og må ikke videregives til tredjepart.</p>
                    <p>Kunden indestår for, at kundens personale, konsulenter og andre der bistår kunden pålægges tilsvarende fortrolighedsforpligtelse med hensyn til forhold omkring Geek Media og Geek Media’s andre kunder.</p>
                    <p>Geek Media videregiver alene information, når dette kan kræves med henvisning til gyldig lovhjemmel af f.eks. myndigheder, domstole eller i tilfælde af lovovertrædelser.</p>

                    <h2>3. Tilbud &amp; accept</h2>
                    <p>Kun skriftlige tilbud er gældende for Sælger. Tilbud er gældende i 6 uger. Den i tilbuddet angivne leveringshorisont/-tidspunkt er alene vejledende, og leveringstiden er først endelig, når den er anført i ordrebekræftelsen.</p>

                    <h2>4. Priser</h2>
                    <p>Priser i tilbud, ordrebekræftelser og kontrakter er dagspriser ekskl. moms og afgifter. Sælger forbeholder sig ret til at ændre priser i tilfælde af væsentlige ændringer i indkøbspriser, produktionsomkostninger, arbejdsløn, råmaterialer, underleverancer, valutakurser, fragt, diskonto, told, skatter, afgifter og o.l. samt ved begivenheder, der er omfattet af pkt. 8.</p>

                    <h2>5. Ordreoptagelse</h2>
                    <p>Hvor andet ikke er angivet løber produkter i min. 3 hele måneder + måneden, hvori produktet bestilles. Hvert kvartal fornyes produktet automatisk til en ny periode. Fejlbestilte perioder tilbagebetales ikke.</p>
                    <p>
                        Opsigelse af produkter sker altid til udløb af igangværende periode. Restperioder refunderes ikke.
                        Køberen hæfter for ethvert tab, Sælgeren måtte lide som følge af, at der gives forkerte oplysninger ved ordreoptagelse
                    </p>

                    <h2>6. Levering &amp; leveringstid</h2>
                    <p>I tilfælde af forsinkelse skal Køber reklamere straks. Køber kan herefter alene hæve købet, såfremt Sælger ikke har leveret senest 5 hverdage efter skriftligt påkrav til Sælger fra Køber. Køber kan i intet tilfælde kræve erstatning som følge af forsinkelse.</p>
                    <h3>6.1. Levering fra ekstern partner</h3>
                    <p>Sælger er ikke ansvarlig for forsinkelse i tilfælde af manglende levering fra ekstern partner, herunder, men ikke begrænset til, trykkeri, tekstforfattere samt SEO optimizers.  Alle reklamtioner skal derfor rettes mod den pågældende eksterne partner.</p>

                    <h2>7. Ejendomsforbehold</h2>
                    <p>Sælger forbeholder sig ejendomsretten til det solgte, indtil hele købesummen og de med salgsgenstandens levering eventuelle forbundne omkostninger til f.eks. levering er betalt af Køber. Ved betaling med check eller veksel anses betaling ikke som endelig betaling, før fuld indfrielse har fundet sted, og pengeinstituttets eventuelle indsigelsesfrist er udløbet. Ejendomsforbeholdet påvirker ikke risikoovergangen til Køber ved levering.</p>

                    <h2>8. Force Majeure</h2>
                    <p>Sælger er ikke ansvarlig for forsinkelse i tilfælde af force majeure, herunder, men ikke begrænset til, arbejdskonflikt og enhver anden omstændighed, som parterne ikke er herre over, såsom brand, krig, beslaglæggelse, valutarestriktioner, oprør og uroligheder, mangel på transportmidler, almindelig vareknaphed, forsinkelse, kassation af større varepartier, restriktioner af drivkraft samt tillige i tilfælde af mangelfulde eller manglende leverancer fra underleverandør, uanset årsagen hertil.

                    </p><p>I disse tilfælde udskydes rettidig levering til begivenhedens udløb, dog maksimalt 4 uger, efter hvilken frist begge parter skal være berettigede til at hæve handlen, uden at det skal betragtes som misligholdelse.</p>

                    <h2>9. Ændringer af lovgivningen, evt. myndighedsindgreb m.v.</h2>
                    <p>I tilfælde af ændring i den danske lovgivning, herunder teleloven og de i medfør af denne udstedte regler og tilladelser, samt ved pålæg fra myndighederne om ændring af et eller flere vilkår i aftalen, kan Geek Media med øjeblikkelig virkning ændre køberens rettigheder og pligter ifølge aftalen, uden at køberen herved har krav på erstatning eller godtgørelse af nogen art.</p>

                    <h2>10. Ansvar</h2>
                    <p>
                        Sælgeren er uden ansvar for tab som følge af indirekte skade og følgeskader, herunder tab af forventet avance, tab af data eller deres reetablering, tab af goodwill, tab i forbindelse med betalingsoverførsler, tekniske nedbrud, uvedkommendes adgang, fejlagtig opsætning eller anden lignede følgeskade, i forbindelse med anvendelsen af systemet eller tab som følge af manglende funktioner i systemet, uanset om Sælgeren har været underrettet om muligheden for et sådant tab, og uanset om Sælgeren kan bebrejdes tabet på baggrund af udvist uagtsomhed eller lignende.
                    </p>

                    <h2>11. Ansvarsbegrænsning</h2>
                    <p>
                        For krav, som vedrører Sælgers opfyldelse eller manglende opfyldelse af sine forpligtelser, er Køber berettiget til erstatning for direkte tab med følgende begrænsninger:
                    </p>
                    <h3>11.1.</h3>
                    <p>Sælgers erstatningsansvar er begrænset til direkte skader/tab, og er – uanset årsag og uanset kravets art – begrænset til det beløb, der er faktureret for pågældende ydelse eller vare, der forårsagede skaden/tabet, eller er årsag til eller direkte forbundet med erstatningskravet.</p>
                    <h3>11.2.</h3>
                    <p>Sælger er under ingen omstændigheder erstatningsansvarlig over for Køber for driftstab, tabt fortjeneste, tabte besparelser eller andre indirekte tab eller følgeskader, der skyldes anvendelse af det solgte eller manglende mulighed for at anvende dette, uanset om Sælger er blevet informeret om sådanne kravs mulighed.</p>
                    <h3>11.3.</h3>
                    <p>Tab, udgifter eller omkostninger forbundet med at hjemtage, genbestille, reparere, bortfjerne eller træffe tilsvarende foranstaltninger med mangelfulde produkter eller produkter, hvori Sælgers produkter er gjort til en bestanddel, kan ikke gøres gældende over for Sælger. Sælger påtager sig intet ansvar som følge af Købers retsforhold over for tredjemand.</p>
                    <h3>11.4.</h3>
                    <p>Sælger er ikke ansvarlig for skade på fast ejendom eller løsøre, som indtræder, medens produkterne er i Købers besiddelse. Sælger er heller ikke ansvarlig for skade på produkter, der er fremstillet af Køber, eller på produkter, hvori disse indgår.</p>
                    <h3>11.5.</h3>
                    <p>Sælger påtager sig ikke ansvar for tab som følge af uvedkommendes adgang til køberens data og/eller systemer.</p>

                    <h2>12. Returvarer</h2>
                    <p>Køber har ikke ret til at returnere varer og leverancer fra Sælger, medmindre dette forudgående og skriftligt er aftalt. Såfremt dette er aftalt, krediteres godkendte returvarer normalt med fradrag af 15 % af salgsprisen ekskl. moms ved franko-levering til Sælgers lager eller andet af Sælger anvist sted i Danmark.</p>
                    <h3>12.1. Specialvarer</h3>
                    <p>Specialvarer, tilvirkede varer eller udstillingsvarer tages ikke retur.</p>

                    <h2>13. Specielle produktbetingelser</h2>
                    <h3>13.1. Domæne</h3>
                    <p>Ved domænebestilling erklærer registranten at brugen af domænenavnet ikke krænker tredjeparts navne- eller varemærkerettigheder eller i øvrigt må formodes at stride mod dansk lovgivning. Efter bestilling af et domæne, blive dette registreret umiddelbart efter hos DK-hostmaster samt registrant af internationale domæner. Hvis ikke andet er angivet i bestillingen, bliver domænet ved registrering peget mod datacenterets to navneservere. Ved fejlbestilte domæner tilbagebetales registreringsgebyret kun såfremt der ikke allerede er afgivet bestilling til DK-hostmaster eller registrant af internationale domæner.</p>
                    <p><strong>Registreringsgebyr:</strong> Til dækning af DK-hostmasters omkostninger betaler registranten en årsafgift på 40 kr. excl. moms, der opkræves ved køb af .dk-domænet. Registranten har pligt til senest 10 måneder efter registrering at meddele DK-hostmaster hvilken gyldig e-mail-konto, der kan anvendes ved udsendelse af faktura via e-mail. Betaling af årsafgift skal foregå via PBS eller andet elektronisk medie foreskrevet af DK-hostmaster. Betaling af årsafgiften giver registranten ret til løbende at få ændret i databaseoplysninger om domænet uden yderligere betaling, herunder f.eks. skift af navneservere eller ændringer i selskabs- eller personoplysninger.</p>

                    <h3>13.2. Webhotel</h3>
                    <p>Der vil løbende blive vurderet funktionen af scripts/programmer som eventuelt kan belaste serveren eller forårsage nedbrud. Hvis dette er tilfældet kan Geek Media til enhver tid fjerne disse scripts/programmer uden forudgående varsel. Indehaveren vil herefter blive informeret.</p>
                    <p>Under normale omstændigheder er der fri trafik, og fri båndbrede for alle webhoteller. Dog kan en grov udnyttelse af dette medfør eksklusion fra datacenterets servere eller begrænsning af båndbredde. Grov udnyttelse kan eksempelvis være overtrædelse af reglerne om websidens indhold, samt at denne overtrædelse forårsager et uforholdsmæssigt stort trafikforbrug.</p>
                    <p>
                        Der foretages daglig backup på alle webhoteller. Restore fra denne kan foretages mod et gebyr på 499 ekskl. Moms. Alle webservere drives på højtydende Windows og Linux/unix servere. Datacenteret tager backup af alle data, og systemerne er overvåget 24 timer i døgnet, hvilket sikrer at der kan tilbydes oppetid på over 99,9%.
                    </p>

                    <h3>13.3. Site monitorering (Geek Check)</h3>
                    <p>Sælgeren tager på ingen måde ansvar for levering af alarm SMS til kunden. Skulle det ske at køberen mobilnetværk har været nede i forbindelse med udsending, kan Sælgeren ikke stilles til ansvar herfor.</p>

                    <h3>13.4. Content Management System (CMS)</h3>
                    <p>
                        Efter levering af løsninger med et content management system, er køberen selv ansvarlig for fremtidige opdateringer af systemet, medmindre at der er indgået en Geek Partner aftale eller andet er angivet i aftalen.</p>

                    <h3>13.5. Hosted Exchange</h3>
                    <p>
                        For abonnementer der indeholder licensbaseret klient software, som kunden, ved abonnementets start, har erhvervet gennem Geek Media, er kunden forpligtet til at afinstallere licenspligtigt software ved ophør af abonnement.
                    </p>
                    <p>Det er ikke tilladt at benytte en Hosted Exchange løsning til distribuering af nyhedsbreve, som SMTP server for websites eller til afsendelse af andre former for automatisk genererede e-mails. En overtrædelse heraf kan resultere i lukning eller midlertidig nedlæggelse af ens konto.</p>

                    <h2>14. Produktansvar</h2>
                    <p>Sælger skal af Køber holdes skadesløs i den udstrækning, Sælger pålægges ansvar over for tredjemand for sådan skade eller sådant tab, som Sælger efter pkt. 9 ikke er ansvarlig for over for Køber.</p>
                    <p>Bliver Sælger sagsøgt af tredjemand i anledning af produktansvar, accepterer Køber at kunne blive adciteret under sagen eller sagsøgt ved den domstol eller voldgiftsret, som behandler sagen.</p>
                    <p>Sælger er i intet tilfælde ansvarlig for driftstab, tabt fortjeneste eller andre økonomiske konsekvenstab. Hvis tredjemand fremsætter krav mod en af parterne om erstatningsansvar i henhold til dette punkt, skal denne part straks underrette den anden herom.</p>

                    <h2>15. Driftbetingelser</h2>
                    <p>
                        Der tilstræbes, at alle systemerne er tilgængelige 24 timer i døgnet, året rundt. Det er dog berettiget at afbryde driften, når vedligeholdelse eller andre tekniske forhold gør det nødvendigt.
                    </p><p>
                        Vedligeholdelse foretages fortrinsvis onsdage mellem kl. 03.00 og kl. 05.00.
                    </p><p>
                        Servere er placeret i specialindrettede serverrum i datacenteret og alle servere er sikret mod strømsvigt med UPS strømbackup.
                    </p><p>
                        Internet linietilgangen (2 x 1000Mbit) sker igennem et samarbejde med TDC.
                    </p>

                    <h2>16. Aftalens ikrafttræden, opsigelse og ophør</h2>
                    <p>
                        Aftalen er indgået for en ubegrænset tidsperiode og træder kraft straks efter, køberen har godkendt aftalen.</p><p>
                        Begge parter kan dog til enhver tid opsige aftalen med 1 måneds varsel. Ønsker køberen at opsige enkelte produkter skal dette foregår skriftligt senest d. 15 i måneden til udløbet af den bestilte periode med mindre andet fremgår ved bestilling af det enkelte produkt. Opsigelser af webhoteller er først gyldige når opsigelsen bekræftes skriftligt af Sælgeren.</p><p>
                        Sælger refunderer ikke betalinger såfremt køberen ønsker at opsige og nedlægge sit produkt midt i en periode. </p><p>
                        Ved aftalens ophør, af en hvilken som helst grund, skal køberen straks ophøre med at anvende materialer som ikke er ejet af køberen.
                    </p>

                    <h2>17. Misligholdelse</h2>
                    <p>
                        Køberen er forpligtet til at oplyse det korrekte antal brugere af Sælgers licenser. Evt. ændringer skal straks angives til Geek Media. Ved ukorrekt angivelse af licensbrugere opkræves manglende licens samt administrationsgebyr på kr. 500 pr. manglende bruger. Ved gentagne tilfælde af ukorrekte angivelse af licensbrugere er Geek Media berettiget til straks at ophæve aftalen.
                    </p><p>
                        Køberen er ansvarlig for, at de af køberen udbudte og anvendte tjenester, herunder programmer, programmel, software, samt de af køberen lagerede filer i enhver form ikke krænker tredjemands rettigheder, herunder tillige immaterielle rettigheder og/eller krænker den til enhver tid gældende lovgivning på området.
                    </p><p>
                        Bliver Sælger opmærksom på kundens ulovlige aktiviteter, tages der forbehold for at aftalen vil blive ophævet med omgående virkning. I tilfælde af ulovlige aktiviteter er køberen forpligtet til i enhver henseende at friholde Sælgeren for eventuelle erstatningskrav opstået som følge heraf.
                    </p><p>
                        Køberen gøres opmærksom på, at Sælgeren ikke udøver nogen form for kontrol med det informationsindhold, som er omfattet af ydelsen.
                    </p><p>
                        Det er derfor købers ansvar at sikre, at brugen af ydelsen ikke strider mod gældende lovgivning og den mellem køberen og Sælgeren indgåede aftale. Misligholder kunden sit abonnement, tages der forbehold for at aftalen kan ophæves med omgående virkning.
                    </p><p>
                        Nedenstående punkter anses for væsentlig misligholdelse, uden listen dog, kan anses for at være udtømmende.
                    </p>
                    <ul class="featuresList">
                        <li>Manglende rettidig betaling efter påkrav.</li>
                        <li>Ukorrekte eller manglende kundeoplysninger.</li>
                        <li>Gentagne tilfælde af ukorrekte angivelse af licensbrugere</li>
                        <li>Alle former for afsendelse og distribution af spam fra eller via kunden.</li>
                        <li>Misbrug af e-mailservere til afsendelse af mail-spam o.l.</li>
                        <li>Spredning af vira.</li>
                        <li>Forsøg på ulovlig indtrængen i netværk, hacking, forsøg på at tilgå andres data, eller udførelse af Denial of Service-angreb. Herunder men ikke begrænset til portscanning og mailbombing. Overtrædelse af disse regler kan, hvis det skønnes nødvendigt, medføre politianmeldelse.</li>
                        <li>Hjemmesider og e-mail, der krænker tredjemands rettigheder.</li>
                        <li>Optræden i strid med god skik på Internettet.</li>
                        <li>Videresalg, udleje, udlån, overdragelse m.m. af abonnementet til tredjemand.</li>
                    </ul>
                    <p>
                        I tilfælde af afbrydelse - som følge af kundens misligholdelse – vil kunden kunne få oplyst om årsagen til afbrydelsen. Er der tale om henvendelse fra tredjemand vedr. dokumenteret ulovligt indhold på en kundes hjemmeside, vil hjemmesiden blive lukket med øjeblikkelig virkning, hvorefter sagsøger har 14 dage til at få udtaget fogedforbud. Sker dette ikke genåbnes hjemmesiden. Afbrydelse som følge af misligholdelse berettiger ikke kunden til afslag i abonnements-betalingen.
                    </p><p>
                        Ophæves aftalen skal kunden betale fuldt abonnement i de pågældende abonnementsperioder samt dække ethvert tab.
                    </p>

                    <h2>18. Ændring af abonnementsvilkårene og priser</h2>
                    <p>Sælger forbeholder sig ret til, på et hvert tidspunkt, at ændre abonnementsvilkårene, herunder produktydelserne samt priserne med 30 dages varsel. Ved lancering af nye produkter m.v., kan ændringerne meddeles og gennemføres uden varsel.</p>

                    <h2>19. Overdragelse</h2>
                    <p>Ingen af parterne er berettiget til helt eller delvist at overdrage rettighederne og forpligtelserne i henhold til denne aftale til tredjemand uden forudgående skriftligt samtykke fra den anden part.</p>

                    <h2>20. Tegninger &amp; beskrivelser</h2>
                    <p>
                        Alle specifikationer og oplysninger om vægt, dimension, kapacitet, pris, tekniske og andre data anført i kataloger, datablade, annoncer, billedmateriale og prislister er omtrentlige og alene vejledende. Sådanne oplysninger er derfor kun bindende i det omfang, disse udtrykkeligt er gengivet i ordren, eller der specifikt henvises til dem.
                    </p>
                    <p>
                        Såfremt konstruktion eller specifikationer m.v. for et af Sælger solgt produkt måtte blive ændret forinden leveringstidspunktet, er Sælger berettiget til at levere produktet med de(n) nu herefter gældende konstruktion og specifikationer m.v., såfremt produktet – efter en objektiv vurdering – ikke er blevet forringet herved. Det samme gælder med hensyn til produktets ydre fremtræden, herunder farve.
                    </p>
                    <p>Alle fremsendte tegninger og beskrivelser forbliver Sælgers ejendom og må ikke uden tilladelse kopieres, reproduceres, overgives til eller på anden måde bringes til tredjemands kundskab.
                    </p>
                    <p>
                        Såfremt Køber – for at kunne gennemføre projektet – har behov for tegninger og dokumentation, kan parterne forudgående skriftligt aftale, at Sælger udleverer tegninger og dokumentation, der er nødvendige for at sætte Køber i stand til at opstille, igangsætte, drive og vedligeholde leverancen. Uden samtykke fra Sælger må denne information ikke anvendes til andet end det, der var formålet ved overdragelsen. Dog forlanger Sælger, at informationen forbliver fortrolig.
                    </p>
                    <p>
                        Sælger forbeholder sig ret til at videregive eventuelle af Køber til Sælger udleverede tegninger og tekniske specifikationer til underleverandører, i det omfang dette er nødvendigt til opfyldelsen af leverancen.</p>

                    <h2>21. Betalinsbetingelser</h2>
                    <p>Alle ydelser ved Geek Media er oplyst i danske kroner og ex. moms, hvor intet andet er opgivet. Alle ydelser ved Geek Meidia betales ved fremsendelse af faktura. Alle fakturaer har 8 dages betalingsfrist. Såfremt betalingen for fakturaerne ikke er Geek Media i hænde herefter, har Geek Media ret til at suspendere Køberens produkter indtil betalingen er modtaget. Køberen får ikke refusion for perioder med lukning såfremt disse skyldes manglende betaling.</p>
                    <p>Geek Media rykker kunden 3 gange inden betalingen overdrages til inddrivelse via inkasso. Ved hver rykker tillægges et administrationsgebyr på 100 kr. + moms.</p>

                    <h2>22. Tvister</h2>
                    <p>Enhver tvist mellem parterne, der udspringer af nærværende aftale, skal afgøres efter dansk ret. Værneting skal være Sø-og Handelsretten i København eller efter Geek Medias valg efter "Regler for behandling af sager ved Det Danske Voldgiftsinstitut (Copenhagen Arbitration)" i København. Hver part udpeger en voldgiftsmand, mens voldgiftsmandens formand udnævnes af Instituttet. Såfremt en part inden 30 dage efter at have indgivet eIler modtaget underretning om begæring om voldgift ikke har udpeget en voldgiftsmand, udnævnes også denne af Instituttet i overensstemmelse med ovennævnte regel.</p>
                </div>
            </div>
        </div>
    </section>
    <?php include("includes/cta.php");?>
</div>
<?php require("includes/footer.php");?>
<?php require("includes/scripts.php");?>
</body>
</html>