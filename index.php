<?php
//Header+OG setup
$siteName = "Geek Media";
$title = "Din freelance partner";
$description = "Lej en freelance samarbejdspartner til at håndtere jeres virksomheds design og web opgaver i Esbjerg og Varde.";
$keywords = "markedsføring, reklame, marketing, grafisk design, grafisk, facebook, sociale medier, projektledelse, Varde, Esbjerg, messer, kundearrangement, arrangement, hjemmeside, web, seo, søgeordsoptimering, hosting, print, print design";
$image = "images/FB-Cover-01.png";
$type = "website";
?>
<!DOCTYPE html>
<html>

<head>
    <?php include("includes/meta.php")?>
    <?php require("includes/styles.php")?>
</head>
<body>

<?php include("includes/preloader.php");?>

<?php include("includes/nav.php");?>

<div class="main-container">
    <section class="hero-slider first-child">
        <ul class="slides">
            <li class="overlay hover-background">
                <div class="background-image-holder">
                    <img class="background-image" alt="Background Image" src="/images/header/layer1.jpg">
                </div>

                <div class="foreground-image-holder layer-1">
                    <img class="background-image" alt="Background Image" src="/images/header/layer2.png">
                </div>

                <div class="foreground-image-holder layer-2">
                    <img class="background-image" alt="Background Image" src="/images/header/layer3.png">
                </div>

                <div class="container align-vertical">
                    <div class="row">
                        <div class="col-sm-9 col-md-7">
                            <h1 class="text-white">Få alle fordelene fra bureauerne med jeres egen lokale freelance samarbejdspartner!</h1>
                            <a href="/bag-om" class="btn btn-primary btn-white">LÆS OM MIG</a>
                            <a href="/kontakt" class="btn btn-primary btn-filled">KONTAKT MIG I DAG</a>
                        </div>
                    </div>
                </div>
            </li>

            <li class="overlay">
                <div class="background-image-holder" >
                    <img class="background-image" alt="Background Image" src="/images/header/hero2.jpg">
                </div>

                <div class="container align-vertical">
                    <div class="row">
                        <div class="col-md-6 col-sm-9">
                            <h1 class="text-white">Lej Geek Media som freelance partner de timer I har brug for det!</h1>
                            <a href="/folio" class="btn btn-primary btn-white" >SE MIN PORTFOLIO</a>
                            <a href="/kontakt" class="btn btn-primary btn-filled" >START DIT PROJEKT</a>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </section>
    <section class="text-image-bottom bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
                    <h1>Velkommen til Geek Media!&nbsp;Få alle fordelene fra bureauerne med jeres egen lokale freelance samarbejdspartner!</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-10 col-md-offset-1 text-center">
                    <p class="lead" >Selvom vi lever i en global verden, kan det stadig betale sig at handle lokalt. Ved at vælge en lokal partner beholder I jeres projekter tæt på jer, og har I spørgsmål eller problemer, skal I ikke ringe til den anden side af jorden for at få hjælp.
                        <br>Jeg vil kunne indgå I jeres team ude på arbejdspladsen, og I vil kun betale for de reelle timer I bruger. Resten af tiden er det gratis!&nbsp;</p>
                </div>
            </div>
        </div>

        <div class="text-center">
            <img alt="Freelancer Essentials" src="/images/bottom1.jpg">
        </div>
    </section>
    <?php include("includes/cta.php");?>
    <section>
        <div class="container">
            <div class="row">

            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <h1>Dine fordele med en <br>freelance samarbejdspartner</h1>
                </div>
                <div class="col-sm-6">
                    <div class="feature feature-icon-large">
                        <div class="pull-left">
                            <i class="icon icon-map-pin"></i>
                        </div>
                        <div class="pull-right">
                            <h5>Lokalt arbejde</h5>
                            <p>
                                Jeg vil kunne indgå I jeres team ude på arbejdspladsen, og I vil kun betale for de reelle
                                timer I bruger. Resten af tiden er det gratis!
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="feature feature-icon-large">
                        <div class="pull-left">
                            <i class="icon icon-linegraph"></i>
                        </div>
                        <div class="pull-right">
                            <h5>Fleksible timer</h5>
                            <p>
                                Book timerne så de passer ind i jeres projektkalender, når i har bruge for det. Så book jeres første timer nu!
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="feature feature-icon-large">
                        <div class="pull-left">
                            <i class="icon icon-tools-2"></i>
                        </div>
                        <div class="pull-right">
                            <h5>En bred værktøjskasse</h5>
                            <p>
                                Jeg kommer med en bred værktøjskasse der kan bruges til netop at løse jeres opgaver.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="feature feature-icon-large">
                        <div class="pull-left">
                            <i class="icon icon-lightbulb"></i>
                        </div>
                        <div class="pull-right">
                            <h5>Første møde er gratis!</h5>
                            <p>
                                Aftale et møde hvor vi sammen brygger på nogle gode
                                idéer. Det første møde er gratis!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section class="no-pad-bottom no-pad-top projects-gallery hidden-xs">

        <div class="projects-wrapper clearfix">

            <div class="projects-container">

                <div class="col-md-4 col-sm-6 no-pad project print image-holder">
                    <div class="background-image-holder">
                        <img class="background-image" alt="Background Image" src="/images/portfolio/citizone_list.jpg">
                    </div>
                    <div class="hover-state">
                        <div class="align-vertical">
                            <h3 class="text-white"><strong>CitiZone</strong></h3>
                            <p class="text-white">
                               Udvikling af fuld visuel identitet for CitiZone
                            </p>
                            <a href="/folio" class="btn btn-primary btn-white" >Se flere</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 no-pad project print image-holder">
                    <div class="background-image-holder">
                        <img class="background-image vjs-1443803752701-4" alt="Background Image" src="/images/portfolio/visit-skanderborg_list.jpg">
                    </div>
                    <div class="hover-state">
                        <div class="align-vertical">
                            <h3 class="text-white"><strong>Visit</strong> Skanderborg</h3>
                            <p class="text-white">
                                Design og opsætning af nyhedsbrevstemplate i MailChimp
                            </p>
                            <a href="/folio" class="btn btn-primary btn-white" >Se flere</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 no-pad project print image-holder">
                    <div class="background-image-holder">
                        <img class="background-image vjs-1443803752701-4" alt="Background Image" src="/images/portfolio/cove-content-marketing_list.jpg">
                    </div>
                    <div class="hover-state">
                        <div class="align-vertical">
                            <h3 class="text-white"><strong>Midtjysk</strong> Turisme</h3>
                            <p class="text-white">
                                Design af cover samt grafiske elementer<br /> til Content Marketing hæfte
                            </p>
                            <a href="/folio" class="btn btn-primary btn-white" >Se flere</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="clients bg-white">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2>Lille udpluk af mine kunder</h2>
                </div>
            </div>

            <div class="row client-row">
                <div class="row-wrapper">
                    <div class="col-sm-3 text-center">
                    <img alt="Client Logo" src="/images/clients/citizone.png">
                    </div>

                    <div class="col-sm-3 text-center">
                        <img alt="Client Logo" src="/images/clients/cleverdeal.png">
                    </div>

                    <div class="col-sm-3 text-center">
                    <img alt="Client Logo" src="/images/clients/destinationer.png">
                    </div>

                    <div class="col-sm-3 text-center">
                        <img alt="Client Logo" src="/images/clients/diersklinik.png">
                    </div>
                </div>
            </div>

            <div class="row client-row">
                <div class="row-wrapper">
                    <div class="col-sm-3 text-center">
                        <img alt="Client Logo" src="/images/clients/midtjysk.png">
                    </div>

                    <div class="col-sm-3 text-center">
                        <img alt="Client Logo" src="/images/clients/visitskanderborg.png">
                    </div>

                    <div class="col-sm-3 text-center">
                        <img alt="client" src="/images/clients/nordichaircare.png">
                    </div>

                    <div class="col-sm-3 text-center">
                        <img alt="client" src="/images/clients/inventailor.png">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php include("includes/newsletterForm.php"); ?>
</div>
<?php require("includes/footer.php");?>
<?php require("includes/scripts.php");?>
</body>
</html>
