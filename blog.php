<?php include("includes/datacon.php");?>
<?php
//Header+OG setup
$siteName = "Geek Media";
$title = "Fra de støvede kroge";
$description = "Læs de seneste blogindlæg fra mig!";
$keywords = "";
$image = "images/header/hero14.jpg";
?>
<!DOCTYPE html>
<html>

<head>
    <?php include("includes/meta.php")?>
    <?php require("includes/styles.php")?>
</head>
<body>

<?php include("includes/preloader.php");?>

<?php include("includes/nav.php");?>

<div class="main-container">
    <header class="page-header">
        <div class="background-image-holder">
            <img class="background-image" alt="Background Image" src="/images/header/hero14.jpg">
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <span class="text-white alt-font">Spændende skriverier</span>
                    <h1 class="text-white">Min blog</h1>
                    <p class="text-white lead">Skriverier fra de støvede<br> kroge i mit hoved</p>
                </div>
            </div>
        </div>
    </header>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div>
                        <?php
                        if ( isset( $_GET['cat'] ) && !empty( $_GET['cat'] ) ){
                            $cat = $_GET["cat"];
                        }else{
                            $cat = "";
                        }
                        //Checks for category selection, if none, show all sorted by date
                        if ($cat == "") {
                            $sqlString = "SELECT LEFT(blogContent, 350) as shortBlog, blog.blogCategoryID, blogID, blogImage, blogLink, blogTitle, blog.createdBy, createdDate, blogcategory.*, blogwriters.*
                            FROM blog
                            INNER JOIN blogcategory ON blog.blogCategoryID = blogcategory.blogCategoryID
                            INNER JOIN blogwriters on blog.createdBy = blogwriters.createdBy
                            WHERE blog.releaseTime<=DATE_ADD(NOW(), INTERVAL 1 HOUR)
                            order by createdDate desc";
                            $result = mysqli_query($conn, $sqlString);
                        }else{
                            $stmt = mysqli_prepare($conn, "SELECT LEFT(blogContent, 350) as shortBlog, blog.blogCategoryID, blogID, blogImage, blogLink, blogTitle, blog.createdBy, createdDate, blogcategory.*, blogwriters.*
                            FROM blog
                            INNER JOIN blogcategory ON blog.blogCategoryID = blogcategory.blogCategoryID
                            INNER JOIN blogwriters on blog.createdBy = blogwriters.createdBy
                            where  blog.releaseTime<=DATE_ADD(NOW(), INTERVAL 1 HOUR) AND catURL=? order by createdDate desc");

                            /* bind parameters for markers */
                            mysqli_stmt_bind_param($stmt, "s", $cat);

                            /* execute query */
                            mysqli_stmt_execute($stmt);

                            /* instead of bind_result: */
                            $result = $stmt->get_result();
                        }
                        $i = 1;
                        if (mysqli_num_rows($result) > 0) {
                            // output data of each row
                            while($row = mysqli_fetch_assoc($result)) {

                                setlocale(LC_ALL, "danish");
                                $creaDate = strftime("%d", strtotime($row["createdDate"]));
                                $creaMonth = strftime("%b", strtotime($row["createdDate"]));
                                if ($i==1) {
                                    ?>
                                    <!-- Blog Post-->
                                    <article class="col-md-12 blog-masonry-item">
                                        <div class="item-inner">
                                            <a href="/blog/<?php echo $row["blogLink"] ?>">
                                                <img alt="Blog image"
                                                     src="/images/article/<?php echo $row["blogImage"] ?>">
                                            </a>

                                            <div class="post-title">
                                                <h2><?php echo $row["blogTitle"] ?></h2>
                                                    <?php
                                                    echo TrimShort($row["shortBlog"]);
                                                    ?>

                                                <div class="post-meta">
                                                <span class="sub alt-font">Posted d.
                                                    <?php
                                                    $posted = strtotime($row["createdDate"]);
                                                    echo date("d F Y", $posted);
                                                    ?></span>
                                                    <span class="sub alt-font">I <a
                                                            href="/blog/kategori/<?php echo $row["catUrl"] ?>"><?php echo $row["catName"] ?></a></span>
                                                </div>
                                                <a href="/blog/<?php echo $row["blogLink"] ?>" class="link-text">Læs
                                                    mere</a>
                                            </div>
                                        </div>
                                    </article>
                                    <!-- End of Blog Post-->
                                    <?php
                                } else{?>
                                <!-- Blog Post-->
                                <article class="col-md-6 blog-masonry-item">
                                    <div class="item-inner" >
                                        <a href="/blog/<?php echo $row["blogLink"]?>">
                                            <img alt="Blog image" src="/images/article/<?php echo $row["blogImage"]?>">
                                        </a>
                                        <div class="post-title">
                                            <h2><?php echo $row["blogTitle"]?></h2>
                                            <div class="post-meta">
                                        <span class="sub alt-font">Posted d.
                                            <?php
                                            $posted = strtotime($row["createdDate"]);
                                            echo date("d F Y", $posted);
                                            ?></span>
                                                <span class="sub alt-font">I <a href="/blog/kategori/<?php echo $row["catUrl"]?>"><?php echo $row["catName"]?></a></span>
                                            </div>
                                            <a href="/blog/<?php echo $row["blogLink"]?>" class="link-text">Læs mere</a>
                                        </div>
                                    </div>
                                </article>
                                <!-- End of Blog Post-->

                                    <?php
                                    }
                            $i++;
                            }
                        } else {
                            ?>
                            <article>
                                <img src="/images/article/empty.jpg" class="resp-img" alt="Det er helt tomt">
                            </article>
                            <?php
                        }
                        ?>
                    </div>

                </div>
                <?php include("includes/blog-sidebar.php");?>
            </div>
        </div>
    </section>
    <?php include("includes/newsletterForm.php");?>
</div>
<?php require("includes/footer.php");?>
<?php require("includes/scripts.php");?>
</body>
</html>
