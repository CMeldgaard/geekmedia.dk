<!DOCTYPE html>
<html lang="da">
	
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Start dit projekt i dag og få svar indenfor én arbejdsdag.">
		<title>Geek Media | Start projekt</title>
		<?php include('includes/styles.php');?>
	</head>
	
	<body>
		
		<!-- Top Bar-->
		<?php include('includes/nav.php');?>
		<!-- End of Top Bar-->
		
		<!-- About Us -->
		<section class="portfolio">
			<div class="breadcrumbs">
				<div class="row">
					<div class="col-sm-6">
						<h1>Start projekt</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb">
							<li>Du er her: </li>
							<li><a href="/">Forside</a>
							</li>
							<li class="active">Start projekt</li>
						</ol>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h2>Start projekt</h2>
					<hr class="small">
					<p>Start i dag og få et uforpligtende tilbud.</p>
				</div>
			</div>
		</section>
		<section class="">
			<div class="spacing-70"></div>
			<div class="row ">
				<div class="col-sm-8 center-block">
<?php 
if (isset($_POST['navn'])){

$modtager = "cm@geekmedia.dk";
$emne = "Opstart af projekt";

$besked = "<h1>Opstart af projekt hos Geek Media</h1> \n
               <strong>Kontaktinformation</strong><br /> \n
               Navn:" . $_POST['navn']. "<br /> \n
			   Firma/Forening:" . $_POST['firmaforening']. "<br /> \n
			   E-mail:" . $_POST['email']. "<br /> \n
			   Telefon:" . $_POST['telefon']. "<br /><br /> \n
			   <h2>Projektinformation</h2> \n
			   <strong>Projekttyper</strong><br />";

			   //Tjekker hvilke projektyper der er valgt
			   if ($_POST['website'] <> "") {
			   $besked .= "Website <br />";
			   }
			   if ($_POST['mobil'] <> "") {
			   $besked .= "Mobil <br />";
			   }
			   if ($_POST['ecommerce'] <> "") {
			   $besked .= "eCommerce <br />";
			   }
			   if ($_POST['visuel-identitet'] <> "") {
			   $besked .= "Visuel identitet <br />";
			   }
			   if ($_POST['grafisk-design'] <> "") {
			   $besked .= "Grafisk design <br />";
			   }
			   if ($_POST['monitorering'] <> "") {
			   $besked .= "Site monitorering <br />";
			   }
			   if ($_POST['hosted-exchange'] <> "") {
			   $besked .= "Hosted exchange <br />";
			   }
			   if ($_POST['hosting'] <> "") {
			   $besked .= "Hosting <br />";
			   }

$besked .= "<br /><strong>Projekt budget</strong><br /> \n
                " . $_POST['budget'] ." <br /><br /> \n
                <br /><strong>Tidsrame</strong><br /> \n
                " . $_POST['tidsramme'] . " <br /><br /> \n
                <br /><strong>Projekt beskrivelse</strong><br /> \n
                " . $_POST['beskrivelse'] . "<br />";

$header  = "MIME-Version: 1.0" . "\r\n";

$header .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";

$header .= "from:" . $_POST['email'] ."";

mail($modtager, $emne, $besked, $header); //Sender formularen
?>
	 <h2>Mange tak for dine projekt</span> informationer.</h1>
        <p>
            Tak fordi du er interesseret i at arbejde sammen med Geek Media. Dine projektinformatiner er nu sendt til os, og vi vil vende tilbage indenfor én  arbejdsdag.
        </p>
<?php }else{ ?>
				<h2>Lad os lave noget fantastisk sammen.</h2>
				<p>
            Tak fordi du er interesseret i at arbejde sammen med Geek Media. At komme igang er meget nemt - blot udfylde de tomme felter herunder, send det til os og vi vil vende tilbage indenfor
                én arbejdsdag. <span class="cursive">Hvor spændende!</span>
        </p>
					    <form action="" method="post">
    <div class="container inputPlanner center">
        <input type="text" placeholder="Navn" name="navn" required="required" />
        <input type="email" placeholder="Email adresse" required="required" name="email" />
        <input type="text" placeholder="Telefon" name="telefon"  required="required" />
        <input type="text" placeholder="Firma / Forening" name="firmaforening" />
        <h2>Hvilke<strong> projekttyper?</strong> </h2>
        <p class="subHeadline">Vælg alle der passer til dit projekt</p>
        <div class="col-md-4 col-sm-6">
            <input type="checkbox" name="website" id="website" value="website" />
            <label for="website">Website</label>
        </div>
        <div class="col-md-4 col-sm-6">
            <input type="checkbox" name="mobil" id="mobil" value="mobil" />
            <label for="mobil">Mobil</label>
        </div>
        <div class="col-md-4 col-sm-6">
            <input type="checkbox" name="ecommerce" id="ecommerce" value="ecommerce" />
            <label for="ecommerce">eCommerce</label>
        </div>
        <div class="col-md-4 col-sm-6">
            <input type="checkbox" name="branding" id="branding" value="branding" />
            <label for="branding">Visuel identitet</label>
        </div>
        <div class="col-md-4 col-sm-6">
            <input type="checkbox" name="grafisk-design" id="grafisk-design" value="grafisk-design" />
            <label for="grafisk-design">Design & UX</label>
        </div>
		<div class="col-md-4 col-sm-6">
            <input type="checkbox" name="print-design" id="print-design" value="print-design" />
            <label for="print-design">Print design</label>
        </div>
        <div class="col-md-4 col-sm-6">
            <input type="checkbox" name="monitorering" id="monitorering" value="monitorering" />
            <label for="monitorering">Site monitorering</label>
        </div>
        <div class="col-md-4 col-sm-6">
            <input type="checkbox" name="hosted-exchange" id="hosted-exchange" value="hosted-exchange" />
            <label for="hosted-exchange">Hosted Exchange</label>
        </div>
        <div class="col-md-4 col-sm-6">
            <input type="checkbox" name="Hosting" id="Hosting" value="Hosting" />
            <label for="Hosting">Hosting</label>
        </div>
    </div>
    <hr />
    <div class="container inputPlanner center">
        <h2>Hvilket<strong> budget</strong> arbejder i med?</h2>
        <p class="subHeadline">Et gennemsigtigt budget vil hjælpe os med at sikre at jeres forventninger bliver mødt.</p>
        <br style="clear:both;" />
        <input type="text" name="budget" />
    </div>
    <hr />
    <div class="container inputPlanner center">
        <h2><strong>Tidsramme</strong></h2>
        <p class="subHeadline">Er der en bestemt tidsramme eller deadline for projektet?</p>
        <input type="text" name="tidsramme" />
        <h2><strong>Omkring</strong> projektet</h2>
        <p class="subHeadline">Beskriv projektet, krav og mål.</p>
        <textarea name="beskrivelse"  required="required"></textarea>
    </div>
    <hr />
    <div class="container inputPlanner center">
        <input type="submit" value="Send projektinformation" />
    </div>
    </form>
<?php }?>
				</div>
			</div>
		</section>
		
		<?php include('includes/bottom.php');?>
	</body>
	
</html>
