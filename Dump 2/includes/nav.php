    <div class="top">
        <div class="row">
            <div class="col-sm-3">
                <div class="logo">
                    <a href="/"><img src="/images/logo.png" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-sm-9">

                <nav id="desktop-menu">
                    <ul class="sf-menu" id="navigation">
                        <li ><a href="/bag-om">Bag om</a>
                        </li>
                        <li><a href="#">Hosting</a>
                            <ul>
                                <li><a href="/geek-host">Geek Host</a>
                                </li>
								<li><a href="/domaener">Domæner</a>
									</li>
                                <li><a href="/hosted-exchange">Hosted Exchange</a>
                                </li>
                                <li><a href="/monitorering">Monitorering</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="#">Web</a>
							<ul>
                                <li><a href="/website">Website m. CMS</a>
                                </li>
								<li><a href="/ecommerce">eCommerce</a>
                                </li>
								<li><a href="/monitorering">Monitorering</a>
                                </li>
                                <li><a href="/mobil">Mobil</a>
                                </li>
								<li><a href="/geek-partner">Geek Partner</a>
                                </li>
								<li><a href="/google-analytics">Google Analytics</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="#">Design</a>
                            <ul>
                                <li><a href="/design-ux">Design & UX</a>
                                </li>
                                <li><a href="/print-design">Print Design</a>
                                </li>
                                <li><a href="/visuel-identitet">Visuel identitet</a>
                                </li>
                            </ul>
                        </li>
						<li ><a href="/folio">Folio</a>
                        </li>
                        <li><a href="/blog">Blog</a>
                        </li>
                        <li><a href="/kontakt">Kontakt</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>