<?php
require('domainAvailability.php');
$service = new HelgeSverre\DomainAvailability\AvailabilityService(true);

$domain = $_GET['domain'];
$tld = $_GET['tld'];

$searchDomain = $domain . "." .$tld;
$available = $service->isAvailable($searchDomain);
$domainStr = "<table style='text-align: center;margin: 0 auto;'>";
if($available) {
		$domainStr = $domainStr . "<tr><td colspan='2'><h2 style='text-transform:uppercase;'>" . $domain . ".".$tld." ER LEDIGT</h2></td></tr>
		<tr><td> <a href='/planner' class='btn btn-trans'>BESTIL NU</btn></td>
		<td> <btn class='btn btn-trans' id='newDomainSearch'>NY SØGNING</btn></td></tr>";
	}
	else {
		$domainStr = $domainStr . "<tr><td colspan='2'><h2 style='text-transform:uppercase;'>" . $domain . ".".$tld." ER OPTAGET</h2></td></tr>
		<tr><td> <a href='/planner' class='btn btn-trans'>REDELEGER</btn></td>
		<td> <btn class='btn btn-trans' id='newDomainSearch'>NY SØGNING</btn></td></tr>";
	}
	echo $domainStr;

