<div class="col-sm-4">
                <div class="sidebar">

                    <div class="widget">
                        <h3 class="badge">KATEGORIER</h3>
                        <ul>
							<li>
								<a href="/blog">Alle indlæg</a><span class="badge"><?php echo countPosts()?></span>
							</li>
							<li>
								<a href="/blog/kategori/chit-chat">Chit chat</a><span class="badge"><?php echo countCategory(1)?></span>
							</li>
							<li>
								<a href="/blog/kategori/nyheder">Nyheder</a><span class="badge"><?php echo countCategory(2)?></span>
							</li>
							<li>
								<a href="/blog/kategori/tech">Tech</a><span class="badge"><?php echo countCategory(3)?></span>
							</li>
							<li>
								<a href="/blog/kategori/tutorials">Tutorials</a><span class="badge"><?php echo countCategory(4)?></span>
							</li>
							<li>
								<a href="/blog/kategori/seneste-arbejde">Seneste arbejde</a><span class="badge"><?php echo countCategory(5)?></span>
							</li>
                        </ul>
                    </div>

                    <div class="widget">
                        <h3 class="badge">UGENS GIF</h3>
                        <?php
							$weekNr = date(W,time());
						?>
						<img src="/images/gifs/<?php echo $weekNr?>.gif">
                    </div>

                    <div class="widget">
                        <h3 class="badge">SENESTE INDLÆG</h3>
                        <ul>
						<?php echo latestPosts(5)?>
                        </ul>
                    </div>

                </div>

            </div>