    <!--  Footer -->
    <div class="social">
        <div class="row">

            <div class="col-sm-6">
                <ul>
                    <li><a href="https://www.facebook.com/GeekMediaDK" title="facebook" target="_blank"><i class="fa fa-facebook"></i></a>
						</li>
                    <li><a href="https://twitter.com/GeekmediaDK" title="twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li><a href="https://instagram.com/geekmediadk" title="instagram" target="_blank"><i class="fa fa-instagram"></i></a>
					</li>
                    <li><a href="https://www.google.com/+GeekmediaDkPlus" title="google plus" target="_blank"><i class="fa fa-google-plus"></i></a>
                    </li>
					<li><a href="https://www.linkedin.com/company/geek-media-dk" title="LinkedIn" target="_blank"><i class="fa fa-linkedin"></i></a>
                    </li>
                </ul>
            </div>

            <div class="col-sm-6">

                <div id="mc_embed_signup">
                    <form class="form-inline validate" action="//geekmedia.us10.list-manage.com/subscribe/post?u=7fa854c313e3baad3707b89c7&amp;id=945468c802" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" target="_blank" novalidate>
                        <div class="row no-gutter">
                            <div class="col-sm-9">
                                <input type="email" value="" name="EMAIL" class="form-control" id="mce-EMAIL" placeholder="Tilmeld dig til mit nyhedsbrev" required>
                                <div style="position: absolute; left: -5000px;">
                                    <input type="text" name="b_b5638e105dac814ad84960d90_9345afa0aa" tabindex="-1" value="">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" value="TILMELD" name="subscribe" id="mc-embedded-subscribe">
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <section class="footer">
        <div class="row">
            <div class="col-sm-3">
                <h4>Services.</h4>
                <ul>
                    <li><a href="/geek-host">Hosting</a>
                    </li>
                    <li><a href="/website">Website</a>
                    </li>
                    <li><a href="/geek-partner">Geek Partner</a>
                    </li>
                    <li><a href="/design-ux">Design & UX</a>
                    </li>
                </ul>
            </div>
			<div class="col-sm-3">
                <h4>Add-on Services.</h4>
                <ul>
					<li><a href="/mobil">Mobil</a>
                    </li>
                    <li><a href="/google-analytics">Google Analytics</a>
                    </li>
                    <li><a href="/hosted-exchange">Hosted Exchange</a>
                    </li>
                    <li><a href="/monitorering">Monitorering</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h4>Firma.</h4>
                <ul>
                    <li><a href="/kontakt">Kontakt mig</a>
                    </li>
                    <li><a href="/bag-om">Bag om</a>
                    </li>
                    <li><a href="/blog">Blog</a>
                    </li>
					<li><a href="/handelsbetingelser">Handelsbetingelser</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h4>Spørg.</h4>
                <ul class="questions">
                    <li><a href="javascript:ShowChat();"><i class="fa fa-comment"></i> LIVE CHAT</a>
                    </li>
                    <li><i class="fa fa-phone"></i> +45 31 60 60 13</li>
                    <li><a href="mailto:hallo@geekmedia.dk"><i class="fa fa-envelope"></i> E-MAIL MIG</a>
                    </li>
                </ul>
            </div>
			<div class="col-md-12 copyright">
				&copy; Copyright Geek Media 2015 - <?php echo date("Y") ?> | CVR 36343451
			</div>
        </div>
    </section>
    <!--  End of Footer -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/hoverIntent.js"></script>
    <script src="/js/superfish.min.js"></script>
    <script src="/js/owl.carousel.js"></script>
    <script src="/js/wow.min.js"></script>
    <script src="/js/jquery.circliful.min.js"></script>
    <script src="/js/waypoints.min.js"></script>
	<script src="/js/jquery.responsiveTabs.js"></script>
    <script src="/js/jquery.slicknav.min.js"></script>
    <script src="/js/retina.min.js"></script>
    <script src="/js/loader.js"></script>
    <script src="/js/custom.js"></script>
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45624510-1', 'auto');
  ga('send', 'pageview');

</script>