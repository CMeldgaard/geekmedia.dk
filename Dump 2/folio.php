<!DOCTYPE html>
<html lang="da">
	
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Et lille udpluk af mine nyeste og bedste cases, fra mig til dig.">
		<title>Geek Media | Portfolio</title>
		<?php include('includes/styles.php');?>
	</head>
	
	<body>
		
		<!-- Top Bar-->
		<?php include('includes/nav.php');?>
		<!-- End of Top Bar-->
		
		<!-- About Us -->
		<section class="portfolio">
			<div class="breadcrumbs">
				<div class="row">
					<div class="col-sm-6">
						<h1>Portfolio</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb">
							<li>Du er her: </li>
							<li><a href="/">Forside</a>
							</li>
							<li class="active">Folio</li>
						</ol>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h2>Portfolio</h2>
					<hr class="small">
					<p>Et lille udpluk af mine nyeste og bedste cases, fra mig til dig.</p>
				</div>
			</div>
		</section>
		<section class="">
			<div class="spacing-70"></div>
			<div class="row ">
				<div class="col-sm-12">
					<ul class="caselist">
						<li class="wow zoomIn" data-wow-delay="0.2s">
							<div class="case-top">
							</div>
							
							<figure class="bottom-arrow">
								<img src="/images/portfolio/cove-content-marketing_list.jpg" class="resp-img" alt="Case study Caseture project">
							</figure>
							<div class="case-desc">
								<div class="case-desc-inner">
									<h5>Cover</h5>
									<h4>Midtjysk Turisme</h4>
									<p>Design af cover til hæfte omhandlende Content Marketing for <strong>Midtjysk Turisme</strong></p>
								</div>
							</div>
						</li>
						<li class="wow zoomIn" data-wow-delay="0.4s">
							<div class="case-top">
							</div>
							
							<figure class="bottom-arrow">
								<img src="/images/portfolio/visti-skanderborg_list.jpg" class="resp-img" alt="Case study Caseture project">
							</figure>
							<div class="case-desc">
								<div class="case-desc-inner">
									<h5>Nyhedsbrev</h5>
									<h4>Visit Skanderborg</h4>
									<p>Design af nyhedsbrevstemplate samt opsætning i Mailchimp for <strong>VisitSkanderborg</strong></p>
								</div>
							</div>
						</li>
						<li class="wow zoomIn" data-wow-delay="0.6s">
							<div class="case-top">
							</div>
							
							<figure class="bottom-arrow">
								<img src="/images/portfolio/citizone_list.jpg" class="resp-img" alt="Case study Caseture project">
							</figure>
							<div class="case-desc">
								<div class="case-desc-inner">
									<h5>Skoleopgave</h5>
									<h4>CitiZone</h4>
									<p>Design af visuelt identitet for <strong>CitiZone</strong></p>
								</div>
							</div>
						</li>
						<li class="wow zoomIn" data-wow-delay="0.2s">
							<div class="case-top">
							</div>
							
							<figure class="bottom-arrow">
								<img src="/images/portfolio/cleverdeal_list.jpg" class="resp-img" alt="Case study Caseture project">
							</figure>
							<div class="case-desc">
								<div class="case-desc-inner">
									<h5>Logo</h5>
									<h4>CleverDeal.dk</h4>
									<p>Redesign af <strong>CleverDeal.dk's</strong> logo</p>
								</div>
							</div>
						</li>
						<li class="wow zoomIn" data-wow-delay="0.4s">
							<div class="case-top">
								<span class="case-options">
									<a href="http://www.diersklinik.dk" target="_blank"><i class="fa fa-link"></i></a>
								</span>
							</div>
							
							<figure class="bottom-arrow">
								<img src="/images/portfolio/diers-klinik_list.jpg" class="resp-img" alt="Case study Caseture project">
							</figure>
							<div class="case-desc">
								<div class="case-desc-inner">
									<h5>Website</h5>
									<h4>Diers Klinik</h4>
									<p>Redesign & hosting af <strong>Diers Klinik's</strong> nye website.</p>
								</div>
							</div>
						</li>
						<li class="wow zoomIn" data-wow-delay="0.6s">
							
							<div class="case-top">
							</div>
							
							<figure class="bottom-arrow">
								<img src="/images/portfolio/easv_list.jpg" class="resp-img" alt="Case study Caseture project">
							</figure>
							<div class="case-desc">
								<div class="case-desc-inner">
									<h5>Skoleprojekt</h5>
									<h4>EASV Esbjerg</h4>
									<p>Udarbejdelse af promotion poster for <strong>EASV Esbjerg</strong>, i forbindelse med skoleopgave.</p>
								</div>
							</div>
						</li>
						<li class="wow zoomIn" data-wow-delay="0.2s">
							<div class="case-top">
							</div>
							
							<figure class="bottom-arrow">
								<img src="/images/portfolio/rethink_list.jpg" class="resp-img" alt="Case study Caseture project">
							</figure>
							<div class="case-desc">
								<div class="case-desc-inner">
									<h5>Print design</h5>
									<h4>RETHINK Kulturturisme</h4>
									<p>
										Udarbejdelse af hæfte for <strong>Midtjysk Turisme's</strong> RETHINK Kulturturisme
									</p>
								</div>
							</div>
						</li>
						<li class="wow zoomIn" data-wow-delay="0.4s">
							<div class="case-top">
							</div>
							
							<figure class="bottom-arrow">
								<img src="/images/portfolio/naturperlen_list.jpg" class="resp-img" alt="Case study Caseture project">
							</figure>
							<div class="case-desc">
								<div class="case-desc-inner">
									<h5>Skoleprojekt</h5>
									<h4>Naturperlen</h4>
									<p>
										Udarbejdelse af logo for <strong>Naturperlen</strong>, i forbindelse med skoleopgave
									</p>
								</div>
							</div>
						</li>
						<li class="wow zoomIn" data-wow-delay="0.6s">
							<div class="case-top">
							</div>
							
							<figure class="bottom-arrow">
								<img src="/images/portfolio/Danish-Entrepreneuship-Award-2014_list.jpg" class="resp-img" alt="Case study Caseture project">
							</figure>
							<div class="case-desc">
								<div class="case-desc-inner">
									<h5>Konkurrence</h5>
									<h4>Danish Entrepreneuship Award 2014</h4>
									<p>
										Konkurrenceforslag til <strong>Danish Entrepreneuship Award 2014</strong>
									</p>
								</div>
							</div>
						</li>
					</ul>
					<div class="spacing-70"></div>
					<h2 class="center">
						<strong>HIGH FIVE</strong>, du nåede bunden!
					</h2>
					<p class="cta">Kunne du lide mit arbejde? <a href="/planner" title="Start et projekt">Så start et projekt idag!</a>
					</div>
				</div>
			</section>
			
			<?php include('includes/bottom.php');?>
		</body>
		
	</html>
