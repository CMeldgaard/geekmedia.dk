<!DOCTYPE html>
<html lang="da">
	
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Administrer din e-mail, kalender og dine kontakter på en effektiv og professionel måde med en Hostex Exchange løsning">
		<title>Geek Media | Hosted Exchange</title>
		<?php include('includes/styles.php');?>
	</head>
	
	<body>
		
		<!-- Top Bar-->
		<?php include('includes/nav.php');?>
		<!-- End of Top Bar-->
		<!-- Domæne registrering -->
		<section class="hosted-exchange">
			<div class="breadcrumbs">
				<div class="row">
					<div class="col-sm-6">
						<h1>Hosted Exchange</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb">
							<li>Du er her: </li>
							<li><a href="/">Forside</a>
							</li>
							<li><a href="#">Hosting</a>
							</li>
							<li class="active">Hosted Exchange</li>
						</ol>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h2>Hosted Exchange</h2>
					<hr class="small">
					<p>Administrer din e-mail, kalender og dine kontakter
på en effektiv og professionel måde</p>
				</div>
			</div>
		</section>
		<section>
			<div class="spacing-70"></div>
			<div class="row ">
				<div class="col-sm-8 center-block">
					<h2>Adgang uanset hvor og hvilken enhed du er på</h2>
					<p>Hosted Exchange er et uundværligt værtøj for virksomheden,
der ønsker at effektivisere sin kommunikation og udstråle
professionalisme overfor sine kunder.</p>
					<p>
						Det giver dig og dine kollegaer mulighed for at synkronisere og
dele jeres e-mails, kontakter og kalendere med hinanden og på
tværs af jeres forskellige enheder såsom mobiltelefonen, tablet,
arbejdscomputeren og hjemme PC-en. Hvis blot du har adgang til
internettet, vil du med webmail, desuden kunne tilgå dine e-mails
fra enhver lokation</p>
					<p>
						Med Hosted Exchange kan du oprette en personlig mailboks
og mailadresse til alle jeres medarbejdere. Mailadresserne vil
desuden inkludere jeres domænenavn, så jeres kunder aldrig
vil være i tvivl om hvem en e-mail kommer fra. Der er ingen
begrænsninger og du kan selv vælge og løbende tilpasse hvor
mange konti du skal bruge og hvor meget plads de hver især skal
have.
					</p>
					<p>
						Systemet sørger for at opfange det meste spam og de fleste vira
med et integreret spamfilter. Derudover inkluderer løsningen
automatisk backup, som bliver gemt i det moderne og topsikre
datacenter, der også har ansvaret for hostingen af din løsning.
Herved vil du aldrig miste dine data eller e-mails, blot fordi din
telefon eller computer forulykker.
					</p>
				</div>
				<div class="col-md-12 jumpDown">
					<a class="btn btn-jump" href="#prices">Se priserne</a>
				</div>
			</div>
		</section>	
<div class="hosted-exchange-quote">
				<div class="row full-width no-gutter">
					<div class="col-sm-6 hosted-exchange-quote-column">
						<div class="thequote wow fadeInLeft" data-wow-delay="0.4s">
							<h5>Success is a science; if you have the conditions, you get the result.</h5>
							<span>Oscar Wilde</span>
						</div>
					</div>
				</div>
			</div>		
		<section class="pricingtables shared">
			<div class="spacing-70"></div>
			<div class="row">
				<div class="col-sm-12" id="prices">
					<h2>Vælg den plan der passer dig bedst</h2>
					<p>Forskellige planer der dækker lige præcist dine behov.</p>
				</div>
			</div>
			
			<div class="row spacing-40 no-gutter">
				
				<div class="col-sm-3 wow fadeInUp hostingfeatures" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; -webkit-animation-delay: 0.2s; animation-name: fadeInUp; -webkit-animation-name: fadeInUp;">
					<div class="panel panel-info">
						<div class="panel-heading">
						</div>
						<div class="panel-body text-center">
						</div>
						<ul class="text-left">
							<li>E-mail kommunikation
							</li>
							<li>Diskplads pr. bruger
							</li>
							<li>Ekstra domænealias
							</li>
							<li>Spamfilter & virusscanning
							</li>
							<li>Daglig backup af e-mails
							</li>
							<li><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Sammel alle dine e-mail adresser i en inbox">Ubegrænset antal e-mail
alias</a>
							</li>
							<li>Egen kalender
							</li>
							<li><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Alle data er SSL krypteret">Sikker kryptering med SSL</a>
							</li>
							<li><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Tilgå dine mails direkte fra en browser">Online webmail</a>
							</li>
							<li>Deling af kalender & mailboks
							</li>
							<li>Synkronisering til mobil/
tablet
							</li>
							<li>Microsoft Outlook licens
							</li>
						</ul>
						<div class="panel-footer">
							
						</div>
					</div>
				</div>		
		<div class="col-sm-3 wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; -webkit-animation-delay: 0.4s; animation-name: fadeInUp; -webkit-animation-name: fadeInUp;">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="text-center">POP3</h3>
				</div>
				<div class="panel-body text-center">
					<div class="monthprice">
						<h4>15,-</h4>
						<span class="per">PR. BRUGER/MD, EX MOMS</span>
					</div>
				</div>
				<ul class="text-center">
					<li>POP3</li>
					<li>500 MB</li>
					<li>Tilkøb</li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-times"></i></li>
					<li><i class="fa fa-times"></i></li>
					<li><i class="fa fa-times"></i></li>
					<li><i class="fa fa-times"></i></li>
					<li><i class="fa fa-times"></i></li>
					<li><i class="fa fa-times"></i></li>
	</ul>
	<div class="panel-footer">
		<a class="btn btn-lg btn-pricetable" href="/planner">START I DAG</a>
	</div>
</div>
</div>

<div class="col-sm-3 wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; -webkit-animation-delay: 0.4s; animation-name: fadeInUp; -webkit-animation-name: fadeInUp;">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="text-center">Exchange light</h3>
				</div>
				<div class="panel-body text-center">
					<div class="monthprice">
						<h4>35,-</h4>
						<span class="per">PR. BRUGER/MD, EX MOMS</span>
					</div>
				</div>
				<ul class="text-center">
					<li>OWA</li>
					<li>1 GB</li>
					<li>Tilkøb</li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-times"></i></li>
					<li><i class="fa fa-times"></i></li>
					<li><i class="fa fa-times"></i></li>
	</ul>
	<div class="panel-footer">
		<a class="btn btn-lg btn-pricetable" href="/planner">START I DAG</a>
	</div>
</div>
</div>

<div class="col-sm-3 wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; -webkit-animation-delay: 0.4s; animation-name: fadeInUp; -webkit-animation-name: fadeInUp;">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="text-center">Hosted Exchange</h3>
				</div>
				<div class="panel-body text-center">
					<div class="monthprice">
						<h4>75,-</h4>
						<span class="per">PR. BRUGER/MD, EX MOMS</span>
					</div>
				</div>
				<ul class="text-center">
					<li>OWA, IMAP, Exchange Outlook</li>
					<li>25 GB</li>
					<li>Tilkøb</li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li>Tilkøb</i></li>
	</ul>
	<div class="panel-footer">
		<a class="btn btn-lg btn-pricetable" href="/planner">START I DAG</a>
	</div>
</div>
</div>

</div>
<div class="spacing-70"></div>
</section>
		
		<?php include('includes/bottom.php');?>
		
		<script>
			$(document).ready(function() {
				$("#domainextensions").owlCarousel({
					autoPlay: 4000,
					items: 8,
					itemsDesktop: [1199, 6],
					itemsDesktopSmall: [979, 6],
					pagination: false
				});
				
					// ______________  TOOLTIPS
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
	});
	
	// ______________ TABS
	$('#shared-hosting-tabs').responsiveTabs({
		startCollapsed: 'accordion'
	});
				
			});
		</script>
		
	</body>
	
</html>
