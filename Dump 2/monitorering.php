<!DOCTYPE html>
<html lang="da">
	
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Utilgængelighed kan betyde tabt omsætning og dårligt image. Live monitorering af dit website">
		<title>Geek Media | Site monitorering</title>
		<?php include('includes/styles.php');?>
	</head>
	
	<body>
		
		<!-- Top Bar-->
		<?php include('includes/nav.php');?>
		<!-- End of Top Bar-->
		<!-- Domæne registrering -->
		<section class="site-monitorering">
			<div class="breadcrumbs">
				<div class="row">
					<div class="col-sm-6">
						<h1>Site monitorering</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb">
							<li>Du er her: </li>
							<li><a href="/">Forside</a>
							</li>
							<li><a href="#">Hosting</a>
							</li>
							<li class="active">Monitorering</li>
						</ol>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h2>Site monitorering</h2>
					<hr class="small">
					<p>Utilgængelighed kan betyde tabt omsætning og 
dårligt image</p>
				</div>
			</div>
		</section>
		<section>
			<div class="spacing-70"></div>
			<div class="row ">
				<div class="col-sm-8 center-block">
					<h2>Hurtigere respons på nedbrud</h2>
					<p>Den almindelige internetbruger bliver mere og mere utålmodig 
og er ikke villig til at vente ret længe, før vedkommende opgiver sit 
ærinde, især hvis der findes alternative sites. Det er derfor vigtigt 
hele tiden at have fokus på dit sites performance og tilgængelighed 
for at sikre brugernes tilfredshed. Målet er ikke kun at sikre 100 % 
oppetid, men i lige så høj grad at reducere svartiderne og handle 
hurtigt ved performanceproblemer.
</p>
					<p>
						GeekCheck er en internetbaseret service der giver overblik 
over jeres web performance 24/7/365. Systemet kræver ingen 
installation af hverken soft- eller hardware. På den måde sparer 
du kostbare interne ressourcer til at udvikle og vedligeholde 
systemer.</p>
					<p>
						Ved at foretage en forespørgsel på sitet udefra, simulerer 
GeekCheck en rigtig brugers bevægelser i webmiljøet. Dette er 
en af de store forskelle fra øvrige målesystemer, der som oftest 
måler på indersiden af firewall’en og for det meste alene på 
teknisk oppetid. Når målingen kun foretages internt, fortæller den 
således ikke noget om hvordan ISP og firewall komponenterne 
performer. Ligesom de heller ikke måler på hele indhol-det på en 
webside, og derved ikke giver et retvisende billede af svartiderne.
					</p>
					<p>
						Når GeekCheck finder performanceproblemer, registreres de i 
en overskuelig fejllog, og der sendes straks en e-mailalarm om 
problemet til de brugere, der er opsat for det pågældende check. 
Det betyder, at vi kan få rettet fejlen hurtigt.
					</p>
				</div>
				<div class="col-md-12 jumpDown">
					<a class="btn btn-jump" href="#prices">Se priserne</a>
				</div>
			</div>
		</section>	
<div class="site-monitorering-quote">
				<div class="row full-width no-gutter">
					<div class="col-sm-6 site-monitorering-quote-column">
						<div class="thequote wow fadeInLeft" data-wow-delay="0.4s">
							<h5>Success is a science; if you have the conditions, you get the result.</h5>
							<span>Oscar Wilde</span>
						</div>
					</div>
				</div>
			</div>		
		<section class="pricingtables shared">
			<div class="spacing-70"></div>
			<div class="row">
				<div class="col-sm-12" id="prices">
					<h2>Vælg den plan der passer dig bedst</h2>
					<p>Forskellige planer der dækker lige præcist dine behov.</p>
				</div>
			</div>
			
			<div class="row spacing-40 no-gutter">
				
				<div class="col-sm-3 wow fadeInUp hostingfeatures" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; -webkit-animation-delay: 0.2s; animation-name: fadeInUp; -webkit-animation-name: fadeInUp;">
					<div class="panel panel-info">
						<div class="panel-heading">
						</div>
						<div class="panel-body text-center">
						</div>
						<ul class="text-left">
							<li>Check rate
							</li>
							<li>Test lokationer
							</li>
							<li>Check pr. domæne
							</li>
							<li>Indholsmatch
							</li>
							<li><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Simulere den reelle loadtid af dit website i en browser">Browser testing*</a>
							</li>
							<li>Uptime branding på website
							</li>
							<li>SMS Alarmering
							</li>
							<li>Virus check
							</li>
							<li><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Hver 6. time udføres der et fuldt site scan for ødelagte links, både internt og eksternt samt 
billedefejl">Fuld scanning af website*</a>
							</li>
							<li>SSL monitorering
							</li>
						</ul>
						<div class="panel-footer">
							
						</div>
					</div>
				</div>		
		<div class="col-sm-3 wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; -webkit-animation-delay: 0.4s; animation-name: fadeInUp; -webkit-animation-name: fadeInUp;">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="text-center">GeekCheck</h3>
				</div>
				<div class="panel-body text-center">
					<div class="monthprice">
						<h4>100,-</h4>
						<span class="per">PR. MD, EX MOMS</span>
					</div>
				</div>
				<ul class="text-center">
					<li>5 minutter</li>
					<li>Tilfældig</li>
					<li>1</li>
					<li><i class="fa fa-times"></i></li>
					<li><i class="fa fa-times"></i></li>
					<li><i class="fa fa-times"></i></li>
					<li><i class="fa fa-times"></i></li>
					<li><i class="fa fa-times"></i></li>
					<li><i class="fa fa-times"></i></li>
					<li><i class="fa fa-times"></i></li>
	</ul>
	<div class="panel-footer">
		<a class="btn btn-lg btn-pricetable" href="/planner">START I DAG</a>
	</div>
</div>
</div>

<div class="col-sm-3 wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; -webkit-animation-delay: 0.4s; animation-name: fadeInUp; -webkit-animation-name: fadeInUp;">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="text-center">GeekCheck Pro</h3>
				</div>
				<div class="panel-body text-center">
					<div class="monthprice">
						<h4>200,-</h4>
						<span class="per">PR. MD, EX MOMS</span>
					</div>
				</div>
				<ul class="text-center">
					<li>1 minut</li>
					<li>8 faste</li>
					<li>10</li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-times"></i></li>
					<li><i class="fa fa-times"></i></li>
					<li><i class="fa fa-times"></i></li>
					<li><i class="fa fa-times"></i></li>
	</ul>
	<div class="panel-footer">
		<a class="btn btn-lg btn-pricetable" href="/planner">START I DAG</a>
	</div>
</div>
</div>

<div class="col-sm-3 wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; -webkit-animation-delay: 0.4s; animation-name: fadeInUp; -webkit-animation-name: fadeInUp;">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="text-center">GeekCheck VIP</h3>
				</div>
				<div class="panel-body text-center">
					<div class="monthprice">
						<h4>500,-</h4>
						<span class="per">PR. MD, EX MOMS</span>
					</div>
				</div>
				<ul class="text-center">
					<li>30 sekunder & 
kontstant</li>
					<li>Ubegrænset</li>
					<li>Ubegrænset</li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
					<li><i class="fa fa-check"></i></li>
	</ul>
	<div class="panel-footer">
		<a class="btn btn-lg btn-pricetable" href="/planner">START I DAG</a>
	</div>
</div>
</div>

</div>
<div class="spacing-70"></div>
</section>
		
		<?php include('includes/bottom.php');?>
		
		<script>
			$(document).ready(function() {
				$("#domainextensions").owlCarousel({
					autoPlay: 4000,
					items: 8,
					itemsDesktop: [1199, 6],
					itemsDesktopSmall: [979, 6],
					pagination: false
				});
				
					// ______________  TOOLTIPS
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
	});
	
	// ______________ TABS
	$('#shared-hosting-tabs').responsiveTabs({
		startCollapsed: 'accordion'
	});
				
			});
		</script>
		
	</body>
	
</html>
