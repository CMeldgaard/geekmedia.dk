<?php include 'includes/datacon.php'?>
<?php include 'includes/functions.php'?>
<?php
	$blogLink = str_replace(".php", "", $_GET["perma"]);
	
	if ($blogLink == "" ){
		header('Location: /blog');
		
		}else{
		$stmt = mysqli_prepare($conn, "SELECT * FROM blog INNER JOIN blogcategory ON blog.blogCategoryID = blogcategory.blogCategoryID INNER JOIN blogwriters on blog.createdBy = blogwriters.createdBy where blogLink=? order by createdDate desc");
		
		/* bind parameters for markers */
		mysqli_stmt_bind_param($stmt, "s", $blogLink);
		
		/* execute query */
		mysqli_stmt_execute($stmt);
		
		/* instead of bind_result: */
		$result = $stmt->get_result();
		
		/* now you can fetch the results into an array - NICE */
		while ($row = $result->fetch_assoc()) {
			
			setlocale(LC_ALL, "danish");
			$creaDate = strftime("%d", strtotime($row[createdDate]));
			$creaMonth = strftime("%b", strtotime($row[createdDate]));
			$metaTitle = $row[blogTitle];
			$metaDescription = (strlen(TrimShort(strip_tags($row["blogContent"]))) > 170) ? substr(TrimShort(strip_tags($row["blogContent"])),0,170) : TrimShort(strip_tags($row["blogContent"]));
			$facebookDescription = (strlen(TrimShort(strip_tags($row["blogContent"]))) > 170) ? substr(TrimShort(strip_tags($row["blogContent"])),0,170) : TrimShort(strip_tags($row["blogContent"]));
			
		?>
		
<!DOCTYPE html>
<html lang="da">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="<?php echo $metaDescription;?>">
		<title>Geek Media | <?php echo $row["blogTitle"]?></title>
		<?php include('includes/styles.php');?>
		
		<meta property="og:image" content="http://www.geekmedia.dk/images/article/<?php echo $row["blogImage"]?>"/>
		<meta property="og:description" content="<?php echo $facebookDescription;?>"/>
		<meta property="og:url" content="http://www.geekmedia.dk/blog/<?php echo $row["blogLink"]?>"/>
		<meta property="og:title" content="Geek Media | <?php echo $row["blogTitle"]?>"/>
	</head>
	
	<body>
		
		<!-- Top Bar-->
		<?php include('includes/nav.php');?>
		<!-- End of Top Bar-->
		<div class="breadcrumbs mb0 solid">
			<div class="row">
				<div class="col-sm-3">
					<h1>Blog</h1>
				</div>
				<div class="col-sm-9">
					<ol class="breadcrumb">
						<li>Du er her: </li>
						<li><a href="/">Forside</a>
						</li>
						<li><a href="/blog">Blog</a>
							</li>
						<li class="active"><?php echo $row["blogTitle"]?></li>
					</ol>
				</div>
			</div>
		</div>

			<div class="blog">
        <div class="row">
            <div class="col-sm-8">						
				<!-- Blog Post-->
				<article>
					<img src="/images/article/<?php echo $row["blogImage"]?>" class="resp-img" alt="<?php echo $row["blogTitle"]?>">
					<div class="post-content">
						<h2><?php echo $row["blogTitle"]?></h2>
						<div class="thedate"><?php echo $row[createdDate]?> | I <a href="/blog/kategori/<?php echo $row["catURL"]?>"><?php echo $row["catName"]?></a></div>
						<hr>
						<p><?php echo $row["blogContent"]?></p>
					</div>
				</article>
				<!-- End of Blog Post-->
            </div>
<?php 								}
							}		?>
            
			<?php include('includes/sidebar-blog.php');?>

        </div>
    </div>


			<!-- End of About Us -->
			
			<?php include('includes/bottom.php');?>
			<script>
				$(document).ready(function() {
					$("#values").owlCarousel({
						items: 1,
						autoPlay: 5000,
						itemsDesktop: [1199, 1],
						itemsDesktopSmall: [979, 1],
						itemsTablet: [768, 1]
					});
					
					$("#testimonials-carousel").owlCarousel({
						items: 1,
						autoPlay: 5000,
						itemsDesktop: [1199, 1],
						itemsDesktopSmall: [979, 1],
						itemsTablet: [768, 1]
					});
				});
			</script>
		</body>
		
	</html>
