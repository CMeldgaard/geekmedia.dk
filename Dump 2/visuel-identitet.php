<!DOCTYPE html>
<html lang="da">
	
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Visuel Identitet er ikke blot ét flot design, men en sammenhængende fortælling af firmaet der præsenter virksomheden til omverdenen">
		<title>Geek Media | Visuel identitet</title>
		<?php include('includes/styles.php');?>
	</head>
	
	<body>
		
		<!-- Top Bar-->
		<?php include('includes/nav.php');?>
		<!-- End of Top Bar-->
		<!-- About Us -->
		<section class="visuel-identitet">
			<div class="breadcrumbs">
				<div class="row">
					<div class="col-sm-6">
						<h1>Visuel identitet</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb">
							<li>Du er her: </li>
							<li><a href="/">Forside</a>
							</li>
							<li><a href="#">Design</a>
							</li>
							<li class="active">Visuel identitet</li>
						</ol>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h2>Visuel identitet</h2>
					<hr class="small">
					<p>Visuel Identitet er ikke blot ét flot design, men en 
					sammenhængende fortælling af firmaet</p>
				</div>
			</div>
		</section>
		<section>
			<div class="spacing-70"></div>
			<div class="row ">
				<div class="col-sm-8 center-block">
					<h2>Visuel Identitet er ikke blot ét flot design</h2>
					<p>En virksomheds visuelle identitet er meget vigtig, da den 
						præsenter virksomheden til omverdenen og derfor skal afspejle 
						virksomhedens kvalitet og branche. Den visuelle identitet skal 
						bruges som et værktøj til at skabe fundamentet for virksomhedens 
					positionering, branding og markedsføring.</p>
					<p>
						For at skabe en god visuel identitet, kræver det indsigt i 
						virksomheden, dens produkter, værdier og vision. Der skal skabes 
						et overordnet tema, alle de grafiske elementer afhænger af, hvilket 
						både kræver erfaring og faglig kompetence.
					</p>
					<p>En visuel identitet skal være integreret dybt med virksomhedens 
						allerede lagte strategi, og være med til at underbygge denne. 
						Den skal afspejle niveauet og kvaliteten af produkter og ydelser 
						fra virksomheden. Den skal være med til at støtte op om alle 
						medier og tiltag hos virksomheden, da den røde tråd skal 
					fremhæves, for at gennemskueliggøre virksomheden for kunden.</p>
					<p>Jeg skaber din komplette identitet, med grafik og temaer der kan 
					genbruges på alle dine medier, som f.eks. brevpapir, vi-sitkort osv. </p>
				</div>
				<div class="spacing-70"></div>
				<div class="col-sm-8 center-block">
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.2s" id="brand_1">
						<h3>Research & analyse</h3>
						<p>Dybdegående brand opdagelse der dækker markedsundersøgelser, demografi, målgrupper og konkurrentanalyse.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.4s" id="brand_2">
						<h3>Holistiske strategier</h3>
						<p>
							Skitsere retningen for hele projektet, herunder eksterne kommunikationer som social og digital markedsføring.
						</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.6s" id="brand_3">
						<h3>Fremtids sikring</h3>
						<p>Rådgivning om eksisterende teknologier og anbefalinger, der skaber muligheder for at udvikle sig med det digitale rum.</p>
					</div>
				</div>
				<div class="col-md-12 jumpDown">
					<a class="btn btn-jump" href="#kampagne">Læs mere</a>
				</div>
			</div>
			
			<div class="spacing-70"></div>
			
			<!--  Call to Action -->
			<section class="calltoaction">
				
				<div class="row wow zoomIn" data-wow-delay="0.2s">
					<div class="col-sm-10 com-md-8 center-block">
						
						<div class="row no-gutter cta-content">
							<div class="col-sm-4">
								<div class="offer wow fadeInUp" data-wow-delay="0.5s">
									<span>LAD OS</span>
									<a href="/planner"><h2>STARTE</h2></a>
									<span>I DAG</span>
								</div>
							</div>
							<div class="col-sm-8">
								
								<div class="offerdescription wow fadeInUp" data-wow-delay="0.7s">
									<h2>START ALLEREDE I DAG</h2>
									<p>ved at udfylde min online formular og få en uforpligtende samtale</p>
								</div>
							</div>
						</div>
					</div>	
				</div>
				
			</section>
			<!-- End of Call to Action -->
			<div class="spacing-70"></div>
			<div class="row ">
				<div class="col-sm-8 center-block" id="kampagne">
					<h2>Kampagne tema</h2>
					<p>
					Står i og skal have lanceret en bestemt kampagne, kan vi sammen finde et tema der passer til jeres eksisterende identitet og logo, hvorved der fastholdes i den vigtige røde tråd i jeres grafiske materiale. Jeg producerer alle materialerne til kampagnen, som fx brochurer, plakater, udendørs bannere, web og meget mere.
					</p>
					<p class="cta">Klar til at brande? <a href="/planner" title="Start et projekt">Kontakt mig i dag!</a></p>
				</div>
			</div>
		</section>
	
	<!-- End of About Us -->
	
	<?php include('includes/bottom.php');?>
</body>

</html>
