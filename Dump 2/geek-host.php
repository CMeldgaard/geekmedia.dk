<!DOCTYPE html>
<html lang="da">
	
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Giv din hjemmeside de absolut bedste vilkår, med en høj stabilitet og super hurtige svartider">
		<title>Geek Media | Geek Host</title>
		<?php include('includes/styles.php');?>
	</head>
	
	<body>
		
		<!-- Top Bar-->
		<?php include('includes/nav.php');?>
		<!-- End of Top Bar-->
		<!-- About Us -->
		<section class="geekhost">
			<div class="breadcrumbs">
				<div class="row">
					<div class="col-sm-6">
						<h1>Geek Host</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb">
							<li>Du er her: </li>
							<li><a href="/">Forside</a>
							</li>
							<li><a href="#">Hosting</a>
							</li>
							<li class="active">Geek Host</li>
						</ol>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h2>Geek Host</h2>
					<hr class="small">
					<p>Giv din hjemmeside de absolut bedste vilkår, med 
					en høj stabilitet og super hurtige svartider</p>
				</div>
			</div>
		</section>
		<section>
			<div class="spacing-70"></div>
			<div class="row ">
				<div class="col-sm-8 center-block">
					<h2>Det rigtige valg</h2>
					<p>Langt de fleste virksomheder, organisationer eller endda 
						private har i dag en online profil i form af en hjemmeside, blog, 
						webshop eller lignende. Derfor forventer langt de fleste kunder 
						også, at kunne finde dig eller din virksomhed på internettet, at 
						kunne fremsøge dine produkter, danne sig et indtryk af din 
						virksomhed eller have et sted at kontakte dig, når de er klar til at 
					initiere et salg.</p>
					<p>
						Webhotellerne jeg tilbyder er ikke udelukkende bygget op efter, 
						at kunne tilbyde dig den absolut laveste pris. Rigtig mange overser 
						desværre vigtigheden af, at ens hjemmeside eller webshop 
						er sikret med en høj oppetid, sikkerhed mod hacking, effektive 
						backuprutiner, hurtige svartider osv. der alle er elementer, som 
						har stor indvirkning på den oplevelse som besøgende eller 
					kunder erfarer.</p>
					<p>
						Der er investeret rigtig store summer i det datacenter og netværk 
						hvor jeg hoster fra, for netop at optimere slutbruger-oplevelsen 
						når de besøger et webhotel, som er hosted hos mig.
					</p>
					<p>
						Derfor vil du heller ikke opleve, at serverne fyldes op til 
						bristepunktet, således at trafikken fra andre brugere vil få 
						indflydelse på ydeevnen fra dit webhotel, som man så ofte ser det 
						hos lavprisselskaberne.
					</p>
				</div>
				<div class="col-md-12 jumpDown">
					<a class="btn btn-jump" href="#features">Læs mere</a>
				</div>
			</div>
			
			<div class="spacing-70"></div>
		</section>
		<!--  Call to Action -->
		<section class="calltoaction">
			
			<div class="row wow zoomIn" data-wow-delay="0.2s">
				<div class="col-sm-10 com-md-8 center-block">
					
					<div class="row no-gutter cta-content">
						<div class="col-sm-4">
							<div class="offer wow fadeInUp" data-wow-delay="0.5s">
								<span>LAD OS</span>
								<a href="/planner"><h2>STARTE</h2></a>
								<span>I DAG</span>
							</div>
						</div>
						<div class="col-sm-8">
							
							<div class="offerdescription wow fadeInUp" data-wow-delay="0.7s">
								<h2>START ALLEREDE I DAG</h2>
								<p>ved at udfylde min online formular og få en uforpligtende samtale</p>
							</div>
						</div>
					</div>
				</div>	
			</div>
			
		</section>
		<!-- End of Call to Action -->
		<div class="spacing-70"></div>
		<section class="shared-features" id="features">
			<div class="row">
				<div class="col-sm-12">
					<div class="wow fadeInLeft" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; -webkit-animation-delay: 0.2s; animation-name: fadeInLeft; -webkit-animation-name: fadeInLeft;">
						<h2>Mere end blot webhosting</h2>
						<p>Geek Host er ikke blot et webhotel, men det er et datacenter der er udviklet med kunderne for øje.</p>
					</div>
					
					<div id="shared-hosting-tabs" class="wow fadeInRight spacing-70 r-tabs" data-wow-delay="0.4s">
						<ul class="r-tabs-nav">
							<li><a href="#feature1" class="r-tabs-anchor"> Høj Performance </a>
							</li>
							<li><a href="#feature2" class="r-tabs-anchor"> Udvidet sikkerhed </a>
							</li>
							<li><a href="#feature3" class="r-tabs-anchor"> Uovertruffen support </a>
							</li>
							<li><a href="#feature4" class="r-tabs-anchor"> 99.9% Oppetid </a>
							</li>
						</ul>
						
						<!-- 1st tab  -->
						<div id="feature1" class="r-tabs-panel">
							<div class="centralicon"><img src="/images/plane.png" alt="">
							</div>
							<div class="tabfeatures">
								<div class="row">
									<div class="col-sm-7">
										<img src="/images/shared-img-1.png" alt="">
									</div>
									
									<div class="col-sm-5">
										<h5>State-of-the-art datacenter</h5>
										<p>Datacenteret er tilkoblet 2 x 10 Gbit fiber forbindelser - den ene til TDC 
											og den anden til Telia. Eftersom TDC og Telia benytter hver deres fiberring, er hele ruten mellem dig som kunde og 
											datacenteret desuden redundant - hvilket betyder, at uanset hvor i kæden 
											der opstår problemer, så vil din hostede løsning fortsat fungere uden 
										problemer.</p>
									</div>
								</div>
							</div>
						</div>
						
						<!-- 2nd tab  -->
						<div id="feature2" class="r-tabs-state-default r-tabs-panel">
							<div class="centralicon"><img src="/images/chair.png" alt="">
							</div>
							<div class="tabfeatures">
								<div class="row">
									<div class="col-sm-12">
										<h5>Slap af. 24 timers vagtordning sikre dine data hele året rundt.</h5>
										<p>Sikkerhedsprocedurer og foranstaltninger kan være nok så 
										vigtige, men datacenterets topprioritet er, at du føler dig tryg</p>
										
										<div class="row spacing-70">
											<div class="col-sm-4">
												<img src="images/shared-3.png" alt="">
												<h6>Hacking</h6>
												<p>Netværksinfrastrukturen er optimeret til at 
													kunne modstå alle former for hackerangreb (heriblandt DoS- og DDoS 
													angreb) på en sådan måde, at du som kunde aldrig vil komme til at mærke 
												til det.</p>
											</div>
											
											<div class="col-sm-4">
												<img src="images/shared-4.png" alt="">
												<h6>Firewall</h6>
												<p>Alle arbejdsmaskiner og servere er udstyret med firewalls og antivirus software, der har til hensigt at blokere alle former for vira mv.</p>
											</div>
											
											<div class="col-sm-4">
												<img src="images/shared-5.png" alt="">
												<h6>Fejl alarmering</h6>
												<p>Alle parametre i datacenteret bliver overvåget, og ved de mindst udsving bliver vagtpersonalet alarmeret og påbegynder fejlfinding på systemet.</p>
											</div>
											
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<!-- 3rd tab  -->
						<div id="feature3" class="r-tabs-state-default r-tabs-panel">
							<div class="centralicon"><img src="images/support.png" alt="">
							</div>
							<div class="tabfeatures">
								<div class="row">
									<div class="col-sm-12">
										<h5>Uovertruffen support. Jeg er der når du har brug for hjælp.</h5>
									</div>
									
									<div class="row spacing-40">
										<div class="col-sm-12">
											
											<div class="block-grid-xs-1 block-grid-sm-3 block-grid-md-3 supportchannels">
												
												<div class="block-grid-item">
													<i class="fa fa-comments-o"></i>
													<h6>Live Chat</h6>
													<p>Få hjælp via min Live Chat alle hverdage fra 8-16.</p>
												</div>
												<div class="block-grid-item">
													<a href="/geek-partner"><i class="fa fa-pencil-square-o"></i></a>
													<h6>Support Tickets</h6>
													<p>Opret support tickets via min Geek Partner aftale.</p>
												</div>
												<div class="block-grid-item">
													<i class="fa fa-twitter"></i>
													<h6>Social Media</h6>
													<p>Find mig og stil supportspørgsmål på mine sociale medier. </p>
												</div>
												
											</div>
											
										</div>
										
									</div>
								</div>
							</div>
						</div>
						
						<!-- 4th tab  -->
						<div id="feature4" class="r-tabs-state-default r-tabs-panel">
							<div class="centralicon"><img src="images/shared-1.png" alt="">
							</div>
							<div class="tabfeatures">
								<div class="row">
									
									<div class="col-sm-5">
										<h5>SLA på 99,9% hver måned</h5>
										<p>Alle webservere drives på højtydende Windows og Linux/unix servere. Der tages backup af alle data, og systemerne er overvåget 24 timer i døgnet, hvilket sikrer at webhotellet har en oppetid på over 99,9%.</p>
									</div>
									
									<div class="col-sm-7">
										<img src="images/shared-img-2.png" alt="">
									</div>
								</div>
							</div>
							
							
						</div>
					</div>
				</div>
				<div class="col-md-12 jumpDown">
					<a class="btn btn-jump" href="#prices">Se priserne</a>
				</div>
			</div>
		</section>
		
		<div class="about-quote odd">
			<div class="row full-width no-gutter">
				<div class="col-sm-6 col-sm-push-6 about-quote2-column">
					<div class="thequote wow fadeInRight" data-wow-delay="0.4s">
						<h5>It does not matter how slowly you go as long as you do not stop.</h5>
						<span>Confucius</span>
					</div>
				</div>
			</div>
		</div>
		
		<section class="pricingtables shared">
			<div class="spacing-70"></div>
			<div class="row">
				<div class="col-sm-12" id="prices">
					<h2>Vælg den plan der passer dig bedst</h2>
					<p>Forskellige planer der dækker lige præcist dine behov.</p>
				</div>
			</div>
			
			<div class="row spacing-40 no-gutter">
				
				<div class="col-sm-3 wow fadeInUp hostingfeatures" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; -webkit-animation-delay: 0.2s; animation-name: fadeInUp; -webkit-animation-name: fadeInUp;">
					<div class="panel panel-info">
						<div class="panel-heading">
						</div>
						<div class="panel-body text-center">
						</div>
						<ul class="text-left">
							<li>Platform
							</li>
							<li>Webserver
							</li>
							<li><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Serverplads bruges både til website samt mails">Serverplads</a>
							</li>
							<li>Scriptsprog
							</li>
							<li>Antal tilknyttede 
								domæner
							</li>
							<li>Database
							</li>
							<li>MSSQL database
							</li>
							<li><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Opsæt udførrelse af automatiserede processor">Cronjobs</a>
							</li>
							<li><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Forbind Frontpage til webserveren">Frontpage extensions</a>
							</li>
							<li>POP3 postkasser
							</li>
							<li>IMAP
							</li>
							<li>Antivirus
							</li>
							<li><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Check din mail overalt i verden">Webmail</a>
							</li>
							<li>DNS administration
							</li>
							<li><a href="/monitorering" data-toggle="tooltip" data-placement="top" data-original-title="Geek Medias 24/7 site monitorering">GeekCheck</a>
							</li>
						</ul>
						<div class="panel-footer">
							
						</div>
					</div>
				</div>
				
				<div class="col-sm-2 wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; -webkit-animation-delay: 0.4s; animation-name: fadeInUp; -webkit-animation-name: fadeInUp;">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 class="text-center">Windows</h3>
						</div>
						<div class="panel-body text-center">
							<div class="monthprice">
								<h4>100,-</h4>
								<span class="per">PER MÅNED, EX MOMS</span>
							</div>
						</div>
						<ul class="text-center">
							<li>Windows</li>
							<li>IIS</li>
							<li>10 GB</li>
							<li>ASP.NET 4.5</li>
							<li><div class="visible-xs">Trafik</div>Ubegrænset</li>
							<li>MS Access, 
							MySQL*</li>
							<li><div class="visible-xs">MSSQL Database</div>Tilkøb</li>
							<li><i class="fa fa-times"></i><div class="visible-xs">Cronjobs</div></li>
							<li><div class="visible-xs">Frontpage Extension</div>Tilkøb</li>
							<li>Ubegrænset<div class="visible-xs"> POP3</div></li>
							<li>Tilkøb<div class="visible-xs">IMAP</div></li>
							<li><i class="fa fa-check"></i><div class="visible-xs">Antivirus</div></li>
							<li><i class="fa fa-check"></i><div class="visible-xs">Webmail</div></li>
							<li><i class="fa fa-check"></i><div class="visible-xs">DNS Administration</div></li>
							<li><i class="fa fa-check"></i><div class="visible-xs">GeekCheck</div></li>
						</ul>
						<div class="panel-footer">
							<a class="btn btn-lg btn-pricetable" href="/planner">START I DAG</a>
						</div>
					</div>
				</div>
				
				<div class="col-sm-2 wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; -webkit-animation-delay: 0.4s; animation-name: fadeInUp; -webkit-animation-name: fadeInUp;">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 class="text-center">Linux</h3>
						</div>
						<div class="panel-body text-center">
							<div class="monthprice">
								<h4>100,-</h4>
								<span class="per">PER MÅNED, EX MOMS</span>
							</div>
						</div>
						<ul class="text-center">
							<li>Linux</li>
							<li>Apache</li>
							<li>10 GB</li>
							<li>PHP 5.3</li>
							<li><div class="visible-xs">Trafik</div>Ubegrænset</li>
							<li>MySQL</li>
							<li><i class="fa fa-times"></i><div class="visible-xs">MSSQL Database</div></li>
							<li><div class="visible-xs">Cronjobs</div>Tilkøb</li>
							<li><i class="fa fa-times"></i><div class="visible-xs">Frontpage Extension</div></li>
							<li>Ubegrænset<div class="visible-xs"> POP3</div></li>
							<li>Tilkøb<div class="visible-xs">IMAP</div></li>
							<li><i class="fa fa-check"></i><div class="visible-xs">Antivirus</div></li>
							<li><i class="fa fa-check"></i><div class="visible-xs">Webmail</div></li>
							<li><i class="fa fa-check"></i><div class="visible-xs">DNS Administration</div></li>
							<li><i class="fa fa-check"></i><div class="visible-xs">GeekCheck</div></li>
						</ul>
						<div class="panel-footer">
							<a class="btn btn-lg btn-pricetable" href="/planner">START I DAG</a>
						</div>
					</div>
				</div>
				
				<div class="col-sm-2 wow fadeInUp" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; -webkit-animation-delay: 0.6s; animation-name: fadeInUp; -webkit-animation-name: fadeInUp;">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 class="text-center">Windows Pro</h3>
						</div>
						<div class="panel-body text-center">
							<div class="monthprice">
								<h4>150,-</h4>
								<span class="per">PER MÅNED, EX MOMS</span>
							</div>
						</div>
						<ul class="text-center">
							<li>Windows</li>
							<li>IIS</li>
							<li>25 GB</li>
							<li>ASP.NET 4.5
							PHP 5.3</li>
							<li><div class="visible-xs">Trafik</div>Ubegrænset</li>
							<li>MS Access, 
							MySQL*</li>
							<li><div class="visible-xs">MSSQL Database</div>Tilkøb</li>
							<li><i class="fa fa-times"></i><div class="visible-xs">Cronjobs</div></li>
							<li><i class="fa fa-times"></i><div class="visible-xs">Frontpage Extension</div></li>
							<li>Ubegrænset<div class="visible-xs"> POP3</div></li>
							<li><i class="fa fa-check"></i><div class="visible-xs">IMAP</div></li>
							<li><i class="fa fa-check"></i><div class="visible-xs">Antivirus</div></li>
							<li><i class="fa fa-check"></i><div class="visible-xs">Webmail</div></li>
							<li><i class="fa fa-check"></i><div class="visible-xs">DNS Administration</div></li>
							<li><i class="fa fa-check"></i><div class="visible-xs">GeekCheck</div></li>
						</ul>
						<div class="panel-footer">
							<a class="btn btn-lg btn-pricetable" href="/planner">START I DAG</a>
						</div>
					</div>
				</div>
				
				<div class="col-sm-2 wow fadeInUp" data-wow-delay="0.8s" style="visibility: visible; animation-delay: 0.8s; -webkit-animation-delay: 0.8s; animation-name: fadeInUp; -webkit-animation-name: fadeInUp;">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 class="text-center">Linux Pro</h3>
						</div>
						<div class="panel-body text-center">
							<div class="monthprice">
								<h4>150,-</h4>
								<span class="per">PER MÅNED, EX MOMS</span>
							</div>
						</div>
						<ul class="text-center">
							<li>Linux</li>
							<li>Litespeed</li>
							<li>25 GB</li>
							<li>PHP 5.5</li>
							<li><div class="visible-xs">Trafik</div>Ubegrænset</li>
							<li>MySQL</li>
							<li><i class="fa fa-times"></i><div class="visible-xs">MSSQL Database</div></li>
							<li><div class="visible-xs">Cronjobs</div>Tilkøb</li>
							<li><i class="fa fa-times"></i><div class="visible-xs">Frontpage Extension</div></li>
							<li>Ubegrænset<div class="visible-xs"> POP3</div></li>
							<li><i class="fa fa-check"></i><div class="visible-xs">IMAP</div></li>
							<li><i class="fa fa-check"></i><div class="visible-xs">Antivirus</div></li>
							<li><i class="fa fa-check"></i><div class="visible-xs">Webmail</div></li>
							<li><i class="fa fa-check"></i><div class="visible-xs">DNS Administration</div></li>
							<li><i class="fa fa-check"></i><div class="visible-xs">GeekCheck</div></li>
						</ul>
						<div class="panel-footer">
							<a class="btn btn-lg btn-pricetable" href="/planner">START I DAG</a>
						</div>
					</div>
				</div>
				
			</div>
			<div class="spacing-70"></div>
		</section>
		
		<!-- End of About Us -->
	
	<?php include('includes/bottom.php');?>
	
	<script type="text/javascript">
		// ______________  TOOLTIPS
		$(document).ready(function() {
			$('[data-toggle="tooltip"]').tooltip();
		});
		
		// ______________ TABS
		$('#shared-hosting-tabs').responsiveTabs({
			startCollapsed: 'accordion'
		});
	</script>
</body>

</html>
