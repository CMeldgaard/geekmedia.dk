<!DOCTYPE html>
<html lang="da">
	
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Form og funktion gennem spændende grænseflader og menneskelige oplevelser">
		<title>Geek Media | Design & UX</title>
		<?php include('includes/styles.php');?>
	</head>
	
	<body>
		
		<!-- Top Bar-->
		<?php include('includes/nav.php');?>
		<!-- End of Top Bar-->
		<!-- About Us -->
		<section class="design-ux">
			<div class="breadcrumbs">
				<div class="row">
					<div class="col-sm-6">
						<h1>Design & UX</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb">
							<li>Du er her: </li>
							<li><a href="/">Forside</a>
							</li>
							<li><a href="#">Design</a>
							</li>
							<li class="active">Design & UX</li>
						</ol>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h2>Design & UX</h2>
					<hr class="small">
					<p>Form og funktion gennem spændende 
grænseflader og menneskelige oplevelser</p>
				</div>
			</div>
		</section>
		<section>
			<div class="spacing-70"></div>
			<div class="row ">
				<div class="col-sm-8 center-block">
					<h2>Bedre oplevelse gennem design</h2>
					<p>Visuel engagement og en imponerende brugeroplevelse i 
det moderne internet er afgørende for vellykkede projekter, 
mindeværdige brands og glade brugere. Som et bureau med 
fokus på menneskelige erfaringer, mener jeg, at kvaliteten af 
et design har en afgørende indflydelse på succesen af et hvert 
projekt. </p>
<p>
Jeg anerkender, at hvert projekt er unikt, og at der kræves sans for 
detaljer for at nå frem til det ultimative og mest givende resultat.
	</p>
	</div>
	<div class="spacing-70"></div>
				<div class="col-sm-8 center-block">
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.2s" id="design_1">
						<h3>Design strategi</h3>
						<p>Målbevidste anbefalinger baseret på konstruktiv design konsultation, brand analyse og gennemgang af målsætninger.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.4s" id="design_2">
						<h3>Informations planlægning</h3>
						<p>Publikum og mål analyse til at udvikle forløb til flot design, opmuntrende enkelhed og klarhed i struktur.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.6s" id="design_3">
						<h3>Interface design</h3>
						<p>Detaljeret konceptualisering af alle centrale elementer i et digitalt projekt til fremme af målbevidste og intelligente grænseflader.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.2s" id="design_4">
						<h3>User experience</h3>
						<p>Løsningsforslag til at fremme engagement gennem enkle, intuitive og dejlige oplevelser på tværs af alle enheder.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.4s" id="design_5">
						<h3>Prototype</h3>
						<p>Udforskning af potentielle koncepter for på tværs af enheder oplevelser gennem wireframes og digitale prototyper</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.6s" id="design_6">
						<h3>Interaktion</h3>
						<p>Udnyttelse af det digitales interaktive natur med unikke muligheder i engagement gennem bruger drevet grænseflader.</p>
					</div>
				</div>
				<br style="clear:both;" />
				<div class="col-md-12 jumpDown">
					<a class="btn btn-jump" href="#cms">Læs mere</a>
				</div>
			</div>
			
			<div class="spacing-70"></div>
			
			<!--  Call to Action -->
    <section class="calltoaction">

        <div class="row wow zoomIn" data-wow-delay="0.2s">
            <div class="col-sm-10 com-md-8 center-block">

                <div class="row no-gutter cta-content">
                    <div class="col-sm-4">
                        <div class="offer wow fadeInUp" data-wow-delay="0.5s">
                            <span>LAD OS</span>
                            <a href="/planner"><h2>STARTE</h2></a>
                            <span>I DAG</span>
                        </div>
                    </div>
                    <div class="col-sm-8">

                        <div class="offerdescription wow fadeInUp" data-wow-delay="0.7s">
                            <h2>START ALLEREDE I DAG</h2>
                            <p>ved at udfylde min online formular og få en uforpligtende samtale</p>
                        </div>
                    </div>
                </div>
            </div>	
        </div>

    </section>
    <!-- End of Call to Action -->
			<div class="spacing-70"></div>
			<div class="row ">
				<div class="col-sm-8 center-block" id="cms">
					<h2>Form med funktion</h2>
						<p>Jeg fokuserer på at udvikle grænseflader af høj kvalitet og 
meningsfulde oplevelser, der forbedrer de digitale projekter og 
fremme formålet. Jeg får digitalt til at se godt, men kun for at få 
det til at fungere endnu bedre.</p>
					<p class="cta">Klar til at interagere? <a href="/planner" title="Start et projekt">Kontakt mig i dag!</a></p>
				</div>
			</div>
		</section>

<!-- End of About Us -->

<?php include('includes/bottom.php');?>
</body>

</html>
