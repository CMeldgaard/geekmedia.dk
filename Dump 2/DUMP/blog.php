<?php

	if ($_SERVER['REQUEST_METHOD']=="POST"){
		// Collect the posted search query
		$q = $_POST["searchStr"];
		
		// Clean up by removing unwanted characters
		$qclean = ereg_replace("[^ 0-9a-zA-Z]", " ", $q);
		
		// Remove multiple adjacent spaces
		while (strstr($qclean, "  ")) {
			$qclean = str_replace("  ", " ", $qclean);
		}
		
		// Replace single spaces with a URL friendly plus sign
		$qclean = str_replace(" ", "+", $qclean);
		
		// If validation has passed, redirect to the URL rewritten search page
		header( 'Location: /blog/sog/'.$q );
		// HTML is output after here – NOT BEFORE!
	}	
?>

<?php include '/includes/datacon.php' ?>
<!DOCTYPE html>
<html lang="da">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title>Geek Media | Blog | <?php echo $q;?></title>
		
		<meta name="description" content="">
		<meta name="keywords" content="">
		<?php include '/includes/headerstyles.php' ?>
	</head>	
	<body>
		<?php include '/includes/nav.php' ?>
		
		<div id="loadLayer">
			<div class="mask"><div class="logo"><div class="mask"></div><div class="bg"></div></div>	</div>
		</div>
		<div id="contentSpace">
			<div id="contentSpaceContainer">
				<div id="container_wrapper">
					<div id="container" class="container">
						<main id="content" class="content">
							<?php include('/includes/header.php')?>
							<section>
								<div class="inner welcome">
									<h1>Idéer, <span class="red">tanker</span> & indsigt.</h1>
									<p class="subHeadline">
										Se hvad der rumsterer rundt i mit hoved af tanker og idéer, og alverdens andre ting! Lyst til at skrive et indlæg, så <a href="/kontakt" class="cursive"> kontakt</a> mig i dag
									</p>
								</div>
								<svg version="1.1" class="svg-diagonal  bottom-left" height="79" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 3315 79" enable-background="new 0 0 3315 79" xml:space="preserve" preserveAspectRatio="none">
									<g>
										<g><polygon fill-rule="evenodd" clip-rule="evenodd" fill="#F7F7F5" points="-1,-1 -1,80 3316,80"></polygon></g>
									</g>
								</svg>
							</section>
							<section class="dark">
								<div class="inner blog">
									<div class="col-md-8">
										<?php
											$cat = $_GET["cat"];
											$search = $_GET["search"];
											$search2 = $search;
											//Checks for category selection, if none, show all sorted by date
											if ($cat == "" and $search=="") {
												$sqlString = "SELECT LEFT(blogContent, 550) as shortBlog, blog.blogCategoryID, blogID, blogImage, blogLink, blogTitle, blog.createdBy, createdDate, blogCategory.*, blogWriters.* FROM blog INNER JOIN blogCategory ON blog.blogCategoryID = blogCategory.blogCategoryID INNER JOIN blogWriters on blog.createdBy = blogWriters.createdBy order by createdDate desc LIMIT 5";
												$result = mysqli_query($conn, $sqlString);
												}else{
												$stmt = mysqli_prepare($conn, "SELECT LEFT(blogContent, 550) as shortBlog, blog.blogCategoryID, blogID, blogImage, blogLink, blogTitle, blog.createdBy, createdDate, blogCategory.*, blogWriters.* FROM blog INNER JOIN blogCategory ON blog.blogCategoryID = blogCategory.blogCategoryID INNER JOIN blogWriters on blog.createdBy = blogWriters.createdBy where catURL=? order by createdDate desc");
												
												/* bind parameters for markers */
												mysqli_stmt_bind_param($stmt, "s", $cat);
												
												/* execute query */
												mysqli_stmt_execute($stmt);
												
												/* instead of bind_result: */
												$result = $stmt->get_result();
											}
											
											if ($search!=""){
												$stmt = mysqli_prepare($conn, "SELECT LEFT(blogContent, 550) as shortBlog, blog.blogContent, blog.blogCategoryID, blogID, blogImage, blogLink, blogTitle, blog.createdBy, createdDate, blogCategory.*, blogWriters.* FROM blog INNER JOIN blogCategory ON blog.blogCategoryID = blogCategory.blogCategoryID INNER JOIN blogWriters on blog.createdBy = blogWriters.createdBy
												WHERE MATCH(blogTitle, blogContent) AGAINST(?,?)
												order by createdDate desc");
												
												/* bind parameters for markers */
												mysqli_stmt_bind_param($stmt, "ss", $search, $search2);
												
												/* execute query */
												mysqli_stmt_execute($stmt);
												
												/* instead of bind_result: */
												$result = $stmt->get_result();
											}
											
											if (mysqli_num_rows($result) > 0) {
												// output data of each row
												while($row = mysqli_fetch_assoc($result)) {  
													
													setlocale(LC_ALL, "danish");
													$creaDate = strftime("%d", strtotime($row[createdDate]));
													$creaMonth = strftime("%b", strtotime($row[createdDate]));
												?>
												
												<div class="banner">
													<a href="/blog/<?php echo $row["blogLink"]?>" class="no-change">
														
														<figure class="bottom-arrow">
															<img src="/images/article/<?php echo $row["blogImage"]?>" class="resp-img" alt="<?php echo $row["blogTitle"]?>">
														</figure>
													</a>	
													<div class="articleDesc clearfix">
														<span class="date"><?php echo $row[createdDate]?> | I <a href="/blog/kategori/<?php echo $row["catURL"]?>" class="red"><?php echo $row["catName"]?></a> | <i class="fa fa-tags"></i></span>
														<a href="/blog/<?php echo $row["blogLink"]?>" class="no-change">
															<h4><?php echo $row["blogTitle"]?></h4>
															
															<div class="article-desc-text">
																<?php 
																	
																	TrimShort($row["shortBlog"]);
																?> .....
															</div>
															
															
														</div>
													</a>
												</div>
												<?php
												}
												} else {
											?>
											
											<p class="subHeadline">Her er endnu ikke noget blogindlæg, prøv en anden kategori eller vend tilbage en anden gang</p>
											
											<?php
											}
										?>
									</div>
									<div class="col-md-4">
										<div class="search">
											<form action="" method="post">
												<input type="text" placeholder="SØG" name="searchStr">
												<button type="submit" class="btn search"><i class="fa fa-search"></i></button>
											</form>
										</div>
										
										<ul class="categories">
											<li>
												<a href="/blog">Alle artikler</a>
											</li>
											<li>
												<a href="/blog/kategori/chit-chat">Chit chat</a>
											</li>
											<li>
												<a href="/blog/kategori/nyheder">Nyheder</a>
											</li>
											<li>
												<a href="/blog/kategori/tech">Tech</a>
											</li>
											<li>
												<a href="/blog/kategori/tutorials">Tutorials</a>
											</li>
											<li>
												<a href="/blog/kategori/seneste-arbejde">Seneste arbejde</a>
											</li>
										</ul>
										
										<div class="weekGif">
											<?php
												$weekNr = date(W,time());
											?>
											<img src="/images/gifs/<?php echo $weekNr?>.gif">
											<h4>Ugens GIF</h4>
										</div>
									</div>
								</div>
							</section>
							<section class="last">
								<svg version="1.1" class="svg-diagonal  top-left" height="79" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 3315 79" enable-background="new 0 0 3315 79" xml:space="preserve" preserveAspectRatio="none">
									<g>
										<g><polygon fill-rule="evenodd" clip-rule="evenodd" fill="#f7f7f5" points="-1,-1 -1,80 3316,-1"></polygon></g>
									</g>
								</svg>
								<div class="inner">
									<h2>
										<strong>HIGH FIVE</strong>, du nåede bunden!
									</h2>
									<p class="subHeadline">
										Kunne du lide vores arbejde?
									</p>
									<a href="/planner" class="button button-blue">Så start et projekt idag!</a>
								</div>
							</section>
						</main>
					<?php include '/includes/bottom.php' ?>																																																																																																																												