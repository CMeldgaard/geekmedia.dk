<!DOCTYPE html>
<html lang="da">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title>Geek Media | Velkommen</title>
		
		<meta name="description" content="">
		<meta name="keywords" content="">
		<?php include '/includes/headerstyles.php' ?>
	</head>
	
	<body>
		<?php include '/includes/nav.php' ?>
		
		<div id="loadLayer">
			<div class="mask"><div class="logo"><div class="mask"></div><div class="bg"></div></div>	</div>
		</div>
		<div id="contentSpace">
			<div id="contentSpaceContainer">
				<div id="container_wrapper">
					<div id="container" class="container">
						<main id="content" class="content">
							<div class="header" style="height: 572px;">
								<div class="mainVideoContainer" style="top: 0px; background-image: url(/images/home_video_fallback_800.jpg);">
									
									<div class="logoContainer">
										<img src="images/logo.png" style="width: 295px; margin-top: 50px;">
									</div>
									<div class="overlay-video"></div>
									<div class="videoContainer">
										<video class="video" poster="/images/home_video_header.jpg" preload="true" loop="true" autoplay="true" muted="true"><!--autobuffer="true"-->
											<source src="includes/geekmedia.mp4" type="video/mp4">
										</video>
										
									</div>
									
								</div>
								
								<div class="colorFillContainer">
									<svg version="1.1" id="colorFill" class="svg-diagonal  bottom-right" height="290" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 150 290" enable-background="new 0 0 150 290" xml:space="preserve" preserveAspectRatio="none" style="height: 75px; transform: translate3d(0px, 0px, 0px);">
										<g id="polygon_bottom-right_290_0aded557ef314d871fdf80e307286359648bcbf359a5">
											<g><polygon fill-rule="evenodd" clip-rule="evenodd" fill="#3caea6" points="151,-1 -1,291 151,291"></polygon></g>
										</g>
									</svg>
									<div class="colorFillBox"></div>
								</div>
								<svg version="1.1" id="colorMask" class="svg-diagonal  bottom-right" height="50" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 3750 50" enable-background="new 0 0 3750 50" xml:space="preserve" preserveAspectRatio="none">
									<g id="polygon_bottom-right_50_939a57b8120e9bf6080e1d3457526bcf90248a5e5eab">
										<g><polygon fill-rule="evenodd" clip-rule="evenodd" fill="#FFF" points="3751,-1 -1,51 3751,51"></polygon></g>
									</g>
								</svg>
							</div>
							<section>
								<div class="inner welcome">
									<h1>Velkommen til Geek Media</h1>
									<p class="subHeadline">
										Mit navn er Carsten Meldgaard og jeg er ejeren af Geek Media i Esbjerg. Jeg har en baggrund som <span class="cursive">webudvikler</span> og multimediedesigner.
										Jeg er specialist i at løse problemstillinger gennem intelligent design, engagerende oplevelser og meningsfulde forbindelser. Mit mål er at skabe høj kvalitets <span class="cursive">digitale kommunikationer</span>, der har en indvirkning på modtageren.
										
									</p>
									<a href="/bag-om" class="button button-blue">Find ud af mere!</a>
								</div>
								<svg version="1.1" id="triangleShape_bottom-left_79_44736f31032aaa1d0cbae942f4c18e23407c6fa0e3eb" class="svg-diagonal  bottom-left" height="79" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 3315 79" enable-background="new 0 0 3315 79" xml:space="preserve" preserveAspectRatio="none">
									<g id="polygon_bottom-left_79_135ea38474843ed33311db400d767671b38f181ec1a0">
										<g><polygon fill-rule="evenodd" clip-rule="evenodd" fill="#F7F7F5" points="-1,-1 -1,80 3316,80"></polygon></g>
									</g>
								</svg>
								
							</section>
							<section class="dark">
								<div class="inner whatIdo">
									<h2>Hvad laver jeg?</h2>
									
									<div class="container">
										<div class="col-sm-4">
											<img src="images/1.png" />
											<h3>WEB</h3>
											<p>We can handle the most challenging technical problems, and we've also got architecture, product and marketing chops.</p>
										</div>
										<div class="col-sm-4">
											<img src="images/2.png" />
											<h3>Design</h3>
											<p>We can handle the most challenging technical problems, and we've also got architecture, product and marketing chops.</p>
										</div>
										<div class="col-sm-4">
											<img src="images/3.png" />
											<h3>out source</h3>
											<p>Skal I igang med et større projekt, eller mangler I arbejdskraft? Så kan Geek Media hjælpe jer! Kontakt mig i dag for at høre hvilke
											muligheder I har.</p>
										</div>
									</div>
									<a href="/services" class="button button-blue">Se alle mine services!</a>
								</div>
							</section>
							<section>
								<svg version="1.1" id="triangleShape_top-left_79_154a999425fcbd9c85a20b09528f01ef14bd26d56777" class="svg-diagonal  top-left" height="79" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 3315 79" enable-background="new 0 0 3315 79" xml:space="preserve" preserveAspectRatio="none">
									<g id="polygon_top-left_79_f6ce8305d1bb79966bccfc3c8611b60f9be0354acedc">
										<g><polygon fill-rule="evenodd" clip-rule="evenodd" fill="#f7f7f5" points="-1,-1 -1,80 3316,-1"></polygon></g>
									</g>
								</svg>
								<div class="inner clients">
									<h2>Mine klienter</h2>
									<ul>
										<li><img src="images/clients/diersklinik.png" class="resp-img" /></li>
										<li><img src="images/clients/midtjysk.png" class="resp-img" /></li>
										<li><img src="images/clients/destinationer.png" class="resp-img" /></li>
										<li><img src="images/clients/visitskanderborg.png" class="resp-img" /></li>
										<li><img src="images/clients/cleverdeal.png" class="resp-img" /></li>
										<li><img src="images/clients/citizone.png" class="resp-img" /></li>
									</ul>
								</div>
								<svg version="1.1" id="triangleShape_bottom-left_79_97dc9648a1a7fcc5b583175efc214f7c432c51ede38d" class="svg-diagonal  bottom-left" height="79" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 3315 79" enable-background="new 0 0 3315 79" xml:space="preserve" preserveAspectRatio="none">
									<g id="polygon_bottom-left_79_6073800e548fcf2d01b16509f381c4428dd35716a6b5">
										<g><polygon fill-rule="evenodd" clip-rule="evenodd" fill="#F7F7F5" points="-1,-1 -1,80 3316,80"></polygon></g>
									</g>
								</svg>
							</section>
							<section class="dark last">
								<div class="inner">
									<h2>
										<strong>START</strong>, i dag!
										</h2>
									<p class="subHeadline">
										Har du en opgave der skal udføres, eller brug for konsulent tid?
									</p> 
										<a href="/planner" class="button button-blue">Få et uforpligtende tilbud i dag!</a>
								</div>
							</section>
						</main>
					<?php include '/includes/bottom.php' ?>																			