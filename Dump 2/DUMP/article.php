<?php include '/includes/datacon.php'?>

<?php
	$blogLink = str_replace(".php", "", $_GET["perma"]);
	
	if ($blogLink == "" ){
		header('Location: /blog');
		
		}else{
		$stmt = mysqli_prepare($conn, "SELECT * FROM blog INNER JOIN blogCategory ON blog.blogCategoryID = blogCategory.blogCategoryID INNER JOIN blogWriters on blog.createdBy = blogWriters.createdBy where blogLink=? order by createdDate desc");
		
		/* bind parameters for markers */
		mysqli_stmt_bind_param($stmt, "s", $blogLink);
		
		/* execute query */
		mysqli_stmt_execute($stmt);
		
		/* instead of bind_result: */
		$result = $stmt->get_result();
		
		/* now you can fetch the results into an array - NICE */
		while ($row = $result->fetch_assoc()) {
			
			setlocale(LC_ALL, "danish");
			$creaDate = strftime("%d", strtotime($row[createdDate]));
			$creaMonth = strftime("%b", strtotime($row[createdDate]));
			$metaTitle = $row[blogTitle];
			
		?>
		
		<!DOCTYPE html>
		<html lang="da">
			<head>
				<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
				<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
				<title>Geek Media | <?php echo $row["blogTitle"]?></title>
				
				<meta name="description" content="<?php TrimShort($row["shortBlog"]) ?>">
				<meta name="keywords" content="">
				<?php include '/includes/headerstyles.php' ?>
			</head>	
			<body>
				<?php include '/includes/nav.php' ?>
				
				<div id="loadLayer">
					<div class="mask"><div class="logo"><div class="mask"></div><div class="bg"></div></div>	</div>
				</div>
				<div id="contentSpace">
					<div id="contentSpaceContainer">
						<div id="container_wrapper">
							<div id="container" class="container">
								<main id="content" class="content">
									<?php include('/includes/header.php')?>
									<section>
										<div class="inner welcome">
											<h1>Idéer, <span class="red">tanker</span> & indsigt.</h1>
											<p class="subHeadline">
												Se hvad der rumsterer rundt i mit hoved af tanker og idéer, og alverdens andre ting! Lyst til at skrive et indlæg, så <a href="/kontakt" class="cursive"> kontakt</a> mig i dag
											</p>
										</div>
										<svg version="1.1" class="svg-diagonal  bottom-left" height="79" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 3315 79" enable-background="new 0 0 3315 79" xml:space="preserve" preserveAspectRatio="none">
											<g>
												<g><polygon fill-rule="evenodd" clip-rule="evenodd" fill="#F7F7F5" points="-1,-1 -1,80 3316,80"></polygon></g>
											</g>
										</svg>
									</section>
									<section class="dark">
										<div class="inner blog">
											<div class="col-md-8">
												
												<div class="banner">
														<figure class="bottom-arrow">
															<img src="/images/article/<?php echo $row["blogImage"]?>" class="resp-img" alt="<?php echo $row["blogTitle"]?>">
														</figure>
													<div class="articleDesc clearfix">
														<span class="date"><?php echo $row[createdDate]?> | I <a href="/blog/kategori/<?php echo $row["catURL"]?>" class="red"><?php echo $row["catName"]?></a> | <i class="fa fa-tags"></i></span>
															<h4><?php echo $row["blogTitle"]?></h4>
															
															<div class="article-desc-text">
																<?php echo $row["blogContent"]?>
															</div>
														</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="search">
													<input type="text" placeholder="SØG">
													<button type="submit" class="btn search"><i class="fa fa-search"></i></button>
												</div>
												
												<ul class="categories">
												<li>
														<a href="/blog">Alle artikler</a>
													</li>
													<li>
														<a href="/blog/kategori/chit-chat">Chit chat</a>
													</li>
													<li>
														<a href="/blog/kategori/nyheder">Nyheder</a>
													</li>
													<li>
														<a href="/blog/kategori/tech">Tech</a>
													</li>
													<li>
														<a href="/blog/kategori/tutorials">Tutorials</a>
													</li>
													<li>
														<a href="/blog/kategori/seneste-arbejde">Seneste arbejde</a>
													</li>
												</ul>
												
												<div class="weekGif">
													<?php
														$weekNr = date(W,time());
													?>
													<img src="/images/gifs/<?php echo $weekNr?>.gif">
													<h4>Ugens GIF</h4>
												</div>
											</div>
										</div>
									</section>
									<section class="last">
										<svg version="1.1" class="svg-diagonal  top-left" height="79" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 3315 79" enable-background="new 0 0 3315 79" xml:space="preserve" preserveAspectRatio="none">
											<g>
												<g><polygon fill-rule="evenodd" clip-rule="evenodd" fill="#f7f7f5" points="-1,-1 -1,80 3316,-1"></polygon></g>
											</g>
										</svg>
										<div class="inner">
											<h2>
												<strong>HIGH FIVE</strong>, du nåede bunden!
											</h2>
											<p class="subHeadline">
												Kunne du lide vores arbejde?
											</p>
											<a href="/planner" class="button button-blue">Så start et projekt idag!</a>
										</div>
									</section>
								</main>
								<?php 
								}
							}							
						include '/includes/bottom.php' ?>																																																																																																																								