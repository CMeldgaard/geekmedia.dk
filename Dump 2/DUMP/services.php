<!DOCTYPE html>
<html lang="da">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title>Geek Media | Velkommen</title>
		
		<meta name="description" content="">
		<meta name="keywords" content="">
		<?php include '/includes/headerstyles.php' ?>
	</head>
	
	<body>
		<?php include '/includes/nav.php' ?>
		
		<div id="loadLayer">
			<div class="mask"><div class="logo"><div class="mask"></div><div class="bg"></div></div>	</div>
		</div>
		<div id="contentSpace">
			<div id="contentSpaceContainer">
				<div id="container_wrapper">
					<div id="container" class="container">
						<main id="content" class="content">
						<?php include('/includes/header.php')?>
							<section>
								<div class="inner welcome">
									<h1>
										Bag <span class="red">om firmaet</span> Geek Media.
									</h1>
									<p class="subHeadline">
										Geek Media er et design og brugerfokuseret digitalt bureau. Geek Media er specialister i at løse problemstillinger gennem intelligent design, engagerende oplevelser og meningsfulde forbindelser.
									</p>
								</div>
								<svg version="1.1" id="triangleShape_bottom-left_79_44736f31032aaa1d0cbae942f4c18e23407c6fa0e3eb" class="svg-diagonal  bottom-left" height="79" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 3315 79" enable-background="new 0 0 3315 79" xml:space="preserve" preserveAspectRatio="none">
									<g id="polygon_bottom-left_79_135ea38474843ed33311db400d767671b38f181ec1a0">
										<g><polygon fill-rule="evenodd" clip-rule="evenodd" fill="#F7F7F5" points="-1,-1 -1,80 3316,80"></polygon></g>
									</g>
								</svg>
								
							</section>
							<section class="dark">
								<div class="inner">
									<h2>Ansigtet bag Geek Media</h2>
									
									<div class="container">
										<div class="team Odd" id="carsten">
											<h3>Carsten Meldgaard</h3>
											<p>
												Carsten elsker design og udvikling mere end pizza og pasta. Han er passioneret omkring spændende brugeroplevelser og at udforske nye måder at levere bedre
												resultater på. Du kan fange ham igang med at læse om udvikling eller ser genudsendelser af The Big Bang Theory.
											</p>
										</div>
									</div>
								</div>
							</section>
							<section>
								<svg version="1.1" id="triangleShape_top-left_79_154a999425fcbd9c85a20b09528f01ef14bd26d56777" class="svg-diagonal  top-left" height="79" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 3315 79" enable-background="new 0 0 3315 79" xml:space="preserve" preserveAspectRatio="none">
									<g id="polygon_top-left_79_f6ce8305d1bb79966bccfc3c8611b60f9be0354acedc">
										<g><polygon fill-rule="evenodd" clip-rule="evenodd" fill="#f7f7f5" points="-1,-1 -1,80 3316,-1"></polygon></g>
									</g>
								</svg>
								<div class="inner">
									<h2>
										Nøglen til <strong>SUCCES</strong>
									</h2>
									<div class="container">
									<p class="subHeadline">
										Nøglen til at skabe store brands, produkter og oplevelser kommer bl.a. gennem en
										forståelse af <span class="cursive">klienternes</span> og <span class="cursive">brugernes</span> behov. <br><br>
										De afgørende faser af konceptudvikling, research og planlægning er <span class="cursive">kernen</span> i ethvert vellykket projekt.
										<br><br>
										<span class="cursive">Geek Media</span> tager stor omhu i at sikre alle de nødvendige
										skridt er taget for at producere perfekte resultater hver gang.
									</p>
									</div>
								</div>
								<svg version="1.1" id="triangleShape_bottom-left_79_97dc9648a1a7fcc5b583175efc214f7c432c51ede38d" class="svg-diagonal  bottom-left" height="79" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 3315 79" enable-background="new 0 0 3315 79" xml:space="preserve" preserveAspectRatio="none">
									<g id="polygon_bottom-left_79_6073800e548fcf2d01b16509f381c4428dd35716a6b5">
										<g><polygon fill-rule="evenodd" clip-rule="evenodd" fill="#F7F7F5" points="-1,-1 -1,80 3316,80"></polygon></g>
									</g>
								</svg>
							</section>
							<section class="dark last">
								<div class="inner">
									<p class="subHeadline">
										Elsker kommunikation og elsker nye oplevelser. Lad os lære hinanden bedre at kende! 
									</p>
									<a href="/planner" class="button button-blue">Kontakt mig i dag!</a>
								</div>
							</section>
						</main>
					<?php include '/includes/bottom.php' ?>																																												