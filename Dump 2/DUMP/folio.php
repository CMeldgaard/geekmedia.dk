<!DOCTYPE html>
<html lang="da">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title>Geek Media | Folio</title>
		
		<meta name="description" content="">
		<meta name="keywords" content="">
		<?php include '/includes/headerstyles.php' ?>
	</head>
	
	<body>
		<?php include '/includes/nav.php' ?>
		
		<div id="loadLayer">
			<div class="mask"><div class="logo"><div class="mask"></div><div class="bg"></div></div>	</div>
		</div>
		<div id="contentSpace">
			<div id="contentSpaceContainer">
				<div id="container_wrapper">
					<div id="container" class="container">
						<main id="content" class="content">
							<?php include('/includes/header.php')?>
							<section>
								<div class="inner welcome">
									<h1>
										Mine <span class="red">bedste og nyeste</span> opgaver.
									</h1>
									<p class="subHeadline">
										Et lille udpluk af mine nyeste og bedste cases, fra mig til dig.
										Ønsker du flere referencer så <a href="/kontakt" class="cursive">kontakt mig</a>, og jeg vil vende tilbage med det ønskede.
									</p>
								</div>
								<svg version="1.1" class="svg-diagonal  bottom-left" height="79" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 3315 79" enable-background="new 0 0 3315 79" xml:space="preserve" preserveAspectRatio="none">
									<g>
										<g><polygon fill-rule="evenodd" clip-rule="evenodd" fill="#F7F7F5" points="-1,-1 -1,80 3316,80"></polygon></g>
									</g>
								</svg>
							</section>
							<section class="dark">
								<div class="inner cases">
									<div class="col-md-4 noPad case">
										<a href="">
											<div class="caseTop"></div>
											<figure class="bottomArrow">
												<img src="/images/projects/naturperlen.jpg" class="resp-img" alt="BDF Energy in the room">
											</figure>
											<div class="caseDesc" style="height: 310px;">
												<div class="case-desc-inner">
													<h5>Naturperlen</h5>
													<h4>Skoleprojekt</h4>
													<p>
														Udarbejdelse af logo for <strong>Naturperlen</strong>, i forbindelse med skoleopgave
													</p>
												</div>
											</div>
										</a>
									</div>
									<div class="col-md-4 noPad case">
										<a href="">
											<div class="caseTop"></div>
											<figure class="bottomArrow">
												<img src="/images/projects/citizone.jpg" class="resp-img" alt="BDF Energy in the room">
											</figure>
											<div class="caseDesc" style="height: 310px;">
												<div class="case-desc-inner">
													<h5>CitiZone</h5>
													<h4>Afgangsprojekt</h4>
													<p>The large-scale field installation showcases the four elements of water, fire, earth and wind and makes it controllable in an interactive way.</p>
												</div>
											</div>
										</a>
									</div>
									<div class="col-md-4 noPad case">
										<a href="">
											<div class="caseTop"></div>
											<figure class="bottomArrow">
												<img src="/images/projects/rethink.jpg" class="resp-img" alt="BDF Energy in the room">
											</figure>
											<div class="caseDesc" style="height: 310px;">
												<div class="case-desc-inner">
													<h5>Midtjysk Turisme</h5>
													<h4>RETHINK</h4>
													<p>The large-scale field installation showcases the four elements of water, fire, earth and wind and makes it controllable in an interactive way.</p>
												</div>
											</div>
										</a>
									</div>
									<div class="col-md-4 noPad case">
										<a href="">
											<div class="caseTop"></div>
											<figure class="bottomArrow">
												<img src="/images/projects/dea.jpg" class="resp-img" alt="BDF Energy in the room">
											</figure>
											<div class="caseDesc" style="height: 310px;">
												<div class="case-desc-inner">
													<h5>Danish Entrepreneuship Award</h5>
													<h4>Plakatkonkurrence</h4>
													<p>
														Konkurrenceforslag til Danish Entrepreneuship Award 2014
													</p>
												</div>
											</div>
										</a>
									</div>
									<div class="col-md-4 noPad case">
										<a href="">
											<div class="caseTop"></div>
											<figure class="bottomArrow">
												<img src="/images/projects/easv.jpg" class="resp-img" alt="BDF Energy in the room">
											</figure>
											<div class="caseDesc" style="height: 310px;">
												<div class="case-desc-inner">
													<h5>Erhvers Akademi Sydvest</h5>
													<h4>Skoleopgave</h4>
													<p>The large-scale field installation showcases the four elements of water, fire, earth and wind and makes it controllable in an interactive way.</p>
												</div>
											</div>
										</a>
									</div>
									<div class="col-md-4 noPad case">
										<a href="">
											<div class="caseTop"></div>
											<figure class="bottomArrow">
												<img src="/images/projects/destinationer.jpg" class="resp-img" alt="BDF Energy in the room">
											</figure>
											<div class="caseDesc" style="height: 310px;">
												<div class="case-desc-inner">
													<h5>Danske Destinationer</h5>
													<h4>Samlet dansk turisme</h4>
													<p>The large-scale field installation showcases the four elements of water, fire, earth and wind and makes it controllable in an interactive way.</p>
												</div>
											</div>
										</a>
									</div>
								</div>
							</section>
							<section class="last">
								<svg version="1.1" id="triangleShape_top-left_79_154a999425fcbd9c85a20b09528f01ef14bd26d56777" class="svg-diagonal  top-left" height="79" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 3315 79" enable-background="new 0 0 3315 79" xml:space="preserve" preserveAspectRatio="none">
									<g id="polygon_top-left_79_f6ce8305d1bb79966bccfc3c8611b60f9be0354acedc">
										<g><polygon fill-rule="evenodd" clip-rule="evenodd" fill="#f7f7f5" points="-1,-1 -1,80 3316,-1"></polygon></g>
									</g>
								</svg>
								<div class="inner">
									<h2>
										<strong>HIGH FIVE</strong>, du nåede bunden!
									</h2>
									<p class="subHeadline">
										Kunne du lide vores arbejde?
									</p>
									<a href="/planner" class="button button-blue">Så start et projekt idag!</a>
								</div>
							</section>
						</main>
					<?php include '/includes/bottom.php' ?>																																																																																									