<!DOCTYPE html>
<html lang="da">
	
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Gør det nemt at vedligeholde en dynamisk hjemmeside uanset dit it-niveau og færdigheder.">
		<title>Geek Media | Website m. CMS</title>
		<?php include('includes/styles.php');?>
	</head>
	
	<body>
		
		<!-- Top Bar-->
		<?php include('includes/nav.php');?>
		<!-- End of Top Bar-->
		<!-- About Us -->
		<section class="website">
			<div class="breadcrumbs">
				<div class="row">
					<div class="col-sm-6">
						<h1>Website m. CMS</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb">
							<li>Du er her: </li>
							<li><a href="/">Forside</a>
							</li>
							<li><a href="#">Web</a>
							</li>
							<li class="active">Website m. CMS</li>
						</ol>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h2>Website m. CMS</h2>
					<hr class="small">
					<p>Gør det nemt at vedligeholde en dynamisk 
hjemmeside uanset dit it-niveau og færdigheder</p>
				</div>
			</div>
		</section>
		<section>
			<div class="spacing-70"></div>
			<div class="row ">
				<div class="col-sm-8 center-block">
					<h2>Gør din markedsføring online</h2>
					<p>Du har få sekunder til at præsentere din virksomhed på internettet 
og derfor skal din hjemmeside være moderne, gennemtænkt og 
brugervenlig – ellers er brugeren væk igen sekundet efter.</p>
					<p>
						Din hjemmeside er din personlighed på internettet og derfor skal 
den være professionelt bearbejdet og samtidig skal navigationen 
være så intuitiv, at dine kunder hurtigt kan finde den ønskede 
information.</p>
					<p>
						Geek Media hjælper nye og eksisterende kunder med online 
markedsføring, både i form af hjemmeside, design, marketing og 
søgemaskineoptimering.
					</p>
					<p>
						Jeg lægger stor vægt på at levere gennemarbejdede websites, 
som bringer dit brand til live og kampagner i mål.
					</p>
				</div>
				<div class="spacing-70"></div>
				<div class="col-sm-8 center-block">
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.2s" id="web_1">
						<h3>Website Strategi</h3>
						<p>Analyse af mål til at give grundige anbefalinger omkring det tekniske, designmæssige og UX overvejelser.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.3s" id="web_2">
						<h3>UI DESIGN</h3>
						<p>Udvikling af høj kvalitetes koncepter der leverer mål påtværs af alle platforme og får websitet til at se godt ud</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.4s" id="web_3">
						<h3>UX DESIGN</h3>
						<p>Fokus på de vigtigste målgrupper for at levere målrettede UX strategier, brugervenlighed, engagement og glæde.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.2s" id="web_4">
						<h3>RESPONSIVE DESIGN</h3>
						<p>Problemfri fleksible rammer for at give optimale oplevelser på tværs af alle enheder, fra smartphones til desktops.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.3s" id="web_5">
						<h3>CM SYSTEM</h3>
						<p>Robust og brugervenlig systemer til at styre websitet indhold og effektiv strukturering, som er skalerbar til netop dit projekts behov.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.4s" id="web_6">
						<h3>HTML5</h3>
						<p>Udforskning af interaktion og moderne teknologier for at få mest ud af digitale platforme og tilskynde engagement.</p>
					</div>
				</div>
				<div class="col-md-12 jumpDown">
					<a class="btn btn-jump" href="#cms">Læs mere</a>
				</div>
			</div>
			
			<div class="spacing-70"></div>
			
			<!--  Call to Action -->
    <section class="calltoaction">

        <div class="row wow zoomIn" data-wow-delay="0.2s">
            <div class="col-sm-10 com-md-8 center-block">

                <div class="row no-gutter cta-content">
                    <div class="col-sm-4">
                        <div class="offer wow fadeInUp" data-wow-delay="0.5s">
                            <span>LAD OS</span>
                            <a href="/planner"><h2>STARTE</h2></a>
                            <span>I DAG</span>
                        </div>
                    </div>
                    <div class="col-sm-8">

                        <div class="offerdescription wow fadeInUp" data-wow-delay="0.7s">
                            <h2>START ALLEREDE I DAG</h2>
                            <p>ved at udfylde min online formular og få en uforpligtende samtale</p>
                        </div>
                    </div>
                </div>
            </div>	
        </div>

    </section>
    <!-- End of Call to Action -->
			<div class="spacing-70"></div>
			<div class="row ">
				<div class="col-sm-8 center-block" id="cms">
					<h2>Styr indholdet direkte i browseren</h2>
					<p>Den gode hjemmeside er hele tiden under forandring. Brugerne 
bliver lokket til med friskt og dynamisk indhold, søgeordene 
i teksterne bliver løbende opdateret og funktioner som blog, 
debatforum, billedgalleri, kalender og meget mere er dybt 
integreret med hjemmesidens struktur og indhold</p>
					<p>
						Desværre er der rigtig mange hjemmesider, som ikke følger 
den ovenstående forskrift, da det ofte er besværligt og meget 
ressourcekrævende at skulle holde en hjemmeside opdateret - 
specielt for uerfarne it-brugere. Det er den problematik, som jeg 
forsøger at komme til livs med ved at opsætte dit website i CMS.</p>
					<p>
						Uanset om du er uddannet grafiker, webudvikler eller på anden 
vis sidder med ansvaret for en virksomheds hjemmeside, vil CMS 
give dig værktøjerne til at etablere og vedligeholde et professionelt 
og dynamisk website
					</p>
					<p>
						CMS betyder bedre hjemmesider, fordi det bliver hurtigt og nemt 
for alle medarbejdere, at udvikle, justere og opdatere den eller 
teste nye idéer, for derefter at måle effekten i et statistikprogram.
					</p>
					<h2>Open Source Content Management</h2>
					<p>
					Jeg bruger veletableret content management systemer til at tillade fuld kontrol over ethvert website, sikre effektivitet, fleksibilitet og brugervenlighed. Mine foretrukne systemer omfatter Wordpress og Concrete5 for deres brugervenlighed og fleksibilitet.
					</p>
					<img src="/images/logo_wordpress.png" width="300" height="100" alt="Wordpress Content Management">
					<img src="/images/logo_concrete5.png" width="300" height="100" alt="Wordpress Content Management">
					<p>
					Frem for alt tror jeg på kvalitet, samarbejde, tillid og det ultimative mål om at opbygge vellykkede projekter, der giver den bedste oplevelse for brugeren.
					</p>
					<p class="cta">Lad os gøre det. <a href="/planner" title="Start et projekt">Start et website projekt i dag!</a></p>
				</div>
			</div>
		</section>

<!-- End of About Us -->

<?php include('includes/bottom.php');?>

<script type="text/javascript">
	// ______________  TOOLTIPS
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
	});
	
	// ______________ TABS
	$('#shared-hosting-tabs').responsiveTabs({
		startCollapsed: 'accordion'
	});
</script>
</body>

</html>
