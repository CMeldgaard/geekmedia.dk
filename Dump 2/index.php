<!DOCTYPE html>
<html lang="da">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Geek Media er et design og brugerfokuseret digitalt bureau. Geek Media er specialister i at løse problemstillinger gennem intelligent design, engagerende oplevelser og meningsfulde forbindelser.">
    <title>Geek Media | Velkommen</title>
    <?php include('includes/styles.php');?>
</head>

<body>

    <!-- Top Bar-->
<?php include('includes/nav.php');?>
    <!-- End of Top Bar-->

    <!-- Slider -->

    <div class="slidercontainer">
        <div id="mainslider" class="owl-carousel">

            <div class="item">
                <div class="slidecaption">
                    <h2>GEEK HOST</h2>
                    <h4>Sikker, hurtig og pålidelig</h4>
                    <p><a class="btn btn-slide" href="/geek-host">Se alle specifikationerne</a>
                    </p>
                </div>
                <img src="/images/1.jpg" alt="" />
            </div>

            <div class="item">
                <div class="slidecaption">
                    <h2>MODERNE WEBSITES DER SÆLGER</h2>
                    <h4>Følg udviklingen og gør din markedsføring online</h4>
                    <p><a class="btn btn-slide" href="/website">Lær mere</a>
                    </p>
                </div>
                <img src="/images/2.jpg" alt="" />
            </div>

            <div class="item">
                <div class="slidecaption">
                    <h2>DESIGN & UX</h2>
                    <h4>Form og funktion gennem spændende grænseflader og menneskelige oplevelser</h4>
                    <p><a class="btn btn-slide" href="/design-ux">Mere info</a>
                    </p>
                </div>
                <img src="/images/3.jpg" alt="" />
            </div>

            <div class="item">
                <div class="slidecaption">
                    <h2>GEEK PARTNER</h2>
                    <h4>Din professionelle samarbejdspartner</h4>
                    <p><a class="btn btn-slide" href="/geek-partner">Se alle fordelene</a>
                    </p>
                </div>
                <img src="/images/4.jpg" alt="" />
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div id="mainslider-nav" class="owl-carousel">
                    <div class="item"><i class="fa fa-server"></i>HOSTING</div>
                    <div class="item"><i class="fa fa-code"></i>UDVIKLING</div>
                    <div class="item"><i class="fa fa-pencil-square-o"></i>DESIGN</div>
                    <div class="item"><i class="fa fa-dashboard"></i>GEEK PARTNER</div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Slider -->

    <!--  Features -->
    <section class="features">
        <div class="row spacing-70">
            <div class="col-sm-12">
                <h1>Velkommen til Geek Media</h1>
                <span>Alle dine behov fra et moderne bureau samlet under et tag</span>
            </div>
        </div>

        <div class="row spacing-70">
            <div class="col-sm-4">
                <div class="feature wow zoomIn" data-wow-delay="0.2s">
                    <img src="/images/ssd.png" alt="" />
                    <h4>Hosting</h4>
                    <p>Webhosting gjort enkelt. Få et webhotel med høj hastighed og ingen nedbrud.</p>
					<a class="btn btn-lg btn-action" href="/geek-host">Læs mere</a>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="feature wow zoomIn" data-wow-delay="0.4s">
                    <img src="/images/rocket.png" alt="" />
                    <h4>Design</h4>
                    <p>Grafisk design er ikke blot ét flot design, men en 
sammenhængende fortælling.</p>
					<a class="btn btn-lg btn-action" href="/design-ux">Læs mere</a>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="feature wow zoomIn" data-wow-delay="0.6s">
                    <img src="/images/support.png" alt="" />
                    <h4>Support</h4>
                    <p>Få en supportaftale der passer til netop dine produkter og behov.</p>
					<a class="btn btn-lg btn-action" href="/geek-partner">Læs mere</a>
                </div>
            </div>

        </div>
    </section>
    <!-- End of Features -->


    <!--  Call to Action -->
    <section class="calltoaction">

        <div class="row wow zoomIn" data-wow-delay="0.2s">
            <div class="col-sm-10 com-md-8 center-block">

                <div class="row no-gutter cta-content">
                    <div class="col-sm-4">
                        <div class="offer wow fadeInUp" data-wow-delay="0.5s">
                            <span>LAD OS</span>
                            <a href="/planner"><h2>STARTE</h2></a>
                            <span>I DAG</span>
                        </div>
                    </div>
                    <div class="col-sm-8">

                        <div class="offerdescription wow fadeInUp" data-wow-delay="0.7s">
                            <h2>START ALLEREDE I DAG</h2>
                            <p>ved at udfylde min online formular og få en uforpligtende samtale</p>
                        </div>
                    </div>
                </div>
            </div>	
        </div>

    </section>
    <!-- End of Call to Action -->

    <div class="testimonials gray">
        <div class="row">
            <div class="col-sm-12">
                <h3>Hvad siger mine kunder</h3>
                <div id="testimonials-carousel" class="owl-carousel">

                    <div class="item">
                        <div class="testimonial-content">
                            <div class="testimonialimg"><img src="/images/testimonial1.jpg" alt="" />
                            </div>
                            <p>I forbindelse med en seminarrække i efterår/vinter 2013/14 havde projektet RETHINK Kulturturisme brug for en skriftlig opsamling af den tilegnede viden. Vi udarbejdede en skitse over indhold og form og kontaktede Carsten efter anbefalinger fra kollegaer i Midtjysk Turisme. Alpha-Omega for produktet var en hurtig leveringstid og fleksibilitet omkring korrektur og udviklingen af produktet. Carsten var effektiv og behagelig i vores mailkorrespondance og forstod hurtigt hvilken stil vi ønskede. Nem og gnidningsløs korrekturrunder, med effektiv brug af online hjælpemidler. Carsten er dygtig til at aflæse kundens designønsker og kommer gerne med egne forslag der kan matche. Vi vil anbefale Carsten som en effektiv samarbejdspartner indenfor grafiske opgaver. </p>
                            <div class="whoclient">
                                <h5>Dorte Dejbjerg Arens, Projektkoordinator, <a href="http://www.kulturturisme.dk/" target="_blank">RETHINK Kulturturisme</a></h5>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <div class="testimonialimg"><img src="/images/testimonial2.jpg" alt="" />
                            </div>
                            <p>Carsten har i gennem en periode hjulpet nm design med diverse freelance opgaver fra a-z. nm design har i perioden bl.a. brugt Carsten til udarbejdelse af designs, lavet programmeringsopgaver af både større og mindre karakter etc. og alt sammen er udarbejdet meget professionelt og inden for aftalt tidsramme. Carsten er en person som meget lyttende og meget engageret, og han laver det han skal. Carsten har en idérig tankegang og har en forståelse for hvad det er kunden ønsker og hvad kunden har behov for, derudover han meget kreativ, med en anderledes tankegang. Vi har erfaret, at Carsten både kan arbejde i php-kodning samt asp-kodning. Vi kan på alle parametre indenfor webdesign varmt anbefale Carsten.
</p>
                            <div class="whoclient">
                                <h5>Martin Arendt, Partner, <a  href="http://www.nm-design.dk/" target="_blank">NM Design</a></h5>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
	<?php include('includes/bottom.php');?>
	 <script>
    /* Home Page Slider
   ========================================================================== */
    $(document).ready(function() {


    var sync1 = $("#mainslider");
    var sync2 = $("#mainslider-nav");

    sync1.owlCarousel({
    singleItem : true,
    slideSpeed : 1000,
    paginationSpeed: 800,
    navigation: false,
    pagination:false,
    autoPlay:7500,
    afterAction : syncPosition,
    responsiveRefreshRate : 200,
    });

    sync2.owlCarousel({
    items : 4,
    itemsDesktop : [1199,4],
    itemsDesktopSmall : [979,4],
    itemsTablet : [768,4],
    itemsMobile : [479,2],
    pagination:false,
    responsiveRefreshRate : 100,
    afterInit : function(el){
    el.find(".owl-item").eq(0).addClass("synced");
    }
    });

    function syncPosition(el){
    var current = this.currentItem;
    $("#mainslider-nav")
    .find(".owl-item")
    .removeClass("synced")
    .eq(current)
    .addClass("synced")
    if($("#mainslider-nav").data("owlCarousel") !== undefined){
    center(current)
    }
    }

    $("#mainslider-nav").on("click", ".owl-item", function(e){
    e.preventDefault();
    var number = $(this).data("owlItem");
    sync1.trigger("owl.goTo",number);
    });

    function center(number){
    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
    var num = number;
    var found = false;
    for(var i in sync2visible){
    if(num === sync2visible[i]){
    var found = true;
    }
    }

    if(found===false){
    if(num>sync2visible[sync2visible.length-1]){
    sync2.trigger("owl.goTo", num - sync2visible.length+2)
    }else{
    if(num - 1 === -1){
    num = 0;
    }
    sync2.trigger("owl.goTo", num);
    }
    } else if(num === sync2visible[sync2visible.length-1]){
    sync2.trigger("owl.goTo", sync2visible[1])
    } else if(num === sync2visible[0]){
    sync2.trigger("owl.goTo", num-1)
    }
    }

    });

/* ?estimonials
   ========================================================================== */
 $(document).ready(function() {
        $("#testimonials-carousel").owlCarousel({
            items: 1,
            autoPlay: 5000,
            itemsDesktop: [1199, 1],
            itemsDesktopSmall: [979, 1],
            itemsTablet: [768, 1]
        });
    });

    // ______________ STATS
jQuery(document).ready(function() {
$('.statistics').waypoint(function() {

 $('#myStat1').circliful();
 $('#myStat2').circliful();
 $('#myStat3').circliful();
 $('#myStat4').circliful();

}, { offset: 800, triggerOnce: true });
});

    </script>
</body>

</html>
