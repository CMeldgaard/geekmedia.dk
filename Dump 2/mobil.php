<!DOCTYPE html>
<html lang="da">
	
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Dine kunder er i konstant bevægelse – fra platform til platform og fra sted til sted. forbered dit website i dag.">
		<title>Geek Media | Mobil</title>
		<?php include('includes/styles.php');?>
	</head>
	
	<body>
		
		<!-- Top Bar-->
		<?php include('includes/nav.php');?>
		<!-- End of Top Bar-->
		<!-- About Us -->
		<section class="mobil">
			<div class="breadcrumbs">
				<div class="row">
					<div class="col-sm-6">
						<h1>Mobil</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb">
							<li>Du er her: </li>
							<li><a href="/">Forside</a>
							</li>
							<li><a href="#">Web</a>
							</li>
							<li class="active">Mobil</li>
						</ol>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h2>Mobil</h2>
					<hr class="small">
					<p>Dine kunder er i konstant bevægelse – fra platform 
til platform og fra sted til sted</p>
				</div>
			</div>
		</section>
		<section>
			<div class="spacing-70"></div>
			<div class="row ">
				<div class="col-sm-8 center-block">
					<h2>Rør den. Klem den. Zoom den. Scroll den.</h2>
					<p>Fremtiden er dynamisk og mobil. Derfor skal du have et responsivt 
design, der tilpasser sig, så din hjemmeside fremstår tiltalende og 
overskuelig, uanset om dine kunder benytter en iPad, smartphone 
eller laptop.</p>
					<p>
						Et website, der kan tilpasse sig brugerens skærmstørrelse gør 
alting meget nemmere. Du kan anlægge én strategi for dit ind-hold 
og behøver kun opdatere det ét sted. Og du kan tænke content 
first, da det er dit indhold — særligt det skrevne ord — der skal 
fungere på alle platforme, uanset den visuelle indpakning</p>
					<p>
						Med denne strategi slipper du for at vedligeholde et særskilt 
mobilsite og kan fokusere på at bygge ét velstruktureret og 
brugervenligt website. Og dette site skal designes mobile 
first. Flere og flere af dine brugere vil nemlig se det først eller 
udelukkende på en smartphone eller en tablet. For deres skyld 
skal sitet koges ind til sin absolutte essens, og på denne baggrund kan vi designe udvidede oplevelser for brugere med mere 
skærmplads og hurtigere netforbindelser. 
					</p>
					<p>
						Jeg kan tilbyde dig et responsivt website, der fungerer på tværs af 
alle platforme! Såfremt du vil beholde dit nuværende website kan jeg også tilbyde 
dig at optimere dette, således at du ikke mister mobile kunder.
					</p>
				</div>
				<div class="spacing-70"></div>
				<div class="col-sm-8 center-block">
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.2s" id="mob_1">
						<h3>Mobil Strategi</h3>
						<p>Dybdegående undersøgelser og analyser til at formulere tydelige mobile strategier, skitsere muligheder og opfylde målsætninger.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.4s" id="mob_3">
						<h3>Mobile websites</h3>
						<p>Omdefinere weboplevelse til touch-baserede enheder. Responsive design og mobile hjemmesider for det moderne web.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.6s" id="mob_4">
						<h3>Interface design</h3>
						<p>Nøje overvejet design til at fremme moderne æstetik og levere unikke præsentationer, der er egnet til formålet.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.2s" id="mob_5">
						<h3>DESIGN & UX</h3>
						<p>Engagere og belønne publikum ved at levere bedre digitale oplevelser på minimalt plads.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.4s" id="mob_6">
						<h3>Videre end mobil</h3>
						<p>Udnyttelse teknologi til at udforske digitale muligheder i nye medier, ud over smartphones og tablets.</p>
					</div>
				</div>
				<div class="col-md-12 jumpDown">
					<a class="btn btn-jump" href="#cms">Læs mere</a>
				</div>
			</div>
			
			<div class="spacing-70"></div>
			
			<!--  Call to Action -->
    <section class="calltoaction">

        <div class="row wow zoomIn" data-wow-delay="0.2s">
            <div class="col-sm-10 com-md-8 center-block">

                <div class="row no-gutter cta-content">
                    <div class="col-sm-4">
                        <div class="offer wow fadeInUp" data-wow-delay="0.5s">
                            <span>LAD OS</span>
                            <a href="/planner"><h2>STARTE</h2></a>
                            <span>I DAG</span>
                        </div>
                    </div>
                    <div class="col-sm-8">

                        <div class="offerdescription wow fadeInUp" data-wow-delay="0.7s">
                            <h2>START ALLEREDE I DAG</h2>
                            <p>ved at udfylde min online formular og få en uforpligtende samtale</p>
                        </div>
                    </div>
                </div>
            </div>	
        </div>

    </section>
    <!-- End of Call to Action -->
			<div class="spacing-70"></div>
			<div class="row ">
				<div class="col-sm-8 center-block" id="cms">
					<h2>Industri førende platforme</h2>
					<p>Uanset dine krav til dit mobile website, uanset om du ønsker en webapp eller et selvstændigt mobilt website, arbejder jeg med de mest effektive platforme, der sikre levering af resultater, der er skræddersyet til netop dit website.</p>
					<img src="/images/logo_bootstrap.jpg" width="260" height="100" alt="Boostrap framework">
					<img src="/images/logo_ionic.png" width="260" height="100" alt="Ionic framework">
					
					<p class="cta">Lyst til at snakke mobilt? <a href="/planner" title="Start et projekt">Lad os tale samme i dag!</a></p>
				</div>
			</div>
		</section>

<!-- End of About Us -->

<?php include('includes/bottom.php');?>

<script type="text/javascript">
	// ______________  TOOLTIPS
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
	});
	
	// ______________ TABS
	$('#shared-hosting-tabs').responsiveTabs({
		startCollapsed: 'accordion'
	});
</script>
</body>

</html>
