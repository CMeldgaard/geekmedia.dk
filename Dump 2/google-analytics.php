<!DOCTYPE html>
<html lang="da">
	
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="Description" content="Hvis din side har kommercielle mål, altså skaffer henvendelser til din forretning, eller direkte salg, så er det vigtigt at opsætte mål i Google Analytics, så du kan spore hvor dine konverteringer kommer fra.">
		<title>Geek Media | Google Analytics</title>
		<?php include('includes/styles.php');?>
	</head>
	
	<body>
		
		<!-- Top Bar-->
		<?php include('includes/nav.php');?>
		<!-- End of Top Bar-->
		<!-- About Us -->
		<section class="google-analytics">
			<div class="breadcrumbs">
				<div class="row">
					<div class="col-sm-6">
						<h1>Google Analytics</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb">
							<li>Du er her: </li>
							<li><a href="/">Forside</a>
							</li>
							<li><a href="#">Web</a>
							</li>
							<li class="active">Google Analytics</li>
						</ol>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h2>Google Analytics</h2>
					<hr class="small">
					<p>Direkte indsigt i dit websites besøgende</p>
				</div>
			</div>
		</section>
		<section>
			<div class="spacing-70"></div>
			<div class="row ">
				<div class="col-sm-8 center-block">
					<h2>Følg dine besøgende fra start til slut</h2>
					<p>Hvis din side har kommercielle mål, altså skaffer henvendelser til din forretning, eller direkte salg, så er det vigtigt at opsætte mål i Google Analytics, så du kan spore hvor dine konverteringer kommer fra.</p>
<p>
Google Analytics er et af mange analyseværktøjer der kan bruges til at forstå brugernes adfærd på din hjemmeside. Du kan f.eks. finde ud af hvilke trafikkilder der bidrager til den største omsætning, hvilke søgeord der leverer mest trafik eller noget helt andet. Adfærd på nettet er gennemsigtigt og der er næsten ingen grænser for hvad du kan få svar på med den rette viden og opsætning.
	</p>
	<p>Med det rette datagrundlag i Google Analytics kan du få den fornøden viden til at foretage konverteringsoptimering af din hjemmeside, sikre at du bruger dit annoncebudget optimalt ved ikke at spilde dem på værdiløse trafikkilder og generelt set få en langt bedre forståelse for dine brugers købsadfærd i kombination med en effektiv søgeordsanalyse der giver indsigt om deres søgeadfærd.</p>
	<p>
	Den optimale opsætning af Google Analytics afhænger af din forretningsstrategi, mål og ønsker. På baggrund af den tekniske opsætning kan der opsættes løbende rapporter og såkaldte dashboards for at følge udviklingen og spotte eventuelle trends som du kan målrette med din markedsføring.
	</p>
	<p>
	Jeg hjælpe dig med at alt indenfor opsætning, rapportering og webanalyse med Google Analytics. 
	</p>
	</div>
	<div class="spacing-70"></div>
				<div class="col-sm-8 center-block">
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.2s" id="analyt_1">
						<h3>Præsentation</h3>
						<p>Se alle dataene i nemme og overskuelige diagrammer efter eget valg.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.4s" id="analyt_2">
						<h3>Segmentering</h3>
						<p>Segmenter og filtrer alle dine data så du kun fokusere på din ønskede målgruppe.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.6s" id="analyt_3">
						<h3>Følg dem</h3>
						<p>Følg dine besøgende hele vejen igennem sitet fra ankomst til afgang.</p>
					</div>
				</div>
				<br style="clear:both;" />
				<div class="col-md-12 jumpDown">
					<a class="btn btn-jump" href="#cms">Læs mere</a>
				</div>
			</div>
			
			<div class="spacing-70"></div>
			
			<!--  Call to Action -->
    <section class="calltoaction">

        <div class="row wow zoomIn" data-wow-delay="0.2s">
            <div class="col-sm-10 com-md-8 center-block">

                <div class="row no-gutter cta-content">
                    <div class="col-sm-4">
                        <div class="offer wow fadeInUp" data-wow-delay="0.5s">
                            <span>LAD OS</span>
                            <a href="/planner"><h2>STARTE</h2></a>
                            <span>I DAG</span>
                        </div>
                    </div>
                    <div class="col-sm-8">

                        <div class="offerdescription wow fadeInUp" data-wow-delay="0.7s">
                            <h2>START ALLEREDE I DAG</h2>
                            <p>ved at udfylde min online formular og få en uforpligtende samtale</p>
                        </div>
                    </div>
                </div>
            </div>	
        </div>

    </section>
    <!-- End of Call to Action -->
			<div class="spacing-70"></div>
			<div class="row ">
				<div class="col-sm-8 center-block" id="cms">
					<h2>Få styr på din Analytics</h2>
					<p>
					Dykker du blot ind i Google Analytics uden at vide hvad du skal kigge efter og bruge alle dataene til? Mit mål er at stoppe folk fra at gå ind i Google Analytics for at se på de samme gamle data, men snarere at udvikle en struktureret tilgang til at løse problemer og besvare spørgsmål.
					</p>
					<p>
					Jeg kan tilbyde undervisning på alle niveauer, med fokus på at sikre en grundlæggende forståelse for Analytics og dataene der præsenteres, og også, at folk forstår at generere de rapporter, de har brug for på en dag-til-dag basis.</p>
					<p class="cta">Klar til at indsamle data? <a href="/planner" title="Start et projekt">Kontakt mig i dag!</a></p>
				</div>
			</div>
		</section>

<!-- End of About Us -->

<?php include('includes/bottom.php');?>
</body>

</html>
