<?php
	
	if ($_SERVER['REQUEST_METHOD']=="POST"){
		// Collect the posted search query
		$q = $_POST["searchStr"];
		
		// Clean up by removing unwanted characters
		$qclean = ereg_replace("[^ 0-9a-zA-Z]", " ", $q);
		
		// Remove multiple adjacent spaces
		while (strstr($qclean, "  ")) {
			$qclean = str_replace("  ", " ", $qclean);
		}
		
		// Replace single spaces with a URL friendly plus sign
		$qclean = str_replace(" ", "+", $qclean);
		
		// If validation has passed, redirect to the URL rewritten search page
		header( 'Location: /blog/sog/'.$q );
		// HTML is output after here – NOT BEFORE!
	}	
?>

<?php include 'includes/datacon.php' ?>
<?php include 'includes/functions.php' ?>
<!DOCTYPE html>
<html lang="da">
	
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Geek Media | Blog</title>
		<?php include('includes/styles.php');?>
	</head>
	
	<body>
		
		<!-- Top Bar-->
		<?php include('includes/nav.php');?>
		<!-- End of Top Bar-->
		<div class="breadcrumbs mb0 solid">
			<div class="row">
				<div class="col-sm-6">
					<h1>Blog</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb">
						<li>Du er her: </li>
						<li><a href="/">Forside</a>
						</li>
						<li class="active">Blog</li>
					</ol>
				</div>
			</div>
		</div>

			<div class="blog">
        <div class="row">
            <div class="col-sm-8">
				<?php
											$cat = $_GET["cat"];
											//Checks for category selection, if none, show all sorted by date
											if ($cat == "") {
												$sqlString = "SELECT LEFT(blogContent, 550) as shortBlog, blog.blogCategoryID, blogID, blogImage, blogLink, blogTitle, blog.createdBy, createdDate, blogcategory.*, blogwriters.* FROM blog INNER JOIN blogcategory ON blog.blogCategoryID = blogcategory.blogCategoryID INNER JOIN blogwriters on blog.createdBy = blogwriters.createdBy order by createdDate desc LIMIT 5";
												$result = mysqli_query($conn, $sqlString);
												}else{
												$stmt = mysqli_prepare($conn, "SELECT LEFT(blogContent, 550) as shortBlog, blog.blogCategoryID, blogID, blogImage, blogLink, blogTitle, blog.createdBy, createdDate, blogcategory.*, blogwriters.* FROM blog INNER JOIN blogcategory ON blog.blogCategoryID = blogcategory.blogCategoryID INNER JOIN blogwriters on blog.createdBy = blogwriters.createdBy where catURL=? order by createdDate desc");
												
												/* bind parameters for markers */
												mysqli_stmt_bind_param($stmt, "s", $cat);
												
												/* execute query */
												mysqli_stmt_execute($stmt);
												
												/* instead of bind_result: */
												$result = $stmt->get_result();
											}
											
											if (mysqli_num_rows($result) > 0) {
												// output data of each row
												while($row = mysqli_fetch_assoc($result)) {  
													
													setlocale(LC_ALL, "danish");
													$creaDate = strftime("%d", strtotime($row[createdDate]));
													$creaMonth = strftime("%b", strtotime($row[createdDate]));
												?>
												
												<!-- Blog Post-->
												<article>
													<img src="/images/article/<?php echo $row["blogImage"]?>" class="resp-img" alt="<?php echo $row["blogTitle"]?>">
													<div class="post-content">
														<h2><a href="/blog/<?php echo $row["blogLink"]?>"><?php echo $row["blogTitle"]?></a></h2>
														<div class="thedate"><?php echo $row[createdDate]?> | I <a href="/blog/kategori/<?php echo $row["catURL"]?>"><?php echo $row["catName"]?></a></div>
														<hr>
														<p><?php 
																	
																	echo TrimShort($row["shortBlog"]);
																?> .....</p>

														<a class="button" href="/blog/<?php echo $row["blogLink"]?>">Læs videre →</a>
													</div>
												</article>
												<!-- End of Blog Post-->
												
												
												
												<?php
												}
												} else {
											?>
											<article>
													<img src="/images/article/empty.jpg" class="resp-img" alt="Det er helt tomt">
													<div class="post-content">
														<h2><a href="/blog/<?php echo $row["blogLink"]?>"><?php echo $row["blogTitle"]?></a></h2>
														<div class="thedate"><?php echo $row[createdDate]?></div>
														<hr>
														<p class="subHeadline">Her er endnu ikke noget blogindlæg, prøv en anden kategori eller vend tilbage en anden gang</p>
													</div>
												</article>
												
											
											
											<?php
											}
										?>
            </div>

            <?php include('includes/sidebar-blog.php');?>

        </div>
    </div>


			<!-- End of About Us -->
			
			<?php include('includes/bottom.php');?>
			<script>
				$(document).ready(function() {
					$("#values").owlCarousel({
						items: 1,
						autoPlay: 5000,
						itemsDesktop: [1199, 1],
						itemsDesktopSmall: [979, 1],
						itemsTablet: [768, 1]
					});
					
					$("#testimonials-carousel").owlCarousel({
						items: 1,
						autoPlay: 5000,
						itemsDesktop: [1199, 1],
						itemsDesktopSmall: [979, 1],
						itemsTablet: [768, 1]
					});
				});
			</script>
		</body>
		
	</html>
