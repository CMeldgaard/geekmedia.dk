<!DOCTYPE html>
<html lang="da">
	
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Din professionelle samarbejdspartner - jeg hjælper med store som små problemer">
		<title>Geek Media | Geek Partner</title>
		<?php include('includes/styles.php');?>
	</head>
	
	<body>
		
		<!-- Top Bar-->
		<?php include('includes/nav.php');?>
		<!-- End of Top Bar-->
		<!-- About Us -->
		<section class="geek-partner">
			<div class="breadcrumbs">
				<div class="row">
					<div class="col-sm-6">
						<h1>Geek Partner</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb">
							<li>Du er her: </li>
							<li><a href="/">Forside</a>
							</li>
							<li><a href="#">Web</a>
							</li>
							<li class="active">Geek Partner</li>
						</ol>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h2>Geek Partner</h2>
					<hr class="small">
					<p>Din professionelle samarbejdspartner - jeg hjælper 
med store som små problemer</p>
				</div>
			</div>
		</section>
		<section>
			<div class="spacing-70"></div>
			<div class="row ">
				<div class="col-sm-8 center-block">
					<h2>Din nye partner.</h2>
					<p>Med en Geek Partner aftale har du hjælp til de problemer du 
skulle støde på. Jeg hjælper dig med vanskeligheder eller ting der 
ikke lige spiller.</p>
<p>
	</div>
	<div class="spacing-70"></div>
				<div class="col-sm-8 center-block">
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.2s" id="part_1">
						<h3>Månedlig support</h3>
						<p>Med Geek Partner har du månedligt 2 timer support, fejlretning og rådgivning.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.4s" id="part_2">
						<h3>Prioritering</h3>
						<p>Prioritering af alle dine supportsager sikre hurtigt respons og rettelse af opståede fejl.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.6s" id="part_3">
						<h3>Rig kontakt</h3>
						<p>Masser af supportmuligheder sikre at du hurtigt og knidningsfrit kan komme i kontakt med mig.</p>
					</div>
				</div>
				<br style="clear:both;" />
				<div class="spacing-70"></div>
	<div class="col-sm-8 center-block">
	<p>
	Det er ofte ikke muligt for virksomheder at have en professionel 
webmaster tilknyttet på fuld tid. Derfor tilbyder jeg denne Geek 
Partner aftale, hvor jeg sørger for at holde styr på jeres hjemmeside. 
Med Geek Partner har du månedligt 2 timer support, fejlretning og 
rådgivning, hvad enten det er til opsætning af nyheder, opsætning 
af produkter, fejlretning på hjemmesiden, rådgivning til om i f.eks. 
skal benytte de sociale medier, søgemaskineoptimering og meget 
mere.
	</p>
					<p>
						Med min partner aftale kan i drage nytte af flere fordele:</p>
					<ul class="featuresList">
					<li>
					Telefonsupport
					</li>
					<li>
					Skrive til Geek Medias support mail
					</li>
					<li>
					Bruge ticket systemet på mit kontrolpanel
					</li>
					<li>
					Skrive direkte med mig, i min live chat
					</li>
					<li>
					25% rabat på timer, udover de medfølgende 2
					</li>
					<li>
					Prioritering i forhold til kunder uden Geek Partner aftalen
					</li>
					</ul>
				</div>
				
				<div class="col-md-12 jumpDown">
					<a class="btn btn-jump" href="#priser">Se priserne</a>
				</div>
			</div>
			
			<div class="spacing-70"></div>
			
			<!--  Call to Action -->
    <section class="calltoaction">

        <div class="row wow zoomIn" data-wow-delay="0.2s">
            <div class="col-sm-10 com-md-8 center-block">

                <div class="row no-gutter cta-content">
                    <div class="col-sm-4">
                        <div class="offer wow fadeInUp" data-wow-delay="0.5s">
                            <span>LAD OS</span>
                            <a href="/planner"><h2>STARTE</h2></a>
                            <span>I DAG</span>
                        </div>
                    </div>
                    <div class="col-sm-8">

                        <div class="offerdescription wow fadeInUp" data-wow-delay="0.7s">
                            <h2>START ALLEREDE I DAG</h2>
                            <p>ved at udfylde min online formular og få en uforpligtende samtale</p>
                        </div>
                    </div>
                </div>
            </div>	
        </div>

    </section>
    <!-- End of Call to Action -->
			<div class="spacing-70"></div>
			<section class="pricingtables default" id="priser">
                        <div class="row">
                            <div class="col-sm-12">
                                <h2>Vælg den plan der passer dig bedst</h2>
								<p>Forskellige planer der dækker lige præcist dine behov.</p>
                            </div>
                        </div>

                        <div class="row no-gutter spacing-40">

                            <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.2s">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3 class="text-center">MÅNEDLIG</h3>
                                    </div>
                                    <div class="panel-body text-center">
                                        <h4>599,-</h4>
                                        <span class="per">PER MÅNED, EX MOMS</span>
                                    </div>
                                    <ul class="text-center">
                                        <li>Telefon support</li>
                                        <li>2 timers support</li>
                                        <li>Chat support</li>
                                        <li>Brug af support mail</li>
                                        <li>25% på øvrige support timer</li>
                                        <li>Support prioritering</li>
                                    </ul>
                                    <div class="panel-footer">
                                        <a class="btn btn-lg btn-pricetable" href="/planner">START I DAG</a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3 wow fadeInUp" data-wow-delay="0.4s">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3 class="text-center">KVARTAL</h3>
                                    </div>
                                    <div class="panel-body text-center">
                                       <h4>579,-</h4>
                                        <span class="per">PER MÅNED, EX MOMS - 1737kr i alt</span>
                                    </div>
                                    <ul class="text-center">
                                        <li>Telefon support</li>
                                        <li>2 timers support</li>
                                        <li>Chat support</li>
                                        <li>Brug af support mail</li>
                                        <li>25% på øvrige support timer</li>
                                        <li>Support prioritering</li>
                                    </ul>
                                    <div class="panel-footer">
                                        <a class="btn btn-lg btn-pricetable" href="/planner">START I DAG</a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3 most-popular wow fadeInUp" data-wow-delay="0.6s">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3 class="text-center">HALVÅRLIG</h3>
                                    </div>
                                    <div class="panel-body text-center">
                                        <h4>559,-</h4>
                                        <span class="per">PER MÅNED, EX MOMS - 3354kr i alt</span>
                                    </div>
                                    <ul class="text-center">
                                        <li>Telefon support</li>
                                        <li>2 timers support</li>
                                        <li>Chat support</li>
                                        <li>Brug af support mail</li>
                                        <li>25% på øvrige support timer</li>
                                        <li>Support prioritering</li>
                                    </ul>
                                    <div class="panel-footer">
                                        <a class="btn btn-lg btn-pricetable" href="/planner">START I DAG</a>
                                    </div>
                                </div>
                            </div>
							<div class="col-sm-3 wow fadeInUp" data-wow-delay="0.8s">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3 class="text-center">ÅRLIG</h3>
                                    </div>
                                    <div class="panel-body text-center">
                                        <h4>539,-</h4>
                                        <span class="per">PER MÅNED, EX MOMS - 6468kr i alt</span>
                                    </div>
                                    <ul class="text-center">
                                        <li>Telefon support</li>
                                        <li>2 timers support</li>
                                        <li>Chat support</li>
                                        <li>Brug af support mail</li>
                                        <li>25% på øvrige support timer</li>
                                        <li>Support prioritering</li>
                                    </ul>
                                    <div class="panel-footer">
                                        <a class="btn btn-lg btn-pricetable" href="/planner">START I DAG</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
		</section>

<!-- End of About Us -->

<?php include('includes/bottom.php');?>

<script type="text/javascript">
	// ______________  TOOLTIPS
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
	});
	
	// ______________ TABS
	$('#shared-hosting-tabs').responsiveTabs({
		startCollapsed: 'accordion'
	});
</script>
</body>

</html>
