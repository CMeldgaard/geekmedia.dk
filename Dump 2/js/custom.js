
	// ______________ OPEN CROWDIO CHAT
function ShowChat(){
		var win = document.getElementById("chatWidgetIframe").contentWindow;
		document.getElementById("chatWidgetIframe").style.display="block";document.getElementById("closedWidget").style.display="none";win.postMessage("openWidget","*");
		};

    "use strict";
	
	// ______________ CROWDIO CHAT
	(function(){var e,t,n;t=false;e=document.createElement("script");e.type="text/javascript";e.src="https://app.crowdio.com/visitor/loadjs/253";e.onload=e.onreadystatechange=function(){if(!t&&(!this.readyState||this.readyState=="complete")){t=true}};n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(e,n)})()
	
	
	
	// ______________ SMOOTH SCROLLING
	$(function() {
		$('a[href*=#]:not(.r-tabs-anchor)').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		});
	});
	
	
    // ______________ RESPONSIVE MENU
    $(document).ready(function() {
		
        $('#navigation').superfish({
            delay: 300,
            animation: {
                opacity: 'show',
                height: 'show'
			},
            speed: 'fast',
            dropShadows: false
		});
		
        $(function() {
            $('#navigation').slicknav({
                closedSymbol: "&#8594;",
                openedSymbol: "&#8595;"
			});
		});
		
	});
	
	
	
    // ______________ ANIMATE EFFECTS
    var wow = new WOW({
        boxClass: 'wow',
        animateClass: 'animated',
        offset: 0,
        mobile: false
	});
    wow.init();
	
    // ______________ BACK TO TOP BUTTON
	
    $(window).scroll(function() {
        if ($(this).scrollTop() > 300) {
            $('#back-to-top').fadeIn('slow');
			} else {
            $('#back-to-top').fadeOut('slow');
		}
	});
    $('#back-to-top').click(function() {
        $("html, body").animate({
            scrollTop: 0
		}, 600);
        return false;
	});


	// ______________ DOMAIN CHECKER
	$(function() {
		$("#domainSearch").click(function() {
			/* stop form from submitting normally */
			event.preventDefault();
			$(".domainSearcher").hide();
			$("#domainResults").show();
			$("#loaderImage").show();
			//Show loader
			//The following code starts the animation
			new imageLoader(cImageSrc, 'startAnimation()');
			$("#domainAppend").append("<p><br />Checker domæne</p>");
			var formData = {
				'domain'              : $('#domain').val(),
				'tld'             : $('#tld').val()
			};
			
			$.ajax({
				type        : 'GET', // define the type of HTTP verb we want to use (POST for our form)
				url         : '/includes/whois.php', // the url where we want to POST
				data        : formData,
				contentType: 'text/html',
				success: function(data) {
					$("#domainAppend").html("");
					$("#loaderImage").hide();
					stopAnimation();
					$("#domainAppend").append(data);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			})
			
		});
		
	});
	
	$(function() {
		$(document).on('click', '#newDomainSearch', function(){ 
			$("#domainResults").hide();
			$("#domainAppend").html("");
			$(".domainSearcher").show();
		});
	});