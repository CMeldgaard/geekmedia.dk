<!DOCTYPE html>
<html lang="da">
	
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Fra skærm til bord - Udtryksfulde publikationer der fanger læserne og skaber opmærksomhed">
		<title>Geek Media | Print design</title>
		<?php include('includes/styles.php');?>
	</head>
	
	<body>
		
		<!-- Top Bar-->
		<?php include('includes/nav.php');?>
		<!-- End of Top Bar-->
		<!-- About Us -->
		<section class="print-design">
			<div class="breadcrumbs">
				<div class="row">
					<div class="col-sm-6">
						<h1>Print design</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb">
							<li>Du er her: </li>
							<li><a href="/">Forside</a>
							</li>
							<li><a href="#">Design</a>
							</li>
							<li class="active">Print design</li>
						</ol>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h2>Print design</h2>
					<hr class="small">
					<p>Fra skærm til bord - Udtryksfulde publikationer der 
fanger læserne</p>
				</div>
			</div>
		</section>
		<section>
			<div class="spacing-70"></div>
			<div class="row ">
				<div class="col-sm-8 center-block">
					<h2>Tryk grafik og printdesign</h2>
					<p>En god måde at øge din virksomheds troværdighed og skabe 
opmærksomhed, er ved hjælp af trykte medier. Med en allerede 
solid digital markedsføring i din virksomhed, er det logiske valg at 
markedsføre sig gennem trykte medier, og det kan jeghjælpe med 
her ved Geek Media. Hvis du søger en samarbejdspartner der er 
professionel, idérig, erfaren og har kompetencerne, er det mig du 
skal vælge til dit næste projekt.</p>
<p>
Jeg skaber et design der er i overensstemmelse med din 
virksomheds identitet og idealer, hvilket er det absolut vigtigste 
når der skal laves trykdesign der skaber resultater. Jeg leverer et 
produkt der er interessant for kunden, samt får din virksomhed 
til at fremstå som kompetent og kvalitetsbevidst overfor kunden. 
Jeg arbejder målrettet på at skabe den løsning der giver mest 
interesse fra den ønskede målgruppe.
	</p>
	<p>Jeg er behjælpelige i alle faser af arbejdsprocessen, fra idé til 
godkendelse af layout til tryk.</p>
	</div>
	<div class="spacing-70"></div>
				<div class="col-sm-8 center-block">
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.2s" id="print_1">
						<h3>Design strategi</h3>
						<p>Målbevidste anbefalinger baseret på konstruktiv design konsultation, brand analyse og gennemgang af målsætninger.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.4s" id="print_2">
						<h3>Løsninger</h3>
						<p>
							Leverer løsning i bredt spektrum som passer til jeres mål og målgruppe, fra plakater til bøger.
						</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.6s" id="print_3">
						<h3>User experience</h3>
						<p>Løsningsforslag til at fremme engagement gennem enkle, intuitive og dejlige oplevelser på tværs af alle løsninger.</p>
					</div>
				</div>
			</div>
			<div class="row ">
				<div class="col-sm-8 center-block" id="cms">

					<p class="cta">Klar til at trykke? <a href="/planner" title="Start et projekt">Kontakt mig i dag!</a></p>
				</div>
			</div>
		</section>

<!-- End of About Us -->

<?php include('includes/bottom.php');?>
</body>

</html>
