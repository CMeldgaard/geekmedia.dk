<!DOCTYPE html>
<html lang="da">
	
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Find det eller de domæner du ønsker og lad mig stå for opsætningen!">
		<title>Geek Media | Domæne registrering</title>
		<?php include('includes/styles.php');?>
	</head>
	
	<body>
		
		<!-- Top Bar-->
		<?php include('includes/nav.php');?>
		<!-- End of Top Bar-->
		<!-- Domæne registrering -->
		<section class="domains">
			<div class="breadcrumbs">
				<div class="row">
					<div class="col-sm-6">
						<h1>Domæne registrering</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb">
							<li>Du er her: </li>
							<li><a href="/">Forside</a>
							</li>
							<li><a href="#">Hosting</a>
							</li>
							<li class="active">Domæne registrering</li>
						</ol>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h2>Ligesom et fingeraftryk. Vælge det rigtige domænenavn.</h2>
					<hr class="small">
					<p>Find det eller de domæner du ønsker og lad mig stå for opsætningen!</p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-10 com-md-8 center-block domainSearcher">
					
					<form class="form-inline domainsearch" method="post" action="">
						<div class="row no-gutter">
							<div class="col-sm-8">
								<input type="text" class="form-control" name="domain" id="domain" onfocus="if (this.value == 'Indtast domænenavn') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Indtast domænenavn';}" value="Indtast domænenavn">
							</div>
							<div class="col-sm-2">
								<select name="tld" id="tld" class="form-control">
									<option value="dk" selected="">dk</option>
									<option value="com">com</option>
									<option value="org">org</option>
									<option value="net">net</option>
									<option value="info">info</option>
									<option value="biz">biz</option>
									<option value="co.uk">co.uk</option>
									<option value="us">us</option>
									<option value="de">de</option>
									<option value="as">as</option>
									<option value="se">se</option>
									<option value="nu">nu</option>
									<option value="tv">tv</option>
									<option value="name">name</option>
									<option value="it">it</option>
									<option value="cc">cc</option>
									<option value="no">no</option>
									<option value="eu">eu</option>
									<option value="be">be</option>
									<option value="nl">nl</option>
									<option value="pl">pl</option>
									<option value="lv">lv</option>
									<option value="in">in</option>
									<option value="ch">ch</option>
									<option value="es">es</option>
									<option value="fr">fr</option>
									<option value="cz">cz</option>
									<option value="hu">hu</option>
									<option value="cn">cn</option>
									<option value="mobi">mobi</option>
									<option value="at">at</option>
									<option value="lu">lu</option>
									<option value="org.uk">org.uk</option>
									<option value="asia">asia</option>
									<option value="jp">jp</option>
									<option value="co.nz">co.nz</option>
									<option value="co">co</option>
									<option value="me">me</option>
									<option value="bz">bz</option>
									<option value="io">io</option>
									<option value="pt">pt</option>
									<option value="bike">bike</option>
									<option value="clothing">clothing</option>
									<option value="guru">guru</option>
									<option value="holdings">holdings</option>
									<option value="plumbing">plumbing</option>
									<option value="singles">singles</option>
									<option value="ventures">ventures</option>
									<option value="menu">menu</option>
									<option value="camera">camera</option>
									<option value="estate">estate</option>
									<option value="equipment">equipment</option>
									<option value="gallery">gallery</option>
									<option value="graphics">graphics</option>
									<option value="lighting">lighting</option>
									<option value="photography">photography</option>
									<option value="contractors">contractors</option>
									<option value="construction">construction</option>
									<option value="directory">directory</option>
									<option value="land">land</option>
									<option value="kitchen">kitchen</option>
									<option value="technology">technology</option>
									<option value="today">today</option>
									<option value="sexy">sexy</option>
									<option value="tattoo">tattoo</option>
									<option value="diamonds">diamonds</option>
									<option value="enterprises">enterprises</option>
									<option value="tips">tips</option>
									<option value="voyage">voyage</option>
									<option value="shoes">shoes</option>
									<option value="careers">careers</option>
									<option value="photos">photos</option>
									<option value="recipes">recipes</option>
									<option value="limo">limo</option>
									<option value="domains">domains</option>
									<option value="cab">cab</option>
									<option value="berlin">berlin</option>
									<option value="academy">academy</option>
									<option value="center">center</option>
									<option value="company">company</option>
									<option value="computer">computer</option>
									<option value="management">management</option>
									<option value="systems">systems</option>
									<option value="uno">uno</option>
									<option value="builders">builders</option>
									<option value="email">email</option>
									<option value="solutions">solutions</option>
									<option value="support">support</option>
									<option value="training">training</option>
									<option value="ceo">ceo</option>
									<option value="camp">camp</option>
									<option value="education">education</option>
									<option value="glass">glass</option>
									<option value="institute">institute</option>
									<option value="repair">repair</option>
									<option value="coffee">coffee</option>
									<option value="florist">florist</option>
									<option value="house">house</option>
									<option value="international">international</option>
									<option value="solar">solar</option>
									<option value="gift">gift</option>
									<option value="guitars">guitars</option>
									<option value="link">link</option>
									<option value="photo">photo</option>
									<option value="pics">pics</option>
									<option value="buzz">buzz</option>
									<option value="holiday">holiday</option>
									<option value="marketing">marketing</option>
									<option value="blue">blue</option>
									<option value="kim">kim</option>
									<option value="pink">pink</option>
									<option value="red">red</option>
									<option value="onl">onl</option>
									<option value="codes">codes</option>
									<option value="farm">farm</option>
									<option value="build">build</option>
									<option value="agency">agency</option>
									<option value="bargains">bargains</option>
									<option value="boutique">boutique</option>
									<option value="cheap">cheap</option>
									<option value="zone">zone</option>
									<option value="kiwi">kiwi</option>
									<option value="qpon">qpon</option>
									<option value="cool">cool</option>
									<option value="watch">watch</option>
									<option value="club">club</option>
									<option value="dance">dance</option>
									<option value="democrat">democrat</option>
									<option value="expert">expert</option>
									<option value="works">works</option>
									<option value="exposed">exposed</option>
									<option value="foundation">foundation</option>
									<option value="best">best</option>
									<option value="wiki">wiki</option>
									<option value="cruises">cruises</option>
									<option value="flights">flights</option>
									<option value="rentals">rentals</option>
									<option value="vacations">vacations</option>
									<option value="villas">villas</option>
									<option value="immobilien">immobilien</option>
									<option value="ninja">ninja</option>
									<option value="xyz">xyz</option>
									<option value="condos">condos</option>
									<option value="maison">maison</option>
									<option value="properties">properties</option>
									<option value="tienda">tienda</option>
									<option value="futbol">futbol</option>
									<option value="reviews">reviews</option>
									<option value="social">social</option>
									<option value="jetzt">jetzt</option>
									<option value="bid">bid</option>
									<option value="trade">trade</option>
									<option value="webcam">webcam</option>
									<option value="uk">uk</option>
									<option value="dating">dating</option>
									<option value="events">events</option>
									<option value="partners">partners</option>
									<option value="productions">productions</option>
									<option value="cards">cards</option>
									<option value="catering">catering</option>
									<option value="community">community</option>
									<option value="cleaning">cleaning</option>
									<option value="ink">ink</option>
									<option value="industries">industries</option>
									<option value="parts">parts</option>
									<option value="supplies">supplies</option>
									<option value="supply">supply</option>
									<option value="tools">tools</option>
									<option value="wang">wang</option>
									<option value="fish">fish</option>
									<option value="report">report</option>
									<option value="vision">vision</option>
									<option value="co.com">co.com</option>
									<option value="blackfriday">blackfriday</option>
									<option value="christmas">christmas</option>
									<option value="services">services</option>
									<option value="pub">pub</option>
									<option value="bar">bar</option>
									<option value="rest">rest</option>
									<option value="wien">wien</option>
									<option value="capital">capital</option>
									<option value="engineering">engineering</option>
									<option value="exchange">exchange</option>
									<option value="gripe">gripe</option>
									<option value="moda">moda</option>
									<option value="tokyo">tokyo</option>
									<option value="voting">voting</option>
									<option value="moe">moe</option>
									<option value="associates">associates</option>
									<option value="lease">lease</option>
									<option value="media">media</option>
									<option value="pictures">pictures</option>
									<option value="consulting">consulting</option>
									<option value="kaufen">kaufen</option>
									<option value="reisen">reisen</option>
									<option value="town">town</option>
									<option value="toys">toys</option>
									<option value="university">university</option>
									<option value="fail">fail</option>
									<option value="financial">financial</option>
									<option value="limited">limited</option>
									<option value="wtf">wtf</option>
									<option value="actor">actor</option>
									<option value="rocks">rocks</option>
									<option value="care">care</option>
									<option value="clinic">clinic</option>
									<option value="dental">dental</option>
									<option value="surgery">surgery</option>
									<option value="career">career</option>
									<option value="cash">cash</option>
									<option value="fund">fund</option>
									<option value="investments">investments</option>
									<option value="tax">tax</option>
									<option value="haus">haus</option>
									<option value="black">black</option>
									<option value="discount">discount</option>
									<option value="fitness">fitness</option>
									<option value="furniture">furniture</option>
									<option value="schule">schule</option>
									<option value="hamburg">hamburg</option>
									<option value="claims">claims</option>
									<option value="credit">credit</option>
									<option value="creditcard">creditcard</option>
									<option value="gratis">gratis</option>
									<option value="audio">audio</option>
									<option value="hiphop">hiphop</option>
									<option value="juegos">juegos</option>
									<option value="cologne">cologne</option>
									<option value="koeln">koeln</option>
									<option value="global">global</option>
									<option value="london">london</option>
									<option value="yokohama">yokohama</option>
									<option value="accountants">accountants</option>
									<option value="digital">digital</option>
									<option value="finance">finance</option>
									<option value="insure">insure</option>
									<option value="cooking">cooking</option>
									<option value="country">country</option>
									<option value="fishing">fishing</option>
									<option value="horse">horse</option>
									<option value="rodeo">rodeo</option>
									<option value="vodka">vodka</option>
									<option value="vegas">vegas</option>
									<option value="church">church</option>
									<option value="guide">guide</option>
									<option value="life">life</option>
									<option value="loans">loans</option>
									<option value="republican">republican</option>
									<option value="host">host</option>
									<option value="website">website</option>
									<option value="press">press</option>
									<option value="direct">direct</option>
									<option value="place">place</option>
									<option value="beer">beer</option>
									<option value="surf">surf</option>
									<option value="deals">deals</option>
									<option value="desi">desi</option>
									<option value="city">city</option>
									<option value="soy">soy</option>
									<option value="healthcare">healthcare</option>
									<option value="army">army</option>
									<option value="airforce">airforce</option>
									<option value="navy">navy</option>
									<option value="vet">vet</option>
									<option value="ooo">ooo</option>
									<option value="lawyer">lawyer</option>
									<option value="com.tr">com.tr</option>
									<option value="com.gr">com.gr</option>
									<option value="com.ng">com.ng</option>
									<option value="market">market</option>
									<option value="mortgage">mortgage</option>
									<option value="gifts">gifts</option>
									<option value="restaurant">restaurant</option>
									<option value="top">top</option>
									<option value="engineer">engineer</option>
									<option value="gent">gent</option>
									<option value="click">click</option>
									<option value="diet">diet</option>
									<option value="help">help</option>
									<option value="hosting">hosting</option>
									<option value="property">property</option>
									<option value="business">business</option>
									<option value="immo">immo</option>
									<option value="network">network</option>
									<option value="pizza">pizza</option>
									<option value="auction">auction</option>
									<option value="software">software</option>
									<option value="dentist">dentist</option>
									<option value="rehab">rehab</option>
									<option value="cat">cat</option>
									<option value="world">world</option>
									<option value="degree">degree</option>
									<option value="gives">gives</option>
									<option value="brussels">brussels</option>
									<option value="vlaanderen">vlaanderen</option>
									<option value="forsale">forsale</option>
									<option value="how">how</option>
									<option value="space">space</option>
									<option value="band">band</option>
									<option value="rip">rip</option>
									<option value="casa">casa</option>
									<option value="work">work</option>
									<option value="lgbt">lgbt</option>
									<option value="delivery">delivery</option>
									<option value="energy">energy</option>
									<option value="cricket">cricket</option>
									<option value="party">party</option>
									<option value="yoga">yoga</option>
									<option value="science">science</option>
									<option value="cymru">cymru</option>
									<option value="wales">wales</option>
									<option value="coach">coach</option>
									<option value="legal">legal</option>
									<option value="memorial">memorial</option>
									<option value="money">money</option>
									<option value="green">green</option>
									<option value="nz">nz</option>
									<option value="tires">tires</option>
									<option value="flowers">flowers</option>
									<option value="wedding">wedding</option>
									<option value="fashion">fashion</option>
									<option value="garden">garden</option>
									<option value="poker">poker</option>
									<option value="fit">fit</option>
									<option value="sale">sale</option>
									<option value="video">video</option>
									<option value="design">design</option>
									<option value="bingo">bingo</option>
									<option value="chat">chat</option>
									<option value="style">style</option>
									<option value="tennis">tennis</option>
									<option value="one">one</option>
									<option value="apartments">apartments</option>
									<option value="casino">casino</option>
									<option value="football">football</option>
									<option value="school">school</option>
									<option value="adult">adult</option>
									<option value="porn">porn</option>
									<option value="markets">markets</option>
									<option value="irish">irish</option>
									<option value="date">date</option>
									<option value="faith">faith</option>
									<option value="review">review</option>
									
								</select>
							</div>
							<div class="col-sm-2">
								<button type="submit" id="domainSearch" class="btn btn-primary" style="width:100%">SØG</button>
							</div>
							
						</div>
					</form>
					<div id="domainextensions" class="owl-carousel owl-theme">
						<div class="owl-item">
							<div class="item">
								<div class="extension">.dk</div>
							</div>
						</div>
						<div class="owl-item">
							<div class="item">
								<div class="extension">.com</div>
							</div>
						</div>
						<div class="owl-item">
							<div class="item">
								<div class="extension">.org</div>
							</div>
						</div>
						<div class="owl-item">
							<div class="item">
								<div class="extension">.net</div>
							</div>
						</div>
						<div class="owl-item">
							<div class="item">
								<div class="extension">.info</div>
							</div>
						</div>
						<div class="owl-item">
							<div class="item">
								<div class="extension">.biz</div>
							</div>
						</div>
						<div class="owl-item">
							<div class="item">
								<div class="extension">.co.uk</div>
							</div>
						</div>
						<div class="owl-item">
							<div class="item">
								<div class="extension">.nu</div>
							</div>
						</div>
						<div class="owl-item">
							<div class="item">
								<div class="extension">.eu</div>
							</div>
						</div>
						<div class="owl-item">
							<div class="item">
								<div class="extension">.bike</div>
							</div>
						</div>
						<div class="owl-item">
							<div class="item">
								<div class="extension">.menu</div>
							</div>
						</div>
						<div class="owl-item">
							<div class="item">
								<div class="extension">.tips</div>
							</div>
						</div>
						<div class="owl-item">
							<div class="item">
								<div class="extension">.computer</div>
							</div>
						</div>
						<div class="owl-item">
							<div class="item">
								<div class="extension">.agency</div>
							</div>
						</div>
						<div class="owl-item">
							<div class="item">
								<div class="extension">.bar</div>
							</div>
						</div>
						<div class="owl-item">
							<div class="item">
								<div class="extension">.ninja</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-8 col-sm-offset-2" id="domainResults">
					<div id="loaderImage" style="margin: 0 auto;"></div>
					<div id="domainAppend" style="margin: 0 auto;"></div>
				</div>
			</div>
		</section>
		<section>
			<div class="row">
			<div class="spacing-70"></div>
				<div class="col-sm-8 center-block">
					<div class="wow" data-wow-delay="0.2s">
						<h2>Køb dit nye domæne her</h2>
					<p>Skal du have et nyt domæne, så kan du nemt bestille dit nye domæne igennem mig. 
					Jeg står for alt bestilling og opsætning af dine domæner.
					Du kan bruge søegfunktionen herover funktionen, hvis du vil skabe dig et overblik over hvilke domænenavne der er ledige.</p>
					</div>
				</div>
				<div class="col-md-12 jumpDown">
					<a class="btn btn-jump" href="#prices">Se priserne</a>
				</div>
			</div>
		</section>
		<div class="about-quote odd">
			<div class="row full-width no-gutter">
				<div class="col-sm-6 col-sm-push-6 about-quote-column">
					<div class="thequote wow fadeInRight" data-wow-delay="0.4s">
						<h5>It does not matter how slowly you go as long as you do not stop.</h5>
						<span>Confucius</span>
					</div>
				</div>
			</div>
		</div>
		<section class="shared-features white spacing-70" id="prices">
			<div class="row">
				<div class=" col-sm-8 center-block">
					<div class="wow fadeInLeft" data-wow-delay="0.2s">
						<h2>Domæne priser</h2>
					<p>Nedenfor finder du en prisliste over de mest populære domæner. Ønsker du en fuld liste så kontakt mig for dette.</p>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="prices wow fadeInRight" data-wow-delay="0.4s">						
						<!-- 2nd tab  -->
								<div class="row">
										<ul class="prices-popular">
											<li class="col-sm-3">
											<div class="panel">
											<h3 class="text-center">.dk</h3>
											<p class="text-center">fra 45,-</p>
											</div>
											</li>

											<li class="col-sm-3">
											<div class="panel">
											<h3 class="text-center">.com</h3>
											<p class="text-center">fra 90,-</p>
											</div>
											</li>

											<li class="col-sm-3">
											<div class="panel">
											<h3 class="text-center">.eu</h3>
											<p class="text-center">fra 90,-</p>
											</div>
											</li>

											<li class="col-sm-3">
											<div class="panel">
											<h3 class="text-center">.org</h3>
											<p class="text-center">fra 90,-</p>
											</div>
											</li>
										</ul>
								</div>
								<div class="row">
<div class="small-12 columns">
<hr>
<ul class="prices-secondary">
<li class="col-sm-2 col-xs-6"><span class="label">.de</span> fra 90,-</li>
<li class="col-sm-2 col-xs-6"><span class="label">.nu</span> fra 200,-</li>
<li class="col-sm-2 col-xs-6"><span class="label">.co.uk</span> fra 90,-</li>
<li class="col-sm-2 col-xs-6"><span class="label">.net</span> fra 90,-</li>
<li class="col-sm-2 col-xs-6"><span class="label">.info</span> fra 90,-</li>
<li class="col-sm-2 col-xs-6"><span class="label">.biz</span> fra 90,-</li>
<li class="col-sm-2 col-xs-6"><span class="label">.nu</span> fra 200,-</li>
<li class="col-sm-2 col-xs-6"><span class="label">.me</span> fra 200,-</li>
<li class="col-sm-2 col-xs-6"><span class="label">.biz</span> fra 90,-</li>
<li class="col-sm-2 col-xs-6"><span class="label">.us</span> fra 90,-</li>
<li class="col-sm-2 col-xs-6"><span class="label">.as</span> fra 450,-</li>
<li class="col-sm-2 col-xs-6"><span class="label">.se</span> fra 130,-</li>
</ul>
</div>
</div>
					</div>
					<div class="spacing-70"></div>
				</div>
			</div>
		</section>
		 
		<section class="domainfeatures spacing-70">
		<div class="row">
			<div class="col-sm-8 center-block">
				<div class="wow fadeInLeft" data-wow-delay="0.2s">
						<h2>Nyttig viden</h2>
					<p>Nedenfor finder du nyttig viden omkring domænerne jeg tilbyder.</p>
					</div>
				<div class="center-block wow fadeIn spacing-40" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeIn;">
                <div id="accordion" class="panel-group">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" class="r-tabs-anchor">Hvordan fornyer jeg mine domæner?</a></h4>
                        </div>

                        <div id="collapse1" class="panel-collapse collapse in" aria-expanded="true">
                            <div class="panel-body">
                                <p>Alle domæner pånår .dk, bliver fornyet igennem mig. .dk domæner skal fornyes igennem DK-Hostmaster.</p>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="collapsed r-tabs-anchor" aria-expanded="false">Hvad sker der hvis jeg ikke fornyer mit domæne?</a></h4>
                        </div>

                        <div id="collapse2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                <p>Såfremt at du ikke fornyer dit domæne vil det blive suspenderet, hvorved besøgene ikke kan besøge dit website, og i sidste ende vil domæne blive frigivet således at andre kan købe det.</p>
                            </div>
                        </div>
                    </div>
					<div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="collapsed r-tabs-anchor" aria-expanded="false">Hvor længe gælder en registrering af et domæne?</a></h4>
                        </div>

                        <div id="collapse3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                <p>Sålænge at du betaler årsafgiften, vil du være ejeren af det pågældende domæne.</p>
                            </div>
                        </div>
                    </div>
					<div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="collapsed r-tabs-anchor" aria-expanded="false">For hvor lang tid betaler man for et domæne?</a></h4>
                        </div>

                        <div id="collapse4" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                <p>De fleste domæner kører med at domænet skal fornyes årligt, dog er der nogle hvor man betaler for flere år ad gangen. Dette gælder fx .com og .net (2år).</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse5" class="collapsed r-tabs-anchor" aria-expanded="false">Registrerer du alle domæner?</a></h4>
                        </div>

                        <div id="collapse5" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                <p>Jeg kan tilbyde registrering af næsten alle tilgængelige domæner der er tilgængelige i dag. Og listen bliver større da der løbende udkommer nye domæner.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			</div>
		 </div>
		</section>
		
		<?php include('includes/bottom.php');?>
		
		<script>
			$(document).ready(function() {
				$("#domainextensions").owlCarousel({
					autoPlay: 4000,
					items: 8,
					itemsDesktop: [1199, 6],
					itemsDesktopSmall: [979, 6],
					pagination: false
				});
				
			});
		</script>
		
	</body>
	
</html>
