<!DOCTYPE html>
<html lang="da">
	
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Bag om Geek Media, hvad jeg står for og hvem jeg er.">
		<title>Geek Media | Bag om Geek Media</title>
		<?php include('includes/styles.php');?>
	</head>
	
	<body>
		
		<!-- Top Bar-->
		<?php include('includes/nav.php');?>
		<!-- End of Top Bar-->

		<!-- About Us -->
		<section class="about">
		<div class="breadcrumbs">
			<div class="row">
				<div class="col-sm-6">
					<h1>Bag om</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb">
						<li>Du er her: </li>
						<li><a href="/">Forside</a>
						</li>
						<li class="active">Bag om</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="row">
				<div class="col-sm-12">
					<h2>Geek Media</h2>
					<hr class="small">
					<p>Din nye partner indenfor design & udvikling</p>
				</div>
			</div>
		</section>
		<section>
			<div class="spacing-70"></div>
			<div class="row ">
				<div class="col-sm-8 center-block">
				<h2>Bag facaden</h2>
					<p>Mit navn er Carsten Meldgaard, og jeg er ejer af Geek Media, som blev startet i 2015. Jeg elsker design og udvikling mere end pizza og pasta. Jeg er passioneret omkring spændende brugeroplevelser og at udforske nye måder at levere bedre	resultater på. Du kan fange mig igang med at læse om udvikling eller ser genudsendelser af The Big Bang Theory.</p>
					
					<p>Geek Media er et design og brugerfokuseret digitalt bureau. Geek Media er specialister i at løse problemstillinger gennem intelligent design, engagerende oplevelser og meningsfulde forbindelser.</p>
					
					<h3>Nøglen til succes</h3>
					<p class="subHeadline">
						Nøglen til at skabe store brands, produkter og oplevelser kommer bl.a. gennem en
						forståelse af <span class="cursive">klienternes</span> og <span class="cursive">brugernes</span> behov. <br><br>
						De afgørende faser af konceptudvikling, research og planlægning er <span class="cursive">kernen</span> i ethvert vellykket projekt.
						<br><br>
						<span class="cursive">Geek Media</span> tager stor omhu i at sikre alle de nødvendige
						skridt er taget for at producere perfekte resultater hver gang.
					</p>
					<p class="subHeadline">
      Jeg er passioneret omkring hvad jeg laver. Det er ikke blot et stykke arbejde, men det er
      min hobby, min store interesse. Derfor er jeg heller ikke bange for at blive en smule arbejdsskabet med et projekt. <a href="/planner" class="cursive">Så start et projekt idag!</a>
    </p>
				</div>
			</div>
			
			<div class="spacing-70"></div>
			
			<div class="about-quote">
				<div class="row full-width no-gutter">
					<div class="col-sm-6 about-quote-column">
						<div class="thequote wow fadeInLeft" data-wow-delay="0.4s">
							<h5>Success is a science; if you have the conditions, you get the result.</h5>
							<span>Oscar Wilde</span>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row spacing-70">
				<div class="col-sm-8 center-block wow fadeInRight" data-wow-delay="0.2s">
					<h3>Geek Media's værdier
						<div id="values" class="owl-carousel">
							<div class="item">
								<div class="wrap">
									<span class="valueNo">01</span>
									<div>
										<h4>Tro på det digitale</h4>
										<p>Det digitale er ikke blot nuet, men vejen frem</p>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="wrap">
									<span class="valueNo">02</span>
									<div>
										<h4>En for alle</h4>
										<p>Vores kunder er hvorfor vi er her, lad os ikke komplicere det</p>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="wrap">
									<span class="valueNo">03</span>
									<div>
										<h4>Kvalitet er vigtigst</h4>
										<p>Masseproduktion er vejen til irellevans</p>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="wrap">
									<span class="valueNo">04</span>
									<div>
										<h4>Form og funktion</h4>
										<p>Design er kun effektivt såfremt det understøtter oplevelsen</p>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="wrap">
									<span class="valueNo">05</span>
									<div>
										<h4>Forvent kreativitet</h4>
										<p>Alle har noget at bidrage med</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</section>
			
			<div class="ourclients">
				<div class="row">
					<div class="col-sm-12">
						<h3>Lille udpluk af mine kunder</h3>
						
						<div class="row spacing-40">
							<div class="col-sm-9 center-block">
								
								<div class="block-grid-sm-4 block-grid-xs-2 clients">
									
									<div class="block-grid-item wow fadeInUp" data-wow-delay="0.05s">
										<img src="images/clients/citizone.png" alt="" />
									</div>
									<div class="block-grid-item wow fadeInUp" data-wow-delay="0.1s">
										<img src="images/clients/cleverdeal.png" alt="" />
									</div>
									<div class="block-grid-item wow fadeInUp" data-wow-delay="0.15s">
										<img src="images/clients/destinationer.png" alt="" />
									</div>
									<div class="block-grid-item wow fadeInUp" data-wow-delay="0.2s">
										<img src="images/clients/diersklinik.png" alt="" />
									</div>
									<div class="block-grid-item wow fadeInUp" data-wow-delay="0.25s">
										<img src="images/clients/midtjysk.png" alt="" />
									</div>
									<div class="block-grid-item wow fadeInUp" data-wow-delay="0.3s">
										<img src="images/clients/visitskanderborg.png" alt="" />
									</div>
									
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			
			
			<div class="about-quote odd">
				<div class="row full-width no-gutter">
					<div class="col-sm-6 col-sm-push-6 about-quote2-column">
						<div class="thequote wow fadeInRight" data-wow-delay="0.4s">
							<h5>It does not matter how slowly you go as long as you do not stop.</h5>
							<span>Confucius</span>
						</div>
					</div>
				</div>
			</div>
			
			<div class="testimonials">
				<div class="row">
					<div class="col-sm-12">
						<h3>Hvad siger mine kunder</h3>
						<div id="testimonials-carousel" class="owl-carousel">
							
							<div class="item">
								<div class="testimonial-content">
									<div class="testimonialimg"><img src="/images/testimonial1.jpg" alt="" />
									</div>
									<p>I forbindelse med en seminarrække i efterår/vinter 2013/14 havde projektet RETHINK Kulturturisme brug for en skriftlig opsamling af den tilegnede viden. Vi udarbejdede en skitse over indhold og form og kontaktede Carsten efter anbefalinger fra kollegaer i Midtjysk Turisme. Alpha-Omega for produktet var en hurtig leveringstid og fleksibilitet omkring korrektur og udviklingen af produktet. Carsten var effektiv og behagelig i vores mailkorrespondance og forstod hurtigt hvilken stil vi ønskede. Nem og gnidningsløs korrekturrunder, med effektiv brug af online hjælpemidler. Carsten er dygtig til at aflæse kundens designønsker og kommer gerne med egne forslag der kan matche. Vi vil anbefale Carsten som en effektiv samarbejdspartner indenfor grafiske opgaver. </p>
									<div class="whoclient">
										<h5>Dorte Dejbjerg Arens, Projektkoordinator, <a href="http://www.kulturturisme.dk/" target="_blank">RETHINK Kulturturisme</a></h5>
									</div>
								</div>
							</div>
							
							<div class="item">
								<div class="testimonial-content">
									<div class="testimonialimg"><img src="/images/testimonial2.jpg" alt="" />
									</div>
									<p>Carsten har i gennem en periode hjulpet nm design med diverse freelance opgaver fra a-z. nm design har i perioden bl.a. brugt Carsten til udarbejdelse af designs, lavet programmeringsopgaver af både større og mindre karakter etc. og alt sammen er udarbejdet meget professionelt og inden for aftalt tidsramme. Carsten er en person som meget lyttende og meget engageret, og han laver det han skal. Carsten har en idérig tankegang og har en forståelse for hvad det er kunden ønsker og hvad kunden har behov for, derudover han meget kreativ, med en anderledes tankegang. Vi har erfaret, at Carsten både kan arbejde i php-kodning samt asp-kodning. Vi kan på alle parametre indenfor webdesign varmt anbefale Carsten.
									</p>
									<div class="whoclient">
										<h5>Martin Arendt, Partner, <a  href="http://www.nm-design.dk/" target="_blank">NM Design</a></h5>
									</div>
									
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<!-- End of About Us -->
			
			<?php include('includes/bottom.php');?>
			<script>
				$(document).ready(function() {
					$("#values").owlCarousel({
						items: 1,
						autoPlay: 5000,
						itemsDesktop: [1199, 1],
						itemsDesktopSmall: [979, 1],
						itemsTablet: [768, 1]
					});
					
					$("#testimonials-carousel").owlCarousel({
						items: 1,
						autoPlay: 5000,
						itemsDesktop: [1199, 1],
						itemsDesktopSmall: [979, 1],
						itemsTablet: [768, 1]
					});
				});
			</script>
		</body>
		
	</html>
