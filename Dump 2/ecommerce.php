<!DOCTYPE html>
<html lang="da">
	
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Etabler en moderne og professionel webshop der konvertere besøgende til kunder">
		<title>Geek Media | eCommerce</title>
		<?php include('includes/styles.php');?>
	</head>
	
	<body>
		
		<!-- Top Bar-->
		<?php include('includes/nav.php');?>
		<!-- End of Top Bar-->
		<!-- About Us -->
		<section class="eCommerce">
			<div class="breadcrumbs">
				<div class="row">
					<div class="col-sm-6">
						<h1>eCommerce</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb">
							<li>Du er her: </li>
							<li><a href="/">Forside</a>
							</li>
							<li><a href="#">Web</a>
							</li>
							<li class="active">eCommerce</li>
						</ol>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<h2>eCommerce</h2>
					<hr class="small">
					<p>Etabler en moderne og professionel webshop der 
konvertere besøgende til kunder</p>
				</div>
			</div>
		</section>
		<section>
			<div class="spacing-70"></div>
			<div class="row ">
				<div class="col-sm-8 center-block">
					<h2>Flyt din butik hjem til kunderne</h2>
					<p>Danskerne har i stor stil taget e-handelskonceptet til sig, hvilket 
betød at den årlige omsætning i Danmark udgjorde kr. 80 mia. i 
2014. Det er 17% højere end året forinden, som var den højeste 
nogenside, og den positive tendens peger alt andet lige på at 
fortsætte.</p>
					<p>
						Desto vigtigere bliver det derfor også for både eksisterende detail 
butikker og nystartede virksomheder at være til stede online, når 
de købelystne danske forbrugere kommer forbi.</p>
					<p>
						Når din eCommerce løsning skal designes tages der udgangspunkt i, at det skal være nemt og intuitivt for kunderne at anvende 
shoppen og finde de ønskede produkter - og alle skal kunne være 
med, uanset deres brugerniveau. 
					</p>
					<p>
						Et primært fokus derudover er, at minimere de økonomiske 
omkostninger og tiden der forbindes med, at opsætte og 
vedligeholde din shop uden at begrænse antallet af tilgængelige 
funktioner, muligheder og brugervenlighed.
					</p>
				</div>
				<div class="spacing-70"></div>
				<div class="col-sm-8 center-block">
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.2s" id="ecom_1">
						<h3>Ecommerce Strategi</h3>
						<p>Udvikle en grundig forståelse af online detail mål og give en sund strategisk retning.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.3s" id="ecom_2">
						<h3>PUREPLAY & OMNICHANNEL</h3>
						<p>Løsninger til både online forhandlere og integrerede platforme for mursten og mørtel butikker.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.4s" id="ecom_3">
						<h3>INDUSTRI LEDENDE PLATFORME</h3>
						<p>Hosted og komplette skræddersyede løsninger bygget på førende platforme til at tilfredsstille alle eCommerce krav.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.2s" id="ecom_4">
						<h3>FULD FEATURED</h3>
						<p>Fra systemintegration, medlemskab, kampagner, betalinger og mere. Jeg har dig dækket.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.3s" id="ecom_5">
						<h3>DESIGN & UX</h3>
						<p>Design & brugeroplevelses strategier for bedre at kommunikere dit brand ud og engagere sig med et publikum.</p>
					</div>
					<div class="col-sm-6 col-md-4 services wow zoomIn" data-wow-delay="0.4s" id="ecom_6">
						<h3>DEVICE INDEPENDENCE</h3>
						<p>Fuldt optimeret eCommerce setup på tværs af alle enheder, herunder responsive og dedikerede mobile websites.</p>
					</div>
				</div>
				<div class="col-md-12 jumpDown">
					<a class="btn btn-jump" href="#cms">Læs mere</a>
				</div>
			</div>
			
			<div class="spacing-70"></div>
			
			<!--  Call to Action -->
    <section class="calltoaction">

        <div class="row wow zoomIn" data-wow-delay="0.2s">
            <div class="col-sm-10 com-md-8 center-block">

                <div class="row no-gutter cta-content">
                    <div class="col-sm-4">
                        <div class="offer wow fadeInUp" data-wow-delay="0.5s">
                            <span>LAD OS</span>
                            <a href="/planner"><h2>STARTE</h2></a>
                            <span>I DAG</span>
                        </div>
                    </div>
                    <div class="col-sm-8">

                        <div class="offerdescription wow fadeInUp" data-wow-delay="0.7s">
                            <h2>START ALLEREDE I DAG</h2>
                            <p>ved at udfylde min online formular og få en uforpligtende samtale</p>
                        </div>
                    </div>
                </div>
            </div>	
        </div>

    </section>
    <!-- End of Call to Action -->
			<div class="spacing-70"></div>
			<div class="row ">
				<div class="col-sm-8 center-block" id="cms">
					<h2>Industri førende platforme</h2>
					<p>Uanset om du har 100.000 varenumre eller kun en håndfuld; uanset om du er en etableret online forhandler eller nye til e-handel, arbejder jeg med de mest effektive eCommerce platforme til at levere resultater, der er skræddersyet til dine behov og krav.</p>
					<img src="/images/logo_magento.png" width="260" height="100" alt="Magento eCommerce">
					<img src="/images/logo_shopify.png" width="260" height="100" alt="Magento eCommerce">
					<img src="/images/logo_woocommerce.png" width="260" height="100" alt="Magento eCommerce">
					<p>Digitalt præsenterer uendelige muligheder for detailhandel og jeg er passionerede omkring at bygge fantastiske eCommerce projekter for alle mine kunder.</p>
					<p class="cta">Klar til at handle? <a href="/planner" title="Start et projekt">Lad os tale samme i dag!</a></p>
				</div>
			</div>
		</section>

<!-- End of About Us -->

<?php include('includes/bottom.php');?>

<script type="text/javascript">
	// ______________  TOOLTIPS
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
	});
	
	// ______________ TABS
	$('#shared-hosting-tabs').responsiveTabs({
		startCollapsed: 'accordion'
	});
</script>
</body>

</html>
