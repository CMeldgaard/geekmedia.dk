
<!DOCTYPE html>
<html lang="da">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Få hurtigt fat på mig!">
    <title>Geek Media | Kontakt mig</title>
    <?php include('includes/styles.php');?>
</head>

<body>

<!-- Top Bar-->
<?php include('includes/nav.php');?>
<!-- End of Top Bar-->

 <!-- Breadcrumps -->
    <div class="breadcrumbs mb0 solid">
        <div class="row">
            <div class="col-sm-6">
                <h1>Kontakt</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li>Du er her: </li>
                    <li><a href="/">Forside</a>
                    </li>
                    <li class="active">Kontakt</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- End of Breadcrumps -->

    <!-- Contact -->
    <div id="map_wrapper">
    <div id="map_canvas" class="mapping"></div>
    </div>
<div class="spacing-70"></div>
    <section class="contact">
        <div class="row">
            <div class="col-sm-8">
                <h3>Kontakt mig</h3>
                <div id="sendstatus"></div>
<div id="contactform">
<?php 
if (isset($_POST['name'])){

$modtager = "cm@geekmedia.dk";
$emne = "Opstart af projekt";

$besked = "<h1>Opstart af projekt hos Geek Media</h1> \n
               <strong>Kontaktinformation</strong><br /> \n
               Navn:" . $_POST['name']. "<br /> \n
			   E-mail:" . $_POST['email']. "<br /> \n
			   <strong>Besked</strong><br /> \n
                " . $_POST['comments'] . "<br />";

$header  = "MIME-Version: 1.0" . "\r\n";

$header .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";

$header .= "from:" . $_POST['email'] ."";

mail($modtager, $emne, $besked, $header); //Sender formularen
?>
	 <h2>Mange tak for din besked</h1>
        <p>
            Tak fordi du er interesseret i at arbejde sammen med Geek Media. Jeg forsøger at besvare alle beskeder indenfor én arbejdsdag.
        </p>
<?php }else{ ?>


<form method="post" action="">

            <p><label for="name">Navn:*</label> <input type="text" class="form-control" name="name" id="name" tabindex="1" /></p>
            <p><label for="email">E-mail:*</label> <input type="text" class="form-control" name="email" id="email" tabindex="2" /></p>
            <p><label for="comments">Besked:*</label> <textarea  class="form-control" name="comments" id="comments" cols="12" rows="6" tabindex="3"></textarea></p>
            <p><input name="submit" type="submit" id="submit" class="submit" value="Send besked" tabindex="4" /></p>

</form>
<?php }?>
</div>
            </div>

             <div class="col-sm-3 col-sm-offset-1">
             <h4 class="badge">E-mail</h4>
            <p><a href="mailto:hallo@geekmedia.dk">hallo@geekmedia.dk</a></p>
            <h4 class="badge">Telefon</h4>
            <p>+45 31 60 60 13</p>
            <h4 class="badge">Find mig her</h4>
            <ul>
            <li><a href="https://www.facebook.com/GeekMediaDK" target="_blank">Facebook</a></li>
            <li><a href="https://twitter.com/GeekmediaDK" target="_blank">Twitter</a></li>
			<li><a href="https://plus.google.com/+GeekmediaDkPlus" target="_blank">Google+</a></li>
            <li><a href="https://www.linkedin.com/company/geek-media-dk" target="_blank">LinkedIn</a></li>
            <li><a href="https://instagram.com/geekmediadk" target="_blank">Instagram</a></li>
            </ul>
            </div>
        </div>
    </section>
    <!-- End of Contact  -->	
	

<?php include('includes/bottom.php');?>
<script>
jQuery(function($) {
    // Asynchronously Load the map API
    var script = document.createElement('script');
    script.src = "http://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
    document.body.appendChild(script);
});

function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap',
        scrollwheel: false,
     draggable: false,
        styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#c6eee7"},{"visibility":"on"}]}]
    };

    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);

    // Multiple Markers
    var markers = [
        ['Geek Media, Rolfsgade 154, 6700 Esbjerg', 55.4743445,8.438759600000026],
    ];

    // Info Window Content
    var infoWindowContent = [
        ['<div class="info_content">' +
        '<h5>Geek Media, Rolfsgade 154 st. tv., 6700 Esbjerg</h5>' +
        '<p>Geek Media er et design og brugerfokuseret digitalt bureau. Geek Media er specialister i at løse problemstillinger gennem intelligent design, engagerende oplevelser og meningsfulde forbindelser.</p>' +        '</div>']
    ];

    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;

    // Loop through our array of markers & place each one on the map
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });

        // Allow each marker to have an info window
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(15);
        google.maps.event.removeListener(boundsListener);
    });

    }
</script>
</body>

</html>