<?php
//Header+OG setup
$siteName = "Geek Media";
$title = "Bag om Geek Media";
$description = "Geek Media ejes af Carsten Meldgaard, som har flere års erfaring indenfor webudvikling, grafisk design og online medier.";
$keywords = "";
$image = "images/header/hero6.jpg";
?>
<!DOCTYPE html>
<html>

<head>
    <?php include("includes/meta.php")?>
    <?php require("includes/styles.php")?>
</head>
<body>

<?php include("includes/preloader.php");?>

<?php include("includes/nav.php");?>

<div class="main-container">
    <header class="page-header">
        <div class="background-image-holder">
            <img class="background-image" alt="Background Image" src="/images/header/hero6.jpg">
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <span class="text-white alt-font">Mød mig</span>
                    <h1 class="text-white">Bag om Geek Media</h1>
                    <p class="text-white lead">Lær hvem der er bag Geek Media,<br> og hvorfor jeg skal indgå i jeres team.</p>
                </div>
            </div>
        </div>
    </header>
    <section class="inline-image-right">
        <div class="container">
            <div class="row ">
                <div class="col-sm-6 align-vertical no-align-mobile">
                    <h1>Bag facaden på Geek Media</h1>
                    <h6>Digitally minded // Creative at heart</h6>
                    <p class="lead ">
                        Mit navn er Carsten Meldgaard, og jeg er ejer af Geek Media, som blev startet i 2015. Jeg elsker
                        design og udvikling mere end pizza og pasta, hvilket siger en del om mig.
                        </p>
                    <p class="lead">
                        Jeg er passioneret omkring spændende brugeroplevelser og at udforske nye måder at levere bedre
                        resultater på. Hvis jeg ikke er fordybet i et webprojekt eller lege med designopsætning kan du
                        helt sikkert fange mig igang med at læse om udvikling eller se genudsendelser af
                        The Big Bang Theory.
                        </p>
                    <p class="lead">
                        Grundlaget for Geek Media er at tilbyde virksomheder adgang til mine freelance kompetencer
                        indenfor design og udvikling, men at de samtidgt skal se mig som en integreret del af
                        virksomheden. Det vil sige at virksomhederne får de samme fordele som ved bureauerne eller en
                        inhouse afdeling, men de betaler kun for de reelle timer de har brug for hver måned.
                    </p>
                </div>

                <div class="col-sm-6 text-center vjs-1444132601945-6" vic="vjs-1444132601945-6">
                    <img alt="Product Image" class="product-image vjs-1444132601945-7" src="/images/devices.png" vic="vjs-1444132601945-7">
                </div>
            </div>
        </div>
    </section>
    <?php include("includes/cta.php");?>
    <section class="side-image text-heavy clearfix">
        <div class="image-container col-md-5 col-sm-3 pull-left">
            <div class="background-image-holder" style="background: url(/images/side2.jpg) 50% 0%;">
                <img class="background-image" alt="Background Image" src="/images/side2.jpg">
            </div>
        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-6 col-md-offset-6 col-sm-8 col-sm-offset-4 content">
                    <h1>Geek Media &amp; Partnere</h1>
                    <p class="lead">
                        Hvor mine kompetencer ikke rækker, har jeg et netværk af "kolleger", som jeg gerne henviser til. Disse "kolleger" er selv selvstændige, og du forhandler derfor direkte med dem, uden nogen form for fordyrende mellemled!</p>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="team-2-member">
                                <div class="image-holder">
                                    <img alt="Team Member" class="background-image" src="/images/team/carsten.jpg">
                                    <div class="hover-state">
                                        <ul class="social-icons align-vertical" style="padding-top: 168.25px;">
                                            <li>
                                                <a href="https://www.facebook.com/cmeldgaard" target="_blank">
                                                    <i class="icon social_facebook"></i>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="https://dk.linkedin.com/in/cmeldgaard" target="_blank">
                                                    <i class="icon social_linkedin"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <span class="name">Carsten Meldgaard<br>
                                <a href="#">Ejer Geek Media</a> </span>

                                <p>
                                    Designer i hjertet og nørd i hovedet. Med over 8 års erfaring indenfor web løsninger
                                    kan jeg guide jer igennem jeres online projekter og samle hele jeres brand i en rød
                                    tråd, og leder jer igennem af facetter i projektet så i kommer i hus med det bedste
                                    resultat.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="team-2-member">
                                <div class="image-holder">
                                    <img alt="Team Member" class="background-image" src="/images/team/soren.jpg">
                                    <div class="hover-state">
                                        <ul class="social-icons align-vertical" style="padding-top: 168.25px;">
                                            <li>
                                                <a href="https://www.facebook.com/profile.php?id=1229824636" target="_blank">
                                                    <i class="icon social_facebook"></i>
                                                </a>
                                            </li>

                                            <li class="sp">
                                                <a href="https://www.linkedin.com/profile/view?id=AAkAAAAxtRwBrq2ZB7tDEYDp87icN6AL6ibhIXg" target="_blank">
                                                    <i class="icon social_linkedin"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <span class="name">Søren Peter Frøsig<br>
                                 <a href="http://www.creativesignature.dk/" target="_blank">Ejer Creative Signature</a>
                                </span>
                                <p>
                                    Creative Signature er den fleksibel marketingressource, som du som virksomhed nemt
                                    kan ”skrue op og ned” for alt efter behov. Der er finger på pulsen,
                                    nærværende sparring og inspiration til optimal udnyttelse af din virksomheds
                                    marketingbudget.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="team-2-member">
                                <div class="image-holder">
                                    <img alt="Team Member" class="background-image" src="/images/extLogo/esatryk.png">
                                    <div class="hover-state">
                                        <ul class="social-icons align-vertical" style="padding-top: 168.25px;">
                                            <li>
                                                <a href="https://www.facebook.com/esatryk/" target="_blank">
                                                    <i class="icon social_facebook"></i>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="https://plus.google.com/116557159646741728709/about" target="_blank">
                                                    <i class="icon social_googleplus"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <span class="name">EsaTryk<br>
                                 <a href="http://www.esatryk.dk/" target="_blank">Trykkeri</a>
                                </span>
                                <p>
                                    Faguddannet personale, CTP-produktion, toptunet maskineri og gode papirkvaliteter.
                                    Det er opskriften på produktion af kvalitet i lange baner. EsaTryk er specialister
                                    i offset-produktion til både tryksager og kartonnage.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

    <section class="expanding-list">
        <div class="background-image-holder">
            <img class="background-image" alt="Background Image" src="/images/hero22.jpg">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h1>Geek Medias Værdier</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                    <ul class="expanding-ul">
                        <li>
                            <div class="title">
                                <i class="icon icon-mobile"></i>
                                <span><strong>Tro på det digitale</strong></span>
                            </div>

                            <div class="text-content">
                                <p>
                                    Det digitale er ikke blot nuet, men vejen frem
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="title">
                                <i class="icon icon-chat"></i>
                                <span><strong>En for alle</strong></span>
                            </div>

                            <div class="text-content">
                                <p>
                                    Mine kunder er hvorfor jeg er her, lad os ikke komplicere det
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="title">
                                <i class="icon icon-pencil"></i>
                                <span><strong>Kvalitet er vigtigst</strong></span>
                            </div>

                            <div class="text-content">
                                <p>
                                    Masseproduktion er vejen til irellevans
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="title">
                                <i class="icon icon-circle-compass"></i>
                                <span><strong>Form og funktion</strong></span>
                            </div>

                            <div class="text-content">
                                <p>
                                    Design er kun effektivt såfremt det understøtter oplevelsen
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="title">
                                <i class="icon icon-lightbulb"></i>
                                <span><strong>Forvent kreativitet</strong></span>
                            </div>

                            <div class="text-content">
                                <p>
                                    Alle har noget at bidrage med
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <?php include("includes/newsletterForm.php");?>
</div>
<?php require("includes/footer.php");?>
<?php require("includes/scripts.php");?>
</body>
</html>
